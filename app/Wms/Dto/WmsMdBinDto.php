<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 库位管理Dto （导入导出）
 */
#[ExcelData]
class WmsMdBinDto implements MineModelExcel
{
    #[ExcelProperty(value: "仓库", index: 0)]
    public string $bin_store;

    #[ExcelProperty(value: "主键", index: 1)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 2)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 3)]
    public string $sys_company_code;

    #[ExcelProperty(value: "库位名称", index: 4)]
    public string $ku_wei_ming_cheng;

    #[ExcelProperty(value: "库位编码", index: 5)]
    public string $ku_wei_bian_ma;

    #[ExcelProperty(value: "库位条码", index: 6)]
    public string $ku_wei_tiao_ma;

    #[ExcelProperty(value: "库区", index: 7)]
    public string $ku_wei_lei_xing;

    #[ExcelProperty(value: "库位属性", index: 8)]
    public string $ku_wei_shu_xing;

    #[ExcelProperty(value: "上架次序", index: 9)]
    public string $shang_jia_ci_xu;

    #[ExcelProperty(value: "取货次序", index: 10)]
    public string $qu_huo_ci_xu;

    #[ExcelProperty(value: "所属客户", index: 11)]
    public string $suo_shu_ke_hu;

    #[ExcelProperty(value: "体积单位", index: 12)]
    public string $ti_ji_dan_wei;

    #[ExcelProperty(value: "重量单位", index: 13)]
    public string $zhong_liang_dan_wei;

    #[ExcelProperty(value: "面积单位", index: 14)]
    public string $mian_ji_dan_wei;

    #[ExcelProperty(value: "出库口", index: 15)]
    public string $zui_da_ti_ji;

    #[ExcelProperty(value: "最大重量", index: 16)]
    public string $zui_da_zhong_liang;

    #[ExcelProperty(value: "货架", index: 17)]
    public string $zui_da_mian_ji;

    #[ExcelProperty(value: "最大托盘", index: 18)]
    public string $zui_da_tuo_pan;

    #[ExcelProperty(value: "长度", index: 19)]
    public string $chang;

    #[ExcelProperty(value: "宽度", index: 20)]
    public string $kuan;

    #[ExcelProperty(value: "高度", index: 21)]
    public string $gao;

    #[ExcelProperty(value: "停用", index: 22)]
    public string $ting_yong;

    #[ExcelProperty(value: "明细", index: 23)]
    public string $ming_xi;

    #[ExcelProperty(value: "产品属性", index: 24)]
    public string $chp_shu_xing;

    #[ExcelProperty(value: "备注1", index: 25)]
    public string $ming_xi1;

//    #[ExcelProperty(value: "备注2", index: 26)]
//    public string $ming_xi2;
//
//    #[ExcelProperty(value: "动线", index: 27)]
//    public string $ming_xi3;
//
//    #[ExcelProperty(value: "LORA_bqid", index: 28)]
//    public string $LORA_bqid;
//
//    #[ExcelProperty(value: "x坐标", index: 29)]
//    public string $xnode;
//
//    #[ExcelProperty(value: "y坐标", index: 30)]
//    public string $ynode;
//
//    #[ExcelProperty(value: "z坐标", index: 31)]
//    public string $znode;
//
//    #[ExcelProperty(value: "状态", index: 32)]
//    public string $status;
//
//    #[ExcelProperty(value: "排序", index: 33)]
//    public string $sort;
//
//    #[ExcelProperty(value: "商户id", index: 34)]
//    public string $mer_id;
//
//    #[ExcelProperty(value: "创建者", index: 35)]
//    public string $created_by;
//
//    #[ExcelProperty(value: "更新者", index: 36)]
//    public string $updated_by;
//
//    #[ExcelProperty(value: "创建时间", index: 37)]
//    public string $created_at;
//
//    #[ExcelProperty(value: "更新时间", index: 38)]
//    public string $updated_at;
//
//    #[ExcelProperty(value: "删除时间", index: 39)]
//    public string $deleted_at;


}