import { request } from '@/utils/request.js'

/**
 * 入库管理 API JS
 */

export default {

  /**
   * 获取入库管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/wmImNoticeH/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加入库管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/wmImNoticeH/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新入库管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/wmImNoticeH/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/wmImNoticeH/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取入库管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/wmImNoticeH/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将入库管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/wmImNoticeH/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取入库管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/wmImNoticeH/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复入库管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/wmImNoticeH/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除入库管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/wmImNoticeH/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改入库管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/wmImNoticeH/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改入库管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/wmImNoticeH/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 入库管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/wmImNoticeH/import',
      method: 'post',
      data
    })
  },

  /**
   * 入库管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/wmImNoticeH/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 入库管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/wmImNoticeH/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}