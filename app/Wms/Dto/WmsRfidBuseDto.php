<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * RFID管理Dto （导入导出）
 */
#[ExcelData]
class WmsRfidBuseDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "流程状态", index: 3)]
    public string $bpm_status;

    #[ExcelProperty(value: "类型", index: 4)]
    public string $wms_rfid_type;

    #[ExcelProperty(value: "业务编号", index: 5)]
    public string $wms_rfid_buseno;

    #[ExcelProperty(value: "业务内容", index: 6)]
    public string $wms_rfid_busecont;

    #[ExcelProperty(value: "RFID1", index: 7)]
    public string $wms_rfid_id1;

    #[ExcelProperty(value: "RFID2", index: 8)]
    public string $wms_rfid_id2;

    #[ExcelProperty(value: "RFID3", index: 9)]
    public string $wms_rfid_id3;

    #[ExcelProperty(value: "状态", index: 10)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 11)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 12)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 13)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 14)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 15)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 16)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 17)]
    public string $deleted_at;


}