<?php

declare (strict_types=1);
namespace App\Crm\Model;

use App\System\Service\SystemUserService;
use Hyperf\Database\Model\SoftDeletes;
use Mine\MineModel;
/**
 * @property int $id 
 * @property int $sales_stag 销售阶段
 * @property int $follow_user 跟进人
 * @property string $name 客户名称
 * @property int $from 客户来源
 * @property int $level 需求级别
 * @property string $com 官网
 * @property string $phone 电话
 * @property string $email 邮箱
 * @property string $legal_perso 法人
 * @property int $money 注册资本
 * @property string $type 公司类型
 * @property int $scale 公司规模
 * @property string $industry 所属行业
 * @property string $range 经营范围
 * @property string $address 地址
 * @property string $address_detail 详细地址
 * @property string $bank 开户银行
 * @property string $bank_number 银行账号
 * @property string $user_name 姓名
 * @property int $sex 性别
 * @property string $position 职位
 * @property string $user_phone 手机
 * @property string $user_email 邮箱
 * @property string $user_wechat 微信
 * @property string $user_ding 钉钉
 * @property string $user_qq QQ
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class CrmUser extends MineModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crm_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sales_stag', 'follow_user', 'follow_user_name','name', 'from', 'level', 'com', 'phone', 'email', 'legal_perso', 'money', 'type', 'scale', 'industry', 'range', 'address', 'address_detail', 'bank', 'bank_number', 'user_name', 'sex', 'position', 'user_phone', 'user_email', 'user_wechat', 'user_ding', 'user_qq', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer', 'sales_stag' => 'string', 'follow_user' => 'array', 'from' => 'string',
        'level' => 'string', 'money' => 'integer', 'scale' => 'integer', 'sex' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime','address'=> 'array'
    ];

    public function setFollowUserAttribute($value)
    {
        /** @var SystemUserService $userService */
        $userService = make(SystemUserService::class);
        $this->attributes['follow_user_name'] = $userService->read((int)$value[0])['username'] ?? '';
        $this->attributes['follow_user'] = json_encode($value, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}