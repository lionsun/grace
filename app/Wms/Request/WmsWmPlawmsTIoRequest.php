<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 月台管理验证数据类
 */
class WmsWmPlawmsTIoRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['plawms_t_id','carno','doc_id','plan_indata','plan_outdata','in_data','ouwms_t_data','plawms_t_beizhu','status','sort',],
             'update' => ['plawms_t_id','carno','doc_id','plan_indata','plan_outdata','in_data','ouwms_t_data','plawms_t_beizhu','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'plawms_t_id.required'=>' 月台不能为空',
'carno.required'=>' 车牌号不能为空',
'plan_indata.required'=>' 计划进入时间不能为空',
'plan_outdata.required'=>' 计划驶出时间不能为空',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 月台 验证
            'plawms_t_id' => 'required',
            // 车牌号 验证
            'carno' => 'required',
            // 计划进入时间 验证
            'plan_indata' => 'required',
            // 计划驶出时间 验证
            'plan_outdata' => 'required',
            ];
        }

}