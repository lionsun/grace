import { request } from '@/utils/request.js'

/**
 * 收支项目 API JS
 */

export default {

  /**
   * 获取收支项目分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/incomePay/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加收支项目
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/incomePay/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新收支项目数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/incomePay/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/incomePay/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取收支项目
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/incomePay/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将收支项目删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/incomePay/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取收支项目数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/incomePay/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复收支项目数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/incomePay/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除收支项目
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/incomePay/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改收支项目数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/incomePay/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改收支项目数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/incomePay/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 收支项目导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/incomePay/import',
      method: 'post',
      data
    })
  },

  /**
   * 收支项目下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/incomePay/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 收支项目导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/incomePay/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}