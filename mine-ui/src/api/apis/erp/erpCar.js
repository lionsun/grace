import { request } from '@/utils/request.js'

/**
 * 车辆管理 API JS
 */

export default {

  /**
   * 获取车辆管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/car/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加车辆管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/car/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新车辆管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/car/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/car/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取车辆管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/car/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将车辆管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/car/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取车辆管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/car/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复车辆管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/car/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除车辆管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/car/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改车辆管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/car/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改车辆管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/car/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 车辆管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/car/import',
      method: 'post',
      data
    })
  },

  /**
   * 车辆管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/car/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 车辆管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/car/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}