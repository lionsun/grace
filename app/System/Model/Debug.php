<?php

declare (strict_types=1);

namespace App\System\Model;

use Mine\MineModel;

/**
 * @property int $id
 * @property string $en_key 英文索引
 * @property string $cn_key 中文索引
 * @property string $remark 日志内容
 * @property int $created_at
 */
class Debug extends MineModel
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_common_manage_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'en_key', 'cn_key', 'remark', 'created_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'created_at' => 'datetime'];

    public static function info($remark = '', $en_key = '', $cn_key = '', $warnType = 'sms')
    {
        $model = new self();
        if (is_numeric($remark)) {
            $remark = strval($remark);
        }
        if (is_array($remark)) {
            $remark = json_encode($remark, JSON_UNESCAPED_UNICODE);
        }

        if (is_object($remark)) {
            $remark = serialize($remark);
            //$remark = $remark->toArray();
        }
        $model->remark = $remark;
        $model->en_key = $en_key;
        $model->cn_key = $cn_key;
        $model->created_at = time();
        $model->save();
    }

    /**
     * 错误预警
     * @param mixed $remark
     * @param string $warnType 预警方式
     * @return void
     */
    public static function warn(mixed $remark, string $warnType = 'sms')
    {
        self::info($remark);
    }
}