import { request } from '@/utils/request.js'

/**
 * 会员信息 API JS
 */

export default {

  /**
   * 获取会员信息分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/user/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加会员信息
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/user/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新会员信息数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/user/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/user/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取会员信息
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/user/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将会员信息删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/user/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取会员信息数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/user/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复会员信息数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/user/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除会员信息
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/user/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改会员信息数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/user/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改会员信息数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/user/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 会员信息导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/user/import',
      method: 'post',
      data
    })
  },

  /**
   * 会员信息下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/user/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 会员信息导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/user/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}