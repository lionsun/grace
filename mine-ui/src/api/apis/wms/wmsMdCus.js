import { request } from '@/utils/request.js'

/**
 * 客户信息 API JS
 */

export default {

  /**
   * 获取客户信息分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/mdCus/index',
      method: 'get',
      params
    })
  },

	/**
	 * 获取客户信息列表
	 * @returns
	 */
	list (params = {}) {
		return request({
			url: 'wms/mdCus/list',
			method: 'get',
			params
		})
	},

  /**
   * 添加客户信息
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/mdCus/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新客户信息数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/mdCus/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/mdCus/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取客户信息
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/mdCus/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将客户信息删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/mdCus/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取客户信息数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/mdCus/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复客户信息数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/mdCus/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除客户信息
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/mdCus/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改客户信息数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/mdCus/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改客户信息数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/mdCus/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 客户信息导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/mdCus/import',
      method: 'post',
      data
    })
  },

  /**
   * 客户信息下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/mdCus/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 客户信息导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/mdCus/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}
