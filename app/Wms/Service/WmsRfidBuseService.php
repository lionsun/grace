<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Service;

use App\Wms\Mapper\WmsRfidBuseMapper;
use Mine\Abstracts\AbstractService;

/**
 * RFID管理服务类
 */
class WmsRfidBuseService extends AbstractService
{
    /**
     * @var WmsRfidBuseMapper
     */
    public $mapper;

    public function __construct(WmsRfidBuseMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}