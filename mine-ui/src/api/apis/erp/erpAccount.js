import { request } from '@/utils/request.js'

/**
 * 结算账户 API JS
 */

export default {

  /**
   * 获取结算账户分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/account/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加结算账户
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/account/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新结算账户数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/account/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/account/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取结算账户
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/account/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将结算账户删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/account/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取结算账户数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/account/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复结算账户数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/account/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除结算账户
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/account/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改结算账户数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/account/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改结算账户数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/account/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 结算账户导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/account/import',
      method: 'post',
      data
    })
  },

  /**
   * 结算账户下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/account/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 结算账户导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/account/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}