<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Service;

use App\Erp\Mapper\ErpCustomerVendorMapper;
use Mine\Abstracts\AbstractService;

/**
 * 供应商信息服务类
 */
class ErpCustomerVendorService extends AbstractService
{
    /**
     * @var ErpCustomerVendorMapper
     */
    public $mapper;

    public function __construct(ErpCustomerVendorMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}