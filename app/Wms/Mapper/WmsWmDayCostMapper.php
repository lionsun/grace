<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsWmDayCost;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 计费管理Mapper类
 */
class WmsWmDayCostMapper extends AbstractMapper
{
    /**
     * @var WmsWmDayCost
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsWmDayCost::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        $query->with(['MdCus']);

        // 客户
        if (isset($params['cus_code']) && $params['cus_code'] !== '') {
            $query->where('cus_code', '=', $params['cus_code']);
        }

        // 费用名称
        if (isset($params['coswms_t_name']) && $params['coswms_t_name'] !== '') {
            $query->where('coswms_t_name', '=', $params['coswms_t_name']);
        }

        // 费用日期
        if (isset($params['coswms_t_data']) && $params['coswms_t_data'] !== '') {
            $query->where('coswms_t_data', '=', $params['coswms_t_data']);
        }

        // 是否已结算
        if (isset($params['coswms_t_js']) && $params['coswms_t_js'] !== '') {
            $query->where('coswms_t_js', '=', $params['coswms_t_js']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 商户id
        if (isset($params['mer_id']) && $params['mer_id'] !== '') {
            $query->where('mer_id', '=', $params['mer_id']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}