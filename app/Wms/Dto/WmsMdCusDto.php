<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 客户信息Dto （导入导出）
 */
#[ExcelData]
class WmsMdCusDto implements MineModelExcel
{
    #[ExcelProperty(value: "主键", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "助记码", index: 3)]
    public string $zhu_ji_ma;

    #[ExcelProperty(value: "客户简称", index: 4)]
    public string $ke_hu_jian_cheng;

    #[ExcelProperty(value: "客户英文名称", index: 5)]
    public string $ke_hu_ying_wen;

    #[ExcelProperty(value: "曾用企业代码", index: 6)]
    public string $zeng_yong_qi;

    #[ExcelProperty(value: "曾用企业名称", index: 7)]
    public string $zeng_yong_qi_ye;

    #[ExcelProperty(value: "客户状态", index: 8)]
    public string $ke_hu_zhuang_tai;

    #[ExcelProperty(value: "客户等级", index: 9)]
    public string $ke_hu_deng_ji;

    #[ExcelProperty(value: "所属行业", index: 10)]
    public string $suo_shu_xing_ye;

    #[ExcelProperty(value: "首签日期", index: 11)]
    public string $shou_qian_ri_qi;

    #[ExcelProperty(value: "终止合作时间", index: 12)]
    public string $zhong_zhi_he_shi_jian;

    #[ExcelProperty(value: "申请时间", index: 13)]
    public string $shen_qing_shi_jian;

    #[ExcelProperty(value: "归属组织代码", index: 14)]
    public string $gui_shu_zu_zh;

    #[ExcelProperty(value: "归属省份代码", index: 15)]
    public string $gui_shu_sheng;

    #[ExcelProperty(value: "归属市代码", index: 16)]
    public string $gui_shu_shi_dai;

    #[ExcelProperty(value: "归属县区代码", index: 17)]
    public string $gui_shu;

    #[ExcelProperty(value: "邮政编码", index: 18)]
    public string $you_zheng_bian_ma;

    #[ExcelProperty(value: "电话", index: 19)]
    public string $dian_hua;

    #[ExcelProperty(value: "传真", index: 20)]
    public string $chuan_zhen;

    #[ExcelProperty(value: "Email地址", index: 21)]
    public string $Emaildi_zhi;

    #[ExcelProperty(value: "网页地址", index: 22)]
    public string $wang_ye_di_zhi;

    #[ExcelProperty(value: "法人代表", index: 23)]
    public string $fa_ren_dai_biao;

    #[ExcelProperty(value: "法人身份证号", index: 24)]
    public string $fa_ren_shen_fen;

//    #[ExcelProperty(value: "注册资金万元", index: 25)]
//    public string $zhu_ce_zi_jin;
//
//    #[ExcelProperty(value: "币别", index: 26)]
//    public string $bi_bie;
//
//    #[ExcelProperty(value: "税务登记证号", index: 27)]
//    public string $shui_wu_deng;
//
//    #[ExcelProperty(value: "组织机构代码证", index: 28)]
//    public string $zu_zhi_ji_gou;
//
//    #[ExcelProperty(value: "道路运输经营许可证", index: 29)]
//    public string $dao_lu_yun_shu;
//
//    #[ExcelProperty(value: "主营业务", index: 30)]
//    public string $zhu_ying_ye_wu;
//
//    #[ExcelProperty(value: "合作意向", index: 31)]
//    public string $he_yi_xiang;
//
//    #[ExcelProperty(value: "批准机关", index: 32)]
//    public string $pi_zhun_ji_guan;
//
//    #[ExcelProperty(value: "批准文号", index: 33)]
//    public string $pi_zhun_wen_hao;
//
//    #[ExcelProperty(value: "注册日期", index: 34)]
//    public string $zhu_ce_ri_qi;
//
//    #[ExcelProperty(value: "备注", index: 35)]
//    public string $bei_zhu;
//
//    #[ExcelProperty(value: "联系人1", index: 36)]
//    public string $zhu_lian_xi_ren1;
//
//    #[ExcelProperty(value: "电话1", index: 37)]
//    public string $dian_hua1;
//
//    #[ExcelProperty(value: "状态", index: 38)]
//    public string $status;
//
//    #[ExcelProperty(value: "排序", index: 39)]
//    public string $sort;
//
//    #[ExcelProperty(value: "手机", index: 40)]
//    public string $shou_ji;
//
//    #[ExcelProperty(value: "商户id", index: 41)]
//    public string $mer_id;
//
//    #[ExcelProperty(value: "负责人", index: 42)]
//    public string $zhu_lian_xi_ren;
//
//    #[ExcelProperty(value: "创建者", index: 43)]
//    public string $created_by;
//
//    #[ExcelProperty(value: "地址", index: 44)]
//    public string $di_zhi;
//
//    #[ExcelProperty(value: "营业执照号", index: 45)]
//    public string $ying_ye_zhi_zhao;
//
//    #[ExcelProperty(value: "更新者", index: 46)]
//    public string $updated_by;
//
//    #[ExcelProperty(value: "中文全称", index: 47)]
//    public string $zhong_wen_qch;
//
//    #[ExcelProperty(value: "创建时间", index: 48)]
//    public string $created_at;
//
//    #[ExcelProperty(value: "客户属性", index: 49)]
//    public string $ke_hu_shu_xing;
//
//    #[ExcelProperty(value: "客户编码", index: 50)]
//    public string $ke_hu_bian_ma;
//
//    #[ExcelProperty(value: "更新时间", index: 51)]
//    public string $updated_at;
//
//    #[ExcelProperty(value: "企业属性", index: 52)]
//    public string $xing_ye_fen_lei;
//
//    #[ExcelProperty(value: "删除时间", index: 53)]
//    public string $deleted_at;


}