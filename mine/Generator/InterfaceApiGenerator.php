<?php
/**
 * MineAdmin is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using MineAdmin.
 *
 * @Author kiki
 * @Link   https://gitee.com/xmo/MineAdmin
 */

declare(strict_types=1);
namespace Mine\Generator;

use App\Setting\Model\SettingGenerateColumns;
use App\Setting\Model\SettingGenerateTables;
use App\System\Service\SystemApiService;
use Hyperf\Database\Model\Collection;
use Hyperf\Utils\Filesystem\Filesystem;
use Mine\Exception\NormalStatusException;
use Mine\Helper\Str;

class InterfaceApiGenerator extends MineGenerator implements CodeGenerator
{
    /**
     * @var SettingGenerateTables
     */
    protected SettingGenerateTables $model;

    /**
     * @var string
     */
    protected string $codeContent;

    /**
     * @var Filesystem
     */
    protected Filesystem $filesystem;

    /**
     * @var Collection
     */
    protected Collection $columns;

    /**
     * @var string
     */
    protected string $interfaceApiNamespace;

    /**
     * 设置生成信息
     * @param SettingGenerateTables $model
     * @return InterfaceApiGenerator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function setGenInfo(SettingGenerateTables $model): InterfaceApiGenerator
    {
        $this->model = $model;
        $this->filesystem = make(Filesystem::class);
        if (empty($model->module_name) || empty($model->menu_name)) {
            throw new NormalStatusException(t('setting.gen_code_edit'));
        }
        $name = $model->module_name;
        if ($model->package_name) {
            $name .= "\\".Str::title($this->model->package_name);
        }
        $this->setNamespace($this->model->namespace);
        $this->setInterfaceApiNamespace("Api\\InterfaceApi\\v2\\$name\\Controller");
//        Api\InterfaceApi\v1\System\Controller\

        $this->columns = SettingGenerateColumns::query()
            ->where('table_id', $model->id)->orderByDesc('sort')
            ->get([ 'column_name', 'column_comment' ]);

        return $this->placeholderReplace();
    }

    /**
     * @param mixed $namespace
     */
    public function setInterfaceApiNamespace(string $namespace): void
    {
        $this->interfaceApiNamespace = $namespace;
    }

    /**
     * @return string
     */
    public function getInterfaceApiNamespace(): string
    {
        return $this->interfaceApiNamespace;
    }

    /**
     * 生成代码
     */
    public function generator(): void
    {
        //        Api\InterfaceApi\v1\System\Controller\
        $module = Str::title($this->model->module_name);
        if ($this->model->generate_type == '0') {
            $path = BASE_PATH . "/runtime/generate/php/api/InterfaceApi/v2/{$module}/Controller/";
        } else {
            $path = BASE_PATH . "/api/InterfaceApi/v2/{$module}/Controller/";
        }
        if (!empty($this->model->package_name)) {
            $path .= Str::title($this->model->package_name) . '/';
        }
        $this->filesystem->exists($path) || $this->filesystem->makeDirectory($path, 0755, true, true);
        $this->filesystem->put($path . "{$this->getClassName()}.php", $this->replace()->getCodeContent());

    }

    public function saveApi()
    {
        $data = [];
        /** @var SystemApiService $apiService */
        $apiService = make(SystemApiService::class);
        $apiService->save($data);
    }

    /**
     * @return string
     */
    public function preview(): string
    {
        return $this->replace()->getCodeContent();
    }


    /**
     * 获取模板地址
     * @return string
     */
    protected function getTemplatePath(): string
    {
        return $this->getStubDir().'/interfaceApi.stub';
    }

    /**
     * 读取模板内容
     * @return string
     */
    protected function readTemplate(): string
    {
        return $this->filesystem->sharedGet($this->getTemplatePath());
    }

    /**
     * 占位符替换
     */
    protected function placeholderReplace(): InterfaceApiGenerator
    {
        $this->setCodeContent(str_replace(
            $this->getPlaceHolderContent(),
            $this->getReplaceContent(),
            $this->readTemplate()
        ));

        return $this;
    }

    /**
     * 获取要替换的占位符
     */
    protected function getPlaceHolderContent(): array
    {
        return [
            '{USE}',
            '{NAMESPACE}',
            '{COMMENT}',
            '{CLASS_NAME}',
            '{SERVICE}',
            '{FUNCTIONS}',
            '{DTO_CLASS}',
            '{PK}',
            '{STATUS_VALUE}',
            '{STATUS_FIELD}',
            '{NUMBER_FIELD}',
            '{NUMBER_TYPE}',
            '{NUMBER_VALUE}',
            '{REQUEST}',
            '{CONTROLLER_ROUTE}',
        ];
    }

    /**
     * 获取要替换占位符的内容
     */
    protected function getReplaceContent(): array
    {
        return [
            $this->getUse(),
            $this->initNamespace(),
            $this->getComment(),
            $this->getClassName(),
            $this->getServiceName(),
            $this->getFunctions(),
            $this->getDtoClass(),
            $this->getPk(),
            $this->getStatusValue(),
            $this->getStatusField(),
            $this->getNumberField(),
            $this->getNumberType(),
            $this->getNumberValue(),
            $this->getRequestName(),
            $this->getControllerRoute(),
        ];
    }

    /**
     * 获取控制器路由
     * @return string
     */
    protected function getControllerRoute(): string
    {
        return sprintf(
            '%s/%s',
            Str::lower($this->model->module_name),
            $this->getShortBusinessName()
        );
    }

    /**
     * 获取短业务名称
     * @return string
     */
    public function getShortBusinessName(): string
    {
        return Str::camel(str_replace(
            Str::lower($this->model->module_name),
            '',
            str_replace(env('DB_PREFIX'), '', $this->model->table_name)
        ));
    }

    /**
     * @return string
     */
    protected function getDtoClass(): string
    {
        return sprintf(
            "\%s\Dto\%s::class",
            $this->model->namespace,
            $this->getBusinessName() . 'Dto'
        );
    }

    /**
     * 获取验证器
     * @return string
     */
    protected function getRequestName(): string
    {
        return $this->getBusinessName(). 'Request';
    }

    /**
     * 获取使用的类命名空间
     * @return string
     */
    protected function getUse(): string
    {
        return <<<UseNamespace
use {$this->getNamespace()}\\Service\\{$this->getBusinessName()}Service;
use {$this->getNamespace()}\\Request\\{$this->getBusinessName()}Request;
UseNamespace;
    }

    /**
     * 获取服务类名称
     * @return string
     */
    protected function getServiceName(): string
    {
        return $this->getBusinessName() . 'Service';
    }

    /**
     * 初始化命名空间
     * @return string
     */
    protected function initNamespace(): string
    {
        return $this->getInterfaceApiNamespace();
    }

    /**
     * 获取控制器注释
     * @return string
     */
    protected function getComment(): string
    {
        return $this->model->menu_name. '接口';
    }

    /**
     * 获取类名称
     * @return string
     */
    protected function getClassName(): string
    {
        return $this->getBusinessName().'Controller';
    }


    /**
     * @return string
     */
    protected function getFunctions(): string
    {
        $menus = explode(',', $this->model->generate_menus);
        $otherMenu = [$this->model->type === 'single' ? 'singleList' : 'treeList'];
        if (in_array('recycle', $menus)) {
            $otherMenu[] = $this->model->type === 'single' ? 'singleRecycleList' : 'treeRecycleList';
            array_push($otherMenu, ...['realDelete', 'recovery']);
            unset($menus[array_search('recycle', $menus)]);
        }
        array_unshift($menus, ...$otherMenu);
        $phpCode = '';
        $path = $this->getStubDir() . 'InterfaceApi/';
        foreach ($menus as $menu) {
            $content = $this->filesystem->sharedGet($path . $menu . '.stub');
            $phpCode .= $content;
        }
        return $phpCode;
    }

    /**
     * @return string
     */
    protected function getPk(): string
    {
        return SettingGenerateColumns::query()
            ->where('table_id', $this->model->id)
            ->where('is_pk', '1')
            ->value('column_name');
    }

    /**
     * @return string
     */
    protected function getStatusValue(): string
    {
        return 'statusValue';
    }

    /**
     * @return string
     */
    protected function getStatusField(): string
    {
        return 'statusName';
    }

    /**
     * @return string
     */
    protected function getNumberField(): string
    {
        return 'numberName';
    }

    /**
     * @return string
     */
    protected function getNumberType(): string
    {
        return 'numberType';
    }

    /**
     * @return string
     */
    protected function getNumberValue(): string
    {
        return 'numberValue';
    }

    /**
     * 获取业务名称
     * @return string
     */
    public function getBusinessName(): string
    {
        return Str::studly(str_replace(env('DB_PREFIX'), '', $this->model->table_name));
    }

    /**
     * 设置代码内容
     * @param string $content
     */
    public function setCodeContent(string $content)
    {
        $this->codeContent = $content;
    }

    /**
     * 获取代码内容
     * @return string
     */
    public function getCodeContent(): string
    {
        return $this->codeContent;
    }
}