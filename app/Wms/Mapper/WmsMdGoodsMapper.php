<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsMdGoods;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 商品管理Mapper类
 */
class WmsMdGoodsMapper extends AbstractMapper
{
    /**
     * @var WmsMdGoods
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsMdGoods::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 所属客户
        if (isset($params['suo_shu_ke_hu']) && $params['suo_shu_ke_hu'] !== '') {
            $query->where('suo_shu_ke_hu', '=', $params['suo_shu_ke_hu']);
        }

        // 商品名称
        if (isset($params['shp_ming_cheng']) && $params['shp_ming_cheng'] !== '') {
            $query->where('shp_ming_cheng', '=', $params['shp_ming_cheng']);
        }

        // 存放温层
        if (isset($params['cf_wen_ceng']) && $params['cf_wen_ceng'] !== '') {
            $query->where('cf_wen_ceng', '=', $params['cf_wen_ceng']);
        }

        // 计费商品类
        if (isset($params['jf_shp_lei']) && $params['jf_shp_lei'] !== '') {
            $query->where('jf_shp_lei', '=', $params['jf_shp_lei']);
        }

        // 商品条码
        if (isset($params['shp_tiao_ma']) && $params['shp_tiao_ma'] !== '') {
            $query->where('shp_tiao_ma', '=', $params['shp_tiao_ma']);
        }

        // 保质期
        if (isset($params['bzhi_qi']) && $params['bzhi_qi'] !== '') {
            $query->where('bzhi_qi', '=', $params['bzhi_qi']);
        }

        // 单位
        if (isset($params['shl_dan_wei']) && $params['shl_dan_wei'] !== '') {
            $query->where('shl_dan_wei', '=', $params['shl_dan_wei']);
        }

        // 拆零单位
        if (isset($params['jsh_dan_wei']) && $params['jsh_dan_wei'] !== '') {
            $query->where('jsh_dan_wei', '=', $params['jsh_dan_wei']);
        }

        // 体积
        if (isset($params['ti_ji_cm']) && $params['ti_ji_cm'] !== '') {
            $query->where('ti_ji_cm', '=', $params['ti_ji_cm']);
        }

        // 净重
        if (isset($params['zhl_kg']) && $params['zhl_kg'] !== '') {
            $query->where('zhl_kg', '=', $params['zhl_kg']);
        }

        // 拆零数量
        if (isset($params['chl_shl']) && $params['chl_shl'] !== '') {
            $query->where('chl_shl', '=', $params['chl_shl']);
        }

        // 宽单品
        if (isset($params['ku_dan_pin']) && $params['ku_dan_pin'] !== '') {
            $query->where('ku_dan_pin', '=', $params['ku_dan_pin']);
        }

        // 高单品
        if (isset($params['gao_dan_pin']) && $params['gao_dan_pin'] !== '') {
            $query->where('gao_dan_pin', '=', $params['gao_dan_pin']);
        }

        // 长整箱
        if (isset($params['ch_zh_xiang']) && $params['ch_zh_xiang'] !== '') {
            $query->where('ch_zh_xiang', '=', $params['ch_zh_xiang']);
        }

        // 宽整箱
        if (isset($params['ku_zh_xiang']) && $params['ku_zh_xiang'] !== '') {
            $query->where('ku_zh_xiang', '=', $params['ku_zh_xiang']);
        }

        // 高整箱
        if (isset($params['gao_zh_xiang']) && $params['gao_zh_xiang'] !== '') {
            $query->where('gao_zh_xiang', '=', $params['gao_zh_xiang']);
        }

        // 毛重
        if (isset($params['zhl_kgm']) && $params['zhl_kgm'] !== '') {
            $query->where('zhl_kgm', '=', $params['zhl_kgm']);
        }

        // 商品客户编码
        if (isset($params['SHP_BIAN_MAKH']) && $params['SHP_BIAN_MAKH'] !== '') {
            $query->where('SHP_BIAN_MAKH', '=', $params['SHP_BIAN_MAKH']);
        }

        // 最小库存
        if (isset($params['min_stock']) && $params['min_stock'] !== '') {
            $query->where('min_stock', '=', $params['min_stock']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}