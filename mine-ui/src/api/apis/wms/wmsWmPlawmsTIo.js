import { request } from '@/utils/request.js'

/**
 * 月台管理 API JS
 */

export default {

  /**
   * 获取月台管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/wmPlaTIo/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加月台管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/wmPlaTIo/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新月台管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/wmPlaTIo/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/wmPlaTIo/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取月台管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/wmPlaTIo/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将月台管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/wmPlaTIo/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取月台管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/wmPlaTIo/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复月台管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/wmPlaTIo/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除月台管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/wmPlaTIo/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改月台管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/wmPlaTIo/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改月台管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/wmPlaTIo/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 月台管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/wmPlaTIo/import',
      method: 'post',
      data
    })
  },

  /**
   * 月台管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/wmPlaTIo/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 月台管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/wmPlaTIo/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}
