import { request } from '@/utils/request.js'

/**
 * 平台用户 API JS
 */

export default {

  /**
   * 获取平台用户分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'common/user/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加平台用户
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'common/user/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新平台用户数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'common/user/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'common/user/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取平台用户
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'common/user/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将平台用户删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'common/user/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取平台用户数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'common/user/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复平台用户数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'common/user/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除平台用户
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'common/user/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改平台用户数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'common/user/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改平台用户数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'common/user/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 平台用户导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'common/user/import',
      method: 'post',
      data
    })
  },

  /**
   * 平台用户下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'common/user/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 平台用户导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'common/user/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}