<?php

declare (strict_types=1);
namespace App\System\Model;

use Mine\MineModel;
/**
 * @property int $id 
 * @property string $en_key 英文索引
 * @property string $cn_key 中文索引
 * @property string $remark 日志内容
 * @property \Carbon\Carbon $created_at 
 */
class SystemCommonManageLog extends MineModel
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_common_manage_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'en_key', 'cn_key', 'type','remark', 'created_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'created_at' => 'datetime'];
}