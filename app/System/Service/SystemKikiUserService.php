<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Service;

use App\System\Mapper\SystemKikiUserMapper;
use Mine\Abstracts\AbstractService;

/**
 * kiki一对一服务类
 */
class SystemKikiUserService extends AbstractService
{
    /**
     * @var SystemKikiUserMapper
     */
    public $mapper;

    public function __construct(SystemKikiUserMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}