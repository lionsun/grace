<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Erp\Request;

use Mine\MineFormRequest;

/**
 * 车辆管理验证数据类
 */
class ErpCarRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['number','brand','user_name','color','mileage','engine','frame_number','factory','factory_at','year_at','insurance_at','level','belong','peice','car_status','status','sort',],
             'update' => ['number','brand','user_name','color','mileage','engine','frame_number','factory','factory_at','year_at','insurance_at','level','belong','peice','car_status','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'number.required'=>' 车牌号码不能为空',
'brand.required'=>' 品牌型号不能为空',
'user_name.required'=>' 驾驶员不能为空',
'mileage.numeric'=>' 行驶里程(公里)格式错误',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 车牌号码 验证
            'number' => 'required',
            // 品牌型号 验证
            'brand' => 'required',
            // 驾驶员 验证
            'user_name' => 'required',
            // 行驶里程(公里) 验证
            'mileage' => 'numeric',
            ];
        }

}