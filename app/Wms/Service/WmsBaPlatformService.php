<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Service;

use App\Wms\Mapper\WmsBaPlatformMapper;
use Mine\Abstracts\AbstractService;

/**
 * 月台定义服务类
 */
class WmsBaPlatformService extends AbstractService
{
    /**
     * @var WmsBaPlatformMapper
     */
    public $mapper;

    public function __construct(WmsBaPlatformMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}