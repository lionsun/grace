<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsMdBin;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 库位管理Mapper类
 */
class WmsMdBinMapper extends AbstractMapper
{
    /**
     * @var WmsMdBin
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsMdBin::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        $query->with(['baStore','mdCus']);

        // 库位名称
        if (isset($params['ku_wei_ming_cheng']) && $params['ku_wei_ming_cheng'] !== '') {
            $query->where('ku_wei_ming_cheng', '=', $params['ku_wei_ming_cheng']);
        }

        // 库位编码
        if (isset($params['ku_wei_bian_ma']) && $params['ku_wei_bian_ma'] !== '') {
            $query->where('ku_wei_bian_ma', '=', $params['ku_wei_bian_ma']);
        }

        // 库位条码
        if (isset($params['ku_wei_tiao_ma']) && $params['ku_wei_tiao_ma'] !== '') {
            $query->where('ku_wei_tiao_ma', '=', $params['ku_wei_tiao_ma']);
        }

        // 库区
        if (isset($params['ku_wei_lei_xing']) && $params['ku_wei_lei_xing'] !== '') {
            $query->where('ku_wei_lei_xing', '=', $params['ku_wei_lei_xing']);
        }

        // 库位属性
        if (isset($params['ku_wei_shu_xing']) && $params['ku_wei_shu_xing'] !== '') {
            $query->where('ku_wei_shu_xing', '=', $params['ku_wei_shu_xing']);
        }

        // 上架次序
        if (isset($params['shang_jia_ci_xu']) && $params['shang_jia_ci_xu'] !== '') {
            $query->where('shang_jia_ci_xu', '=', $params['shang_jia_ci_xu']);
        }

        // 取货次序
        if (isset($params['qu_huo_ci_xu']) && $params['qu_huo_ci_xu'] !== '') {
            $query->where('qu_huo_ci_xu', '=', $params['qu_huo_ci_xu']);
        }

        // 仓库
        if (isset($params['bin_store']) && $params['bin_store'] !== '') {
            $query->where('bin_store', '=', $params['bin_store']);
        }

        // 客户
        if (isset($params['suo_shu_ke_hu']) && $params['suo_shu_ke_hu'] !== '') {
            $query->where('suo_shu_ke_hu', '=', $params['suo_shu_ke_hu']);
        }

        // 库位使用状态
        if (isset($params['ku_location_use_status']) && $params['ku_location_use_status'] !== '') {
            $query->where('ku_location_use_status', '=', $params['ku_location_use_status']);
        }

        // 货架
        if (isset($params['ku_shelves']) && $params['ku_shelves'] !== '') {
            $query->where('ku_shelves', '=', $params['ku_shelves']);
        }

        // 货架层数
        if (isset($params['ku_layers']) && $params['ku_layers'] !== '') {
            $query->where('ku_layers', '=', $params['ku_layers']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}