<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsMdCus;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 客户信息Mapper类
 */
class WmsMdCusMapper extends AbstractMapper
{
    /**
     * @var WmsMdCus
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsMdCus::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 中文全称
        if (isset($params['zhong_wen_qch']) && $params['zhong_wen_qch'] !== '') {
            $query->where('zhong_wen_qch', '=', $params['zhong_wen_qch']);
        }

        // 客户编码
        if (isset($params['ke_hu_bian_ma']) && $params['ke_hu_bian_ma'] !== '') {
            $query->where('ke_hu_bian_ma', '=', $params['ke_hu_bian_ma']);
        }

        // 企业属性
        if (isset($params['xing_ye_fen_lei']) && $params['xing_ye_fen_lei'] !== '') {
            $query->where('xing_ye_fen_lei', '=', $params['xing_ye_fen_lei']);
        }

        // 终止合作时间
        if (isset($params['zhong_zhi_he_shi_jian'][0]) && isset($params['zhong_zhi_he_shi_jian'][1])) {
            $query->whereBetween('zhong_zhi_he_shi_jian', [$params['zhong_zhi_he_shi_jian'][0], $params['zhong_zhi_he_shi_jian'][1]]);
        }

        // 客户属性
        if (isset($params['ke_hu_shu_xing']) && $params['ke_hu_shu_xing'] !== '') {
            $query->where('ke_hu_shu_xing', '=', $params['ke_hu_shu_xing']);
        }

        // 地址
        if (isset($params['di_zhi']) && $params['di_zhi'] !== '') {
            $query->where('di_zhi', '=', $params['di_zhi']);
        }

        // 负责人
        if (isset($params['zhu_lian_xi_ren']) && $params['zhu_lian_xi_ren'] !== '') {
            $query->where('zhu_lian_xi_ren', '=', $params['zhu_lian_xi_ren']);
        }

        // 手机
        if (isset($params['shou_ji']) && $params['shou_ji'] !== '') {
            $query->where('shou_ji', '=', $params['shou_ji']);
        }

        // 法人身份证号
        if (isset($params['fa_ren_shen_fen']) && $params['fa_ren_shen_fen'] !== '') {
            $query->where('fa_ren_shen_fen', '=', $params['fa_ren_shen_fen']);
        }

        // 注册资金万元
        if (isset($params['zhu_ce_zi_jin']) && $params['zhu_ce_zi_jin'] !== '') {
            $query->where('zhu_ce_zi_jin', '=', $params['zhu_ce_zi_jin']);
        }

        // 主营业务
        if (isset($params['zhu_ying_ye_wu']) && $params['zhu_ying_ye_wu'] !== '') {
            $query->where('zhu_ying_ye_wu', '=', $params['zhu_ying_ye_wu']);
        }

        // 备注
        if (isset($params['bei_zhu']) && $params['bei_zhu'] !== '') {
            $query->where('bei_zhu', '=', $params['bei_zhu']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}