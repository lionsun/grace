<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Service;

use App\Wms\Mapper\WmsMdGoodsMapper;
use Mine\Abstracts\AbstractService;

/**
 * 商品管理服务类
 */
class WmsMdGoodsService extends AbstractService
{
    /**
     * @var WmsMdGoodsMapper
     */
    public $mapper;

    public function __construct(WmsMdGoodsMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}