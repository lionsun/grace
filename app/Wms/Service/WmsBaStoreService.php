<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Service;

use App\Wms\Mapper\WmsBaStoreMapper;
use Mine\Abstracts\AbstractService;

/**
 * 仓库管理服务类
 */
class WmsBaStoreService extends AbstractService
{
    /**
     * @var WmsBaStoreMapper
     */
    public $mapper;

    public function __construct(WmsBaStoreMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * 获取启用的仓库
     * @param array $params
     * @return array
     */
    public function getActiveList(array $params = []): array
    {
        $params['active_status'] = true;

        return $this->getList($params);
    }
}