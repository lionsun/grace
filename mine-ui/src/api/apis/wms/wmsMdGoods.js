import { request } from '@/utils/request.js'

/**
 * 商品管理 API JS
 */

export default {

  /**
   * 获取商品管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/mdGoods/index',
      method: 'get',
      params
    })
  },

	/**
	 * 启用的数据
	 * @returns
	 */
	list (params = {}) {
		return request({
			url: 'wms/mdGoods/list',
			method: 'get',
			params
		})
	},

  /**
   * 添加商品管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/mdGoods/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新商品管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/mdGoods/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/mdGoods/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取商品管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/mdGoods/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将商品管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/mdGoods/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取商品管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/mdGoods/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复商品管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/mdGoods/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除商品管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/mdGoods/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改商品管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/mdGoods/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改商品管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/mdGoods/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 商品管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/mdGoods/import',
      method: 'post',
      data
    })
  },

  /**
   * 商品管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/mdGoods/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 商品管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/mdGoods/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}
