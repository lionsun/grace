<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsWmImNoticeH;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 入库管理Mapper类
 */
class WmsWmImNoticeHMapper extends AbstractMapper
{
    /**
     * @var WmsWmImNoticeH
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsWmImNoticeH::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        $query->with(['mdGoods','mdBin','mdCus','baPlatform']);
        // 商品
        if (isset($params['goods_id']) && $params['goods_id'] !== '') {
            $query->where('goods_id', '=', $params['goods_id']);
        }

        // 库位
        if (isset($params['md_bin_id']) && $params['md_bin_id'] !== '') {
            $query->where('md_bin_id', '=', $params['md_bin_id']);
        }

        // 客户
        if (isset($params['cus_code']) && $params['cus_code'] !== '') {
            $query->where('cus_code', '=', $params['cus_code']);
        }

        // 预计到货时间
        if (isset($params['im_data'][0]) && isset($params['im_data'][1])) {
            $query->whereBetween('im_data', [$params['im_data'][0], $params['im_data'][1]]);
        }

        // 客户订单号
        if (isset($params['im_cus_code']) && $params['im_cus_code'] !== '') {
            $query->where('im_cus_code', '=', $params['im_cus_code']);
        }

        // 订单类型
        if (isset($params['order_type_code']) && $params['order_type_code'] !== '') {
            $query->where('order_type_code', '=', $params['order_type_code']);
        }

        // 月台
        if (isset($params['platform_code']) && $params['platform_code'] !== '') {
            $query->where('platform_code', '=', $params['platform_code']);
        }

        // 单据状态
        if (isset($params['im_sta']) && $params['im_sta'] !== '') {
            $query->where('im_sta', '=', $params['im_sta']);
        }

        // 
        if (isset($params['READ_ONLY']) && $params['READ_ONLY'] !== '') {
            $query->where('READ_ONLY', '=', $params['READ_ONLY']);
        }

        // 
        if (isset($params['WHERE_CON']) && $params['WHERE_CON'] !== '') {
            $query->where('WHERE_CON', '=', $params['WHERE_CON']);
        }

        // 供应商编码
        if (isset($params['sup_code']) && $params['sup_code'] !== '') {
            $query->where('sup_code', '=', $params['sup_code']);
        }

        // 供应商名称
        if (isset($params['sup_name']) && $params['sup_name'] !== '') {
            $query->where('sup_name', '=', $params['sup_name']);
        }

        // 对接单据类型
        if (isset($params['pi_class']) && $params['pi_class'] !== '') {
            $query->where('pi_class', '=', $params['pi_class']);
        }

        // 账套
        if (isset($params['pi_master']) && $params['pi_master'] !== '') {
            $query->where('pi_master', '=', $params['pi_master']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}