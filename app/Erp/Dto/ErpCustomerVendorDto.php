<?php
namespace App\Erp\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 供应商信息Dto （导入导出）
 */
#[ExcelData]
class ErpCustomerVendorDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "名称", index: 1)]
    public string $name;

    #[ExcelProperty(value: "联系人", index: 2)]
    public string $user_name;

    #[ExcelProperty(value: "手机号码", index: 3)]
    public string $phone;

    #[ExcelProperty(value: "联系电话", index: 4)]
    public string $user_phone;

    #[ExcelProperty(value: "电子邮箱", index: 5)]
    public string $email;

    #[ExcelProperty(value: "传真", index: 6)]
    public string $fax;

    #[ExcelProperty(value: "期初应收", index: 7)]
    public string $opening_money;

    #[ExcelProperty(value: "期末应收", index: 8)]
    public string $end_money;

    #[ExcelProperty(value: "纳税人识别号", index: 9)]
    public string $tax_iden_num;

    #[ExcelProperty(value: "税率(%)", index: 10)]
    public string $tax_rate;

    #[ExcelProperty(value: "开户行", index: 11)]
    public string $bank;

    #[ExcelProperty(value: "账号", index: 12)]
    public string $account;

    #[ExcelProperty(value: "状态", index: 13)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 14)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 15)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 16)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 17)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 18)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 19)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 20)]
    public string $deleted_at;


}