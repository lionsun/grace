<?php
namespace App\System\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 测试5Dto （导入导出）
 */
#[ExcelData]
class SystemKikiTest5Dto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "用户名", index: 1)]
    public string $username;

    #[ExcelProperty(value: "图片", index: 2)]
    public string $image;

    #[ExcelProperty(value: "文件", index: 3)]
    public string $file;

    #[ExcelProperty(value: "性别", index: 4)]
    public string $sex;

    #[ExcelProperty(value: "爱好", index: 5)]
    public string $like;

    #[ExcelProperty(value: "创建者", index: 6)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 7)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 8)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 9)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 10)]
    public string $deleted_at;

    #[ExcelProperty(value: "多图", index: 11)]
    public string $pic;

    #[ExcelProperty(value: "内容", index: 12)]
    public string $content;

    #[ExcelProperty(value: "商家类型", index: 13)]
    public string $type;

    #[ExcelProperty(value: "状态", index: 14)]
    public string $status;

    #[ExcelProperty(value: "状态2", index: 15)]
    public string $status2;

    #[ExcelProperty(value: "订单状态", index: 16)]
    public string $order_status;

    #[ExcelProperty(value: "评分", index: 17)]
    public string $rate;

    #[ExcelProperty(value: "颜色", index: 18)]
    public string $color;

    #[ExcelProperty(value: "进度条", index: 19)]
    public string $pro;

    #[ExcelProperty(value: "省", index: 20)]
    public string $province;

    #[ExcelProperty(value: "市", index: 21)]
    public string $city;

    #[ExcelProperty(value: "区", index: 22)]
    public string $area;

    #[ExcelProperty(value: "省市区", index: 23)]
    public string $address;

    #[ExcelProperty(value: "多行表单", index: 24)]
    public string $multiLineList;

    #[ExcelProperty(value: "用户ids", index: 25)]
    public string $users;

//    #[ExcelProperty(value: "用户id", index: 26)]
//    public string $user_id;


}