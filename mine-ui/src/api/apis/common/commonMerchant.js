import { request } from '@/utils/request.js'

/**
 * 商家管理 API JS
 */

export default {

  /**
   * 获取商家管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'common/merchant/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加商家管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'common/merchant/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新商家管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'common/merchant/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'common/merchant/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取商家管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'common/merchant/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将商家管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'common/merchant/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取商家管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'common/merchant/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复商家管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'common/merchant/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除商家管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'common/merchant/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改商家管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'common/merchant/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改商家管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'common/merchant/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 商家管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'common/merchant/import',
      method: 'post',
      data
    })
  },

  /**
   * 商家管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'common/merchant/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 商家管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'common/merchant/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}