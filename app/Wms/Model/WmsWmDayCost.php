<?php

declare (strict_types=1);
namespace App\Wms\Model;

use Hyperf\Database\Model\Relations\HasOne;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;
/**
 * @property string $id 主键
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $cus_code 客户
 * @property string $cus_name 客户名称
 * @property string $coswms_t_code 费用
 * @property string $coswms_t_name 费用名称
 * @property string $coswms_t_data 费用日期
 * @property string $day_coswms_t_yj 每日费用
 * @property string $day_coswms_t_bhs 不含税价
 * @property string $day_coswms_t_se 税额
 * @property string $day_coswms_t_hsj 含税价
 * @property string $coswms_t_ori 费用来源
 * @property string $beizhu 备注
 * @property string $coswms_t_sta 状态
 * @property string $coswms_t_sl 计费数量
 * @property string $coswms_t_unit 数量单位
 * @property string $coswms_t_js 是否已结算
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsWmDayCost extends MineModel
{
    use SoftDeletes;
    use Snowflake;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_wm_day_cost';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sys_org_code', 'sys_company_code', 'cus_code', 'cus_name', 'coswms_t_code', 'coswms_t_name', 'coswms_t_data', 'day_coswms_t_yj', 'day_coswms_t_bhs', 'day_coswms_t_se', 'day_coswms_t_hsj', 'coswms_t_ori', 'beizhu', 'coswms_t_sta', 'coswms_t_sl', 'coswms_t_unit', 'coswms_t_js', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string','status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer',
        'created_by' => 'integer', 'updated_by' => 'integer', 'created_at' => 'datetime',
        'updated_at' => 'datetime','coswms_t_js' => 'string'
    ];

    /**
     * 客户
     * @return HasOne
     */
    public function MdCus(): HasOne
    {
        return $this->hasOne(WmsMdCus::class, 'id', 'cus_code');
    }
}