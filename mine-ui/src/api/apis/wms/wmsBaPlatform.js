import { request } from '@/utils/request.js'

/**
 * 月台定义 API JS
 */

export default {

  /**
   * 获取月台定义分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/baPlatform/index',
      method: 'get',
      params
    })
  },

	/**
	 * 获取月台管理列表
	 * @returns
	 */
	list (params = {}) {
		return request({
			url: 'wms/baPlatform/list',
			method: 'get',
			params
		})
	},

  /**
   * 添加月台定义
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/baPlatform/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新月台定义数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/baPlatform/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/baPlatform/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取月台定义
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/baPlatform/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将月台定义删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/baPlatform/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取月台定义数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/baPlatform/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复月台定义数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/baPlatform/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除月台定义
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/baPlatform/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改月台定义数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/baPlatform/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改月台定义数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/baPlatform/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 月台定义导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/baPlatform/import',
      method: 'post',
      data
    })
  },

  /**
   * 月台定义下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/baPlatform/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 月台定义导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/baPlatform/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}
