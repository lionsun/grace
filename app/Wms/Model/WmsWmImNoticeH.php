<?php

declare (strict_types=1);
namespace App\Wms\Model;

use Hyperf\Database\Model\Relations\HasOne;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;
/**
 * @property string $id 主键
 * @property string $goods_id 商品id
 * @property string $md_bin_id 库位id
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $cus_code 客户编码
 * @property string $im_data 预计到货时间
 * @property string $im_cus_code 客户订单号
 * @property string $im_car_dri 司机
 * @property string $im_car_mobile 司机电话
 * @property string $im_car_no 车号
 * @property string $order_type_code 订单类型
 * @property string $platform_code 月台
 * @property string $im_beizhu 备注
 * @property string $im_sta 单据状态
 * @property string $notice_id 进货通知单号
 * @property string $appendix 附件
 * @property string $READ_ONLY 
 * @property string $WHERE_CON 
 * @property string $sup_code 供应商编码
 * @property string $sup_name 供应商名称
 * @property string $pi_class 对接单据类型
 * @property string $pi_master 账套
 * @property string $area_code 存放库区
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsWmImNoticeH extends MineModel
{
    use SoftDeletes;
    use Snowflake;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_wm_im_notice_h';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'goods_id', 'md_bin_id', 'sys_org_code', 'sys_company_code', 'cus_code', 'im_data', 'im_cus_code', 'im_car_dri', 'im_car_mobile', 'im_car_no', 'order_type_code', 'platform_code', 'im_beizhu', 'im_sta', 'notice_id', 'appendix', 'READ_ONLY', 'WHERE_CON', 'sup_code', 'sup_name', 'pi_class', 'pi_master', 'area_code', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string', 'cus_code' => 'string', 'goods_id' => 'string', 'md_bin_id' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'
    ];

    /**
     * 商品
     * @return HasOne
     */
    public function mdGoods(): HasOne
    {
        return $this->hasOne(WmsMdGoods::class, 'id', 'goods_id');
    }

    /**
     * 库位
     * @return HasOne
     */
    public function mdBin(): HasOne
    {
        return $this->hasOne(WmsMdBin::class, 'id', 'md_bin_id');
    }

    /**
     * 客户
     * @return HasOne
     */
    public function mdCus(): HasOne
    {
        return $this->hasOne(WmsMdCus::class, 'id', 'cus_code');
    }

    /**
     * 月台
     * @return HasOne
     */
    public function baPlatform(): HasOne
    {
        return $this->hasOne(WmsBaPlatform::class, 'id', 'platform_code');
    }
}