<?php
namespace App\Crm\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 客户管理Dto （导入导出）
 */
#[ExcelData]
class CrmUserDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "销售阶段", index: 1)]
    public string $sales_stag;

    #[ExcelProperty(value: "跟进人", index: 2)]
    public string $follow_user;

    #[ExcelProperty(value: "客户名称", index: 3)]
    public string $name;

    #[ExcelProperty(value: "客户来源", index: 4)]
    public string $from;

    #[ExcelProperty(value: "需求级别", index: 5)]
    public string $level;

    #[ExcelProperty(value: "官网", index: 6)]
    public string $com;

    #[ExcelProperty(value: "电话", index: 7)]
    public string $phone;

    #[ExcelProperty(value: "邮箱", index: 8)]
    public string $email;

    #[ExcelProperty(value: "法人", index: 9)]
    public string $legal_perso;

    #[ExcelProperty(value: "注册资本", index: 10)]
    public string $money;

    #[ExcelProperty(value: "公司类型", index: 11)]
    public string $type;

    #[ExcelProperty(value: "公司规模", index: 12)]
    public string $scale;

    #[ExcelProperty(value: "所属行业", index: 13)]
    public string $industry;

    #[ExcelProperty(value: "经营范围", index: 14)]
    public string $range;

    #[ExcelProperty(value: "地址", index: 15)]
    public string $address;

    #[ExcelProperty(value: "详细地址", index: 16)]
    public string $address_detail;

    #[ExcelProperty(value: "开户银行", index: 17)]
    public string $bank;

    #[ExcelProperty(value: "银行账号", index: 18)]
    public string $bank_number;

    #[ExcelProperty(value: "姓名", index: 19)]
    public string $user_name;

    #[ExcelProperty(value: "性别", index: 20)]
    public string $sex;

    #[ExcelProperty(value: "职位", index: 21)]
    public string $position;

    #[ExcelProperty(value: "手机", index: 22)]
    public string $user_phone;

    #[ExcelProperty(value: "邮箱", index: 23)]
    public string $user_email;

    #[ExcelProperty(value: "微信", index: 24)]
    public string $user_wechat;

    #[ExcelProperty(value: "钉钉", index: 25)]
    public string $user_ding;

    #[ExcelProperty(value: "QQ", index: 26)]
    public string $user_qq;

    #[ExcelProperty(value: "状态", index: 27)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 28)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 29)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 30)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 31)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 32)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 33)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 34)]
    public string $deleted_at;


}