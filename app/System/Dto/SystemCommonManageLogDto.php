<?php
namespace App\System\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 业务日志Dto （导入导出）
 */
#[ExcelData]
class SystemCommonManageLogDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "英文索引", index: 1)]
    public string $en_key;

    #[ExcelProperty(value: "中文索引", index: 2)]
    public string $cn_key;

    #[ExcelProperty(value: "日志内容", index: 3)]
    public string $remark;

    #[ExcelProperty(value: "created_at", index: 4)]
    public string $created_at;


}