<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 车辆管理Dto （导入导出）
 */
#[ExcelData]
class WmsTmsWmsMdCheliangDto implements MineModelExcel
{
    #[ExcelProperty(value: "主键", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "流程状态", index: 3)]
    public string $bpm_status;

    #[ExcelProperty(value: "车牌号", index: 4)]
    public string $chepaihao;

    #[ExcelProperty(value: "车型", index: 5)]
    public string $chexing;

    #[ExcelProperty(value: "颜色", index: 6)]
    public string $color;

    #[ExcelProperty(value: "最大体积", index: 7)]
    public string $zuidatiji;

    #[ExcelProperty(value: "载重", index: 8)]
    public string $zaizhong;

    #[ExcelProperty(value: "载人数", index: 9)]
    public string $zairen;

    #[ExcelProperty(value: "准假驾照", index: 10)]
    public string $jiazhao;

    #[ExcelProperty(value: "是否可用", index: 11)]
    public string $zhuangtai;

    #[ExcelProperty(value: "备注", index: 12)]
    public string $beizhu;

    #[ExcelProperty(value: "默认司机", index: 13)]
    public string $username;

    #[ExcelProperty(value: "gps", index: 14)]
    public string $gpsid;

    #[ExcelProperty(value: "quyu", index: 15)]
    public string $quyu;

    #[ExcelProperty(value: "状态", index: 16)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 17)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 18)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 19)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 20)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 21)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 22)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 23)]
    public string $deleted_at;


}