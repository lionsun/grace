<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Request;

use Mine\MineFormRequest;

/**
 * 客户信息验证数据类
 */
class ErpCustomerRequest extends MineFormRequest
{
    /**
     * 验证场景
     */
    public $scenes = [
        'create' => ['name', 'user_name', 'phone', 'user_phone', 'email', 'fax', 'opening_money', 'end_money', 'tax_iden_num', 'tax_rate', 'bank', 'account', 'status', 'sort',],
        'update' => ['name', 'user_name', 'phone', 'user_phone', 'email', 'fax', 'opening_money', 'end_money', 'tax_iden_num', 'tax_rate', 'bank', 'account', 'status', 'sort',],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'name.required' => ' 名称不能为空',
            'phone.required' => ' 手机号码不能为空',
            'phone.phone' => ' 手机号码格式错误',
            'user_phone.phone' => ' 联系电话格式错误',
            'email.email' => ' 电子邮箱格式错误',
            'opening_money.numeric' => ' 期初应收格式错误',
            'end_money.numeric' => ' 期末应收格式错误',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [

            // 名称 验证
            'name' => 'required',
            // 手机号码 验证
            'phone' => 'required|phone',
            // 联系电话 验证
            'user_phone' => 'phone',
            // 电子邮箱 验证
            'email' => 'email',
            // 期初应收 验证
            'opening_money' => 'numeric',
            // 期末应收 验证
            'end_money' => 'numeric',
        ];
    }

}