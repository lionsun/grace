<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 商品管理Dto （导入导出）
 */
#[ExcelData]
class WmsMdGoodsDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "所属客户", index: 3)]
    public string $suo_shu_ke_hu;

    #[ExcelProperty(value: "商品名称", index: 4)]
    public string $shp_ming_cheng;

    #[ExcelProperty(value: "商品简称", index: 5)]
    public string $shp_jian_cheng;

    #[ExcelProperty(value: "商品编码", index: 6)]
    public string $shp_bian_ma;

    #[ExcelProperty(value: "商品型号", index: 7)]
    public string $shp_xing_hao;

    #[ExcelProperty(value: "商品规格", index: 8)]
    public string $shp_gui_ge;

    #[ExcelProperty(value: "商品颜色", index: 9)]
    public string $shp_yan_se;

    #[ExcelProperty(value: "产品属性", index: 10)]
    public string $chp_shu_xing;

    #[ExcelProperty(value: "存放温层", index: 11)]
    public string $cf_wen_ceng;

    #[ExcelProperty(value: "拆零控制", index: 12)]
    public string $chl_kong_zhi;

    #[ExcelProperty(value: "码盘单层数量", index: 13)]
    public string $mp_dan_ceng;

    #[ExcelProperty(value: "码盘层高", index: 14)]
    public string $mp_ceng_gao;

    #[ExcelProperty(value: "计费商品类", index: 15)]
    public string $jf_shp_lei;

    #[ExcelProperty(value: "商品品牌", index: 16)]
    public string $shp_pin_pai;

    #[ExcelProperty(value: "商品条码", index: 17)]
    public string $shp_tiao_ma;

    #[ExcelProperty(value: "品牌图片", index: 18)]
    public string $pp_tu_pian;

    #[ExcelProperty(value: "保质期", index: 19)]
    public string $bzhi_qi;

    #[ExcelProperty(value: "单位", index: 20)]
    public string $shl_dan_wei;

    #[ExcelProperty(value: "拆零单位", index: 21)]
    public string $jsh_dan_wei;

    #[ExcelProperty(value: "体积", index: 22)]
    public string $ti_ji_cm;

    #[ExcelProperty(value: "净重", index: 23)]
    public string $zhl_kg;

    #[ExcelProperty(value: "拆零数量", index: 24)]
    public string $chl_shl;

    #[ExcelProperty(value: "件数与体积比", index: 25)]
    public string $jti_ji_bi;

//    #[ExcelProperty(value: "件数与毛重比", index: 26)]
//    public string $jm_zhong_bi;
//
//    #[ExcelProperty(value: "件数与净重比", index: 27)]
//    public string $jj_zhong_bi;
//
//    #[ExcelProperty(value: "尺寸单位", index: 28)]
//    public string $chc_dan_wei;
//
//    #[ExcelProperty(value: "长单品", index: 29)]
//    public string $ch_dan_pin;
//
//    #[ExcelProperty(value: "宽单品", index: 30)]
//    public string $ku_dan_pin;
//
//    #[ExcelProperty(value: "高单品", index: 31)]
//    public string $gao_dan_pin;
//
//    #[ExcelProperty(value: "长整箱", index: 32)]
//    public string $ch_zh_xiang;
//
//    #[ExcelProperty(value: "宽整箱", index: 33)]
//    public string $ku_zh_xiang;
//
//    #[ExcelProperty(value: "高整箱", index: 34)]
//    public string $gao_zh_xiang;
//
//    #[ExcelProperty(value: "商品描述", index: 35)]
//    public string $shp_miao_shu;
//
//    #[ExcelProperty(value: "停用", index: 36)]
//    public string $zhuang_tai;
//
//    #[ExcelProperty(value: "毛重", index: 37)]
//    public string $zhl_kgm;
//
//    #[ExcelProperty(value: "商品客户编码", index: 38)]
//    public string $SHP_BIAN_MAKH;
//
//    #[ExcelProperty(value: "基准温度", index: 39)]
//    public string $JIZHUN_WENDU;
//
//    #[ExcelProperty(value: "最小库存", index: 44)]
//    public string $min_stock;
//
//    #[ExcelProperty(value: "状态", index: 45)]
//    public string $status;
//
//    #[ExcelProperty(value: "排序", index: 46)]
//    public string $sort;
//
//    #[ExcelProperty(value: "商户id", index: 47)]
//    public string $mer_id;
//
//    #[ExcelProperty(value: "创建者", index: 48)]
//    public string $created_by;
//
//    #[ExcelProperty(value: "更新者", index: 49)]
//    public string $updated_by;
//
//    #[ExcelProperty(value: "创建时间", index: 50)]
//    public string $created_at;
//
//    #[ExcelProperty(value: "更新时间", index: 51)]
//    public string $updated_at;
//
//    #[ExcelProperty(value: "删除时间", index: 52)]
//    public string $deleted_at;


}