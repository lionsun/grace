<?php

declare (strict_types=1);

namespace App\System\Model;

use Hyperf\Database\Model\SoftDeletes;
use Mine\MineModel;

/**
 * @property int $id
 * @property string $username
 * @property string $phone
 */
class SystemKikiUser extends MineModel
{
    use SoftDeletes;

    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_kiki_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'username', 'phone', 'address_id'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer'];

    /**
     * 用户
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function address(): \Hyperf\Database\Model\Relations\HasOne
    {
        return $this->hasOne(SystemKikiUserAddress::class, 'id', 'address_id');
    }

//    public function kiki()
//    {
//        return $this->belongsTo(SystemKikiTest5::class, 'id', 'user_id');
//    }
}