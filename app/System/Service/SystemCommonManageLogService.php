<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Service;

use App\System\Mapper\SystemCommonManageLogMapper;
use Mine\Abstracts\AbstractService;

/**
 * 业务日志服务类
 */
class SystemCommonManageLogService extends AbstractService
{
    /**
     * @var SystemCommonManageLogMapper
     */
    public $mapper;

    public function __construct(SystemCommonManageLogMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * 获取列表数据（带分页）
     * @param array|null $params
     * @param bool $isScope
     * @return array
     */
    public function getPageList(?array $params = null, bool $isScope = true): array
    {
        return parent::getPageList($params, $isScope);
    }
}