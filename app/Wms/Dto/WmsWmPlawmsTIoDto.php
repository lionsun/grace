<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 月台管理Dto （导入导出）
 */
#[ExcelData]
class WmsWmPlawmsTIoDto implements MineModelExcel
{
    #[ExcelProperty(value: "主键", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "月台", index: 3)]
    public string $plawms_t_id;

    #[ExcelProperty(value: "车牌号", index: 4)]
    public string $carno;

    #[ExcelProperty(value: "单据编号", index: 5)]
    public string $doc_id;

    #[ExcelProperty(value: "计划进入时间", index: 6)]
    public string $plan_indata;

    #[ExcelProperty(value: "计划驶出时间", index: 7)]
    public string $plan_outdata;

    #[ExcelProperty(value: "月台状态", index: 8)]
    public string $plawms_t_sta;

    #[ExcelProperty(value: "进入时间", index: 9)]
    public string $in_data;

    #[ExcelProperty(value: "驶出时间", index: 10)]
    public string $ouwms_t_data;

    #[ExcelProperty(value: "备注", index: 11)]
    public string $plawms_t_beizhu;

    #[ExcelProperty(value: "月台操作", index: 12)]
    public string $plawms_t_oper;

    #[ExcelProperty(value: "状态", index: 13)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 14)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 15)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 16)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 17)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 18)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 19)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 20)]
    public string $deleted_at;


}