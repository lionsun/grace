<?php

declare (strict_types=1);
namespace App\Common\Model;

use App\Common\Service\Tool\ConstantsService;
use Carbon\Carbon;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\DbConnection\Db;
use Hyperf\Snowflake\Concern\Snowflake;
use Hyperf\Validation\Rule;
use Mine\Exception\NormalStatusException;
use Mine\MineModel;
/**
 * @property varchar $id 用户id
 * @property int $mer_id 商家id
 * @property string $account 用户账号
 * @property string $pwd 用户密码
 * @property string $pwd_hash 用户密码hash
 * @property string $real_name 真实姓名
 * @property int $birthday 生日
 * @property string $card_id 身份证号码
 * @property string $mark 用户备注
 * @property int $partner_id 合伙人id
 * @property int $group_id 用户分组id
 * @property string $nickname 用户昵称
 * @property string $avatar 用户头像
 * @property string $phone 手机号码
 * @property string $add_ip 添加ip
 * @property string $last_time 最后一次登录时间
 * @property string $last_ip 最后一次登录ip
 * @property string $now_money 用户余额
 * @property string $brokerage_price 佣金金额
 * @property int $integral 用户剩余积分
 * @property string $exp 会员经验
 * @property int $sign_num 连续签到天数
 * @property int $status 1为正常，0为禁止
 * @property int $level 等级
 * @property int $agent_level 分销等级
 * @property int $spread_open 是否有推广资格
 * @property int $spread_uid 推广员id
 * @property int $spread_time 推广员关联时间
 * @property int $is_promoter 是否为推广员
 * @property int $pay_count 用户购买次数
 * @property int $spread_count 下级人数
 * @property int $clean_time 清理会员时间
 * @property string $address 详细地址
 * @property int $adminid 管理员编号 
 * @property string $login_type 用户登陆类型，h5,wechat,routine
 * @property string $user_type 用户类型，shop,erp,crm
 * @property string $record_phone 记录临时电话
 * @property int $is_money_level 会员来源  0: 购买商品升级   1：花钱购买的会员2: 会员卡领取
 * @property int $is_ever_level 是否永久性会员  0: 非永久会员  1：永久会员
 * @property string $overdue_time 会员到期时间
 * @property string $uniqid 
 * @property int $division_type 代理类型：0普通，1事业部，2代理，3员工
 * @property int $division_status 代理状态
 * @property int $is_division 事业部状态
 * @property int $is_agent 代理状态
 * @property int $is_staff 员工状态
 * @property int $division_id 事业部id
 * @property int $agent_id 代理商id
 * @property int $staff_id 员工id
 * @property int $division_percent 分佣比例
 * @property int $division_change_time 事业部/代理/员工修改时间
 * @property int $division_end_time 事业部/代理/员工结束时间
 * @property int $division_invite 代理商邀请码
 * @property int $sharing_money 分账金额，单位分
 * @property int $map_level 地图角色：1门店商家，2业务员，3代理
 * @property int $map_acting_level 地图代理等级：1全国，2省，3市，4区
 * @property string $map_code 地图代理区域编码
 * @property int $city_level 城市代理等级
 * @property string $city_level_code 城市代理城市area_code
 * @property string $city_level_address 城市代理城市address
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property Carbon $created_at 创建时间
 * @property Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class CommonUser extends MineModel
{
    use SoftDeletes;
    use Snowflake {
        creating as createPrimaryKey;
    }

    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'common_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'mer_id', 'account', 'pwd', 'pwd_hash','real_name', 'birthday', 'card_id', 'mark', 'partner_id',
        'group_id', 'nickname', 'avatar', 'phone', 'add_ip', 'last_time', 'last_ip', 'now_money',
        'brokerage_price', 'integral', 'exp', 'sign_num', 'status', 'level', 'agent_level', 'spread_open',
        'spread_uid', 'spread_time', 'is_promoter', 'pay_count', 'spread_count', 'clean_time', 'address',
        'adminid', 'login_type', 'user_type', 'record_phone', 'is_money_level', 'is_ever_level',
        'overdue_time', 'uniqid', 'division_type', 'division_status', 'is_division', 'is_agent',
        'is_staff', 'division_id', 'agent_id', 'staff_id', 'division_percent', 'division_change_time',
        'division_end_time', 'division_invite', 'sharing_money', 'map_level', 'map_acting_level',
        'map_code', 'city_level', 'city_level_code', 'city_level_address', 'created_by', 'updated_by',
        'created_at', 'updated_at', 'deleted_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'mer_id' => 'integer', 'birthday' => 'datetime', 'partner_id' => 'integer',
        'group_id' => 'integer', 'now_money' => 'decimal:2', 'brokerage_price' => 'decimal:2',
        'integral' => 'integer', 'exp' => 'decimal:2', 'sign_num' => 'integer', 'status' => 'string',
        'level' => 'integer', 'agent_level' => 'integer', 'spread_open' => 'integer',
        'spread_uid' => 'integer', 'spread_time' => 'integer', 'is_promoter' => 'integer',
        'pay_count' => 'integer', 'spread_count' => 'integer', 'clean_time' => 'integer',
        'adminid' => 'integer', 'is_money_level' => 'integer', 'is_ever_level' => 'integer',
        'division_type' => 'integer', 'division_status' => 'integer', 'is_division' => 'integer',
        'is_agent' => 'integer', 'is_staff' => 'integer', 'division_id' => 'integer',
        'agent_id' => 'integer', 'staff_id' => 'integer', 'division_percent' => 'integer',
        'division_change_time' => 'integer', 'division_end_time' => 'integer',
        'division_invite' => 'integer', 'sharing_money' => 'integer', 'map_level' => 'integer',
        'map_acting_level' => 'integer', 'city_level' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime','address' => 'array'
    ];

    // 数据创建与更新时
    public function saving()
    {

        $phone = CommonUser::query()->where('phone', $this->phone)->first();
        $account = CommonUser::query()->where('account', $this->account)->first();
        // 新增
        if (empty($this->id)) {
            if ($phone) {
                throw new NormalStatusException('手机号已存在');
            }
            if ($account) {
                throw new NormalStatusException('账号已存在');
            }
        } else {
            if ($phone && $phone->id != $this->id) {
                throw new NormalStatusException('手机号已存在');
            }
            if ($account && $account->id != $this->id) {
                throw new NormalStatusException('账号已存在');
            }
        }

        $this->createPrimaryKey();
        if (empty($this->avatar)) {
            $this->avatar = ConstantsService::AVATAR;
        }

        $this->pwd_hash = md5($this->pwd);

    }
}