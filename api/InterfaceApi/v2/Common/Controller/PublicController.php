<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace Api\InterfaceApi\v2\Common\Controller;

use Api\BaseController;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\DeleteMapping;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\PutMapping;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Mine\Annotation\ApiAuth;
use Hyperf\HttpServer\Annotation\Middleware;
use Api\Middleware\ApiMiddleware;
use App\Common\Service\CommonUserService;
use App\Common\Request\CommonUserRequest;

/**
 * 平台用户接口
 */
#[Controller(prefix: "v2/common/public"), Middleware(ApiMiddleware::class)]
class PublicController extends BaseController
{
    /**
     * 业务处理服务
     */
    #[Inject]
    protected CommonUserService $userService;

    /**
     * 帐密登录
     * @return ResponseInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[PostMapping("login")]
    public function login(): ResponseInterface
    {
        $this->validate(CommonUserRequest::class, 'api_create');
        return $this->success($this->userService->login($this->request->all()));
    }

}