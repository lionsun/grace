import { request } from '@/utils/request.js'

/**
 * 供应商信息 API JS
 */

export default {

  /**
   * 获取供应商信息分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/customerVendor/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加供应商信息
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/customerVendor/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新供应商信息数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/customerVendor/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/customerVendor/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取供应商信息
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/customerVendor/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将供应商信息删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/customerVendor/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取供应商信息数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/customerVendor/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复供应商信息数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/customerVendor/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除供应商信息
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/customerVendor/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改供应商信息数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/customerVendor/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改供应商信息数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/customerVendor/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 供应商信息导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/customerVendor/import',
      method: 'post',
      data
    })
  },

  /**
   * 供应商信息下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/customerVendor/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 供应商信息导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/customerVendor/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}