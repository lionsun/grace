<?php
/**
 * MineAdmin is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using MineAdmin.
 *
 * @Author kiki
 * @Link   https://gitee.com/xmo/MineAdmin
 */

declare(strict_types=1);

namespace Mine\Generator\Traits;

trait MapperGeneratorTraits
{
    /**
     * 获取搜索代码
     * @param $column
     * @return string
     */
    protected function getSearchCode($column): string
    {
        return match ($column['query_type']) {
            'neq'     => $this->getSearchPHPString($column['column_name'], '!=', $column['column_comment'],$column['view_type']),
            'gt'      => $this->getSearchPHPString($column['column_name'], '<', $column['column_comment'],$column['view_type']),
            'gte'     => $this->getSearchPHPString($column['column_name'], '<=', $column['column_comment'],$column['view_type']),
            'lt'      => $this->getSearchPHPString($column['column_name'], '>', $column['column_comment'],$column['view_type']),
            'lte'     => $this->getSearchPHPString($column['column_name'], '>=', $column['column_comment'],$column['view_type']),
            'like'    => $this->getSearchPHPString($column['column_name'], 'like', $column['column_comment'],$column['view_type']),
            'between' => $this->getSearchPHPString($column['column_name'], 'between', $column['column_comment'],$column['view_type']),
            default   => $this->getSearchPHPString($column['column_name'], '=', $column['column_comment'],$column['view_type']),
        };
    }

    /**
     * @param $name
     * @param $mark
     * @param $comment
     * @return string
     */
    protected function getSearchPHPString($name, $mark, $comment,$viewType): string
    {
        if ($mark == 'like') {
            return <<<php

        // {$comment}
        if (isset(\$params['{$name}']) && \$params['{$name}'] !== '') {
            \$query->where('{$name}', 'like', '%'.\$params['{$name}'].'%');
        }

php;

        }

        if ($mark == 'between') {
            return <<<php

        // {$comment}
        if (isset(\$params['{$name}'][0]) && isset(\$params['{$name}'][1])) {
            \$query->whereBetween('{$name}', [\$params['{$name}'][0], \$params['{$name}'][1]]);
        }

php;
        }

        if ($viewType == 'tabs') {
            return <<<php

        // {$comment}
        if (isset(\$params['{$name}']) && \$params['{$name}'] >= 0) {
            \$query->where('{$name}', '{$mark}', \$params['{$name}']);
        }

php;
        }

        return <<<php

        // {$comment}
        if (isset(\$params['{$name}']) && \$params['{$name}'] !== '') {
            \$query->where('{$name}', '{$mark}', \$params['{$name}']);
        }

php;
    } // 该方法结束位置
}