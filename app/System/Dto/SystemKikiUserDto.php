<?php
namespace App\System\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * kiki一对一Dto （导入导出）
 */
#[ExcelData]
class SystemKikiUserDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "username", index: 1)]
    public string $username;

    #[ExcelProperty(value: "phone", index: 2)]
    public string $phone;


}