<?php

declare(strict_types=1);

/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/7/8
 * Time: 18:14
 */

use Hyperf\Crontab\Crontab;

return [
    // 是否开启定时任务
    'enable' => true,
    'crontab' => [
        // Callback类型定时任务（默认,每秒执行 秒，分，时，日，月，星期0～6（0表示星期天））
        // (new Crontab())->setName('Foo')->setRule('*/1 * * * * *')->setCallback([App\Common\Task\FooTask::class, 'execute'])->setMemo('这是一个示例的定时任务'),
        // 每1小时执行一次服务监控
        // (new Crontab())->setName('ServiceTask')->setRule('0 0 * * * *')->setCallback([App\Common\Task\ServiceTask::class, 'execute'])->setMemo('服务监控定时任务'),
    ],
];