<?php

declare (strict_types=1);

namespace App\Wms\Model;

use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;

/**
 * @property string $id 主键
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $zhong_wen_qch 中文全称
 * @property string $zhu_ji_ma 助记码
 * @property string $ke_hu_jian_cheng 客户简称
 * @property string $ke_hu_bian_ma 客户编码
 * @property string $ke_hu_ying_wen 客户英文名称
 * @property string $zeng_yong_qi 曾用企业代码
 * @property string $zeng_yong_qi_ye 曾用企业名称
 * @property string $ke_hu_zhuang_tai 客户状态
 * @property string $xing_ye_fen_lei 企业属性
 * @property string $ke_hu_deng_ji 客户等级
 * @property string $suo_shu_xing_ye 所属行业
 * @property string $shou_qian_ri_qi 首签日期
 * @property string $zhong_zhi_he_shi_jian 终止合作时间
 * @property string $shen_qing_shi_jian 申请时间
 * @property string $ke_hu_shu_xing 客户属性
 * @property string $gui_shu_zu_zh 归属组织代码
 * @property string $gui_shu_sheng 归属省份代码
 * @property string $gui_shu_shi_dai 归属市代码
 * @property string $gui_shu 归属县区代码
 * @property string $di_zhi 地址
 * @property string $you_zheng_bian_ma 邮政编码
 * @property string $zhu_lian_xi_ren 主联系人
 * @property string $dian_hua 电话
 * @property string $shou_ji 手机
 * @property string $chuan_zhen 传真
 * @property string $Emaildi_zhi Email地址
 * @property string $wang_ye_di_zhi 网页地址
 * @property string $fa_ren_dai_biao 法人代表
 * @property string $fa_ren_shen_fen 法人身份证号
 * @property string $zhu_ce_zi_jin 注册资金万元
 * @property string $bi_bie 币别
 * @property string $ying_ye_zhi_zhao 营业执照号
 * @property string $shui_wu_deng 税务登记证号
 * @property string $zu_zhi_ji_gou 组织机构代码证
 * @property string $dao_lu_yun_shu 道路运输经营许可证
 * @property string $zhu_ying_ye_wu 主营业务
 * @property string $he_yi_xiang 合作意向
 * @property string $pi_zhun_ji_guan 批准机关
 * @property string $pi_zhun_wen_hao 批准文号
 * @property string $zhu_ce_ri_qi 注册日期
 * @property string $bei_zhu 备注
 * @property string $zhu_lian_xi_ren1 联系人1
 * @property string $dian_hua1 电话1
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsMdCus extends MineModel
{
    use SoftDeletes;
    use Snowflake;

    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_md_cus';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sys_org_code', 'sys_company_code', 'zhong_wen_qch', 'zhu_ji_ma', 'ke_hu_jian_cheng',
        'ke_hu_bian_ma', 'ke_hu_ying_wen', 'zeng_yong_qi', 'zeng_yong_qi_ye', 'ke_hu_zhuang_tai',
        'xing_ye_fen_lei', 'ke_hu_deng_ji', 'suo_shu_xing_ye', 'shou_qian_ri_qi',
        'zhong_zhi_he_shi_jian', 'shen_qing_shi_jian', 'ke_hu_shu_xing',
        'gui_shu_zu_zh', 'gui_shu_sheng', 'gui_shu_shi_dai', 'gui_shu', 'di_zhi', 'you_zheng_bian_ma',
        'zhu_lian_xi_ren', 'dian_hua', 'shou_ji', 'chuan_zhen', 'Emaildi_zhi', 'wang_ye_di_zhi',
        'fa_ren_dai_biao', 'fa_ren_shen_fen', 'zhu_ce_zi_jin', 'bi_bie', 'ying_ye_zhi_zhao',
        'shui_wu_deng', 'zu_zhi_ji_gou', 'dao_lu_yun_shu', 'zhu_ying_ye_wu', 'he_yi_xiang',
        'pi_zhun_ji_guan', 'pi_zhun_wen_hao', 'zhu_ce_ri_qi', 'bei_zhu', 'zhu_lian_xi_ren1',
        'dian_hua1', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at',
        'updated_at', 'deleted_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string', 'xing_ye_fen_lei' => 'string', 'ke_hu_shu_xing' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime',
        'xing_ye_fen_lei' => 'string'
    ];
}