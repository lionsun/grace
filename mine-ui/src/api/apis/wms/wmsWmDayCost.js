import { request } from '@/utils/request.js'

/**
 * 计费管理 API JS
 */

export default {

  /**
   * 获取计费管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/wmDayCost/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加计费管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/wmDayCost/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新计费管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/wmDayCost/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/wmDayCost/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取计费管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/wmDayCost/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将计费管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/wmDayCost/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取计费管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/wmDayCost/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复计费管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/wmDayCost/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除计费管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/wmDayCost/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改计费管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/wmDayCost/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改计费管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/wmDayCost/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 计费管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/wmDayCost/import',
      method: 'post',
      data
    })
  },

  /**
   * 计费管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/wmDayCost/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 计费管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/wmDayCost/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}