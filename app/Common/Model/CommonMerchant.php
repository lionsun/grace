<?php

declare (strict_types=1);
namespace App\Common\Model;

use App\Common\Service\Tool\ConstantsService;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\Exception\NormalStatusException;
use Mine\MineModel;
/**
 * @property int $id 
 * @property int $sys_user_id 系统用户ID
 * @property string $title 店铺名称
 * @property string $video 店铺宣传视频
 * @property string $image 店铺图片
 * @property string $desc 店铺说明
 * @property string $logo 店铺LOGO
 * @property string $name 联系人
 * @property string $tel 联系电话
 * @property string $province 省
 * @property string $city 市
 * @property string $area 区
 * @property string $address 详细地址
 * @property string $old_address 完整地址
 * @property string $service 提供的服务
 * @property string $id_card 身份证号
 * @property string $id_card_a 身份证正面
 * @property string $id_card_b 身份证反面
 * @property string $license 营业执照
 * @property int $status
 * @property int $state 审核状态 1待审核 2已通过 3已拒绝
 * @property string $error_msg 拒绝原因
 * @property int $is_show 是否显示 1是 2否
 * @property int $type 商家类型
 * @property string $lat 纬度
 * @property string $log 经度
 * @property string $service_score 服务评分
 * @property string $goods_score 商品评分
 * @property string $sales_score 售后评分
 * @property int $keep 收藏数
 * @property int $comment 评论数
 * @property string $goods_score_all 商品总分
 * @property string $service_score_all 服务总分
 * @property int $number 商家编号
 * @property string $identification 认证
 * @property string $money 支付金额
 * @property string $pay_money 真实支付金额
 * @property int $pay_type 支付方式
 * @property int $pay_status 支付状态
 * @property string $signPath 签名
 * @property int $weight 权重排序
 * @property string $charge 商家手续费
 * @property string $pay_time 支付时间
 * @property string $expire_time 过期时间
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class CommonMerchant extends MineModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'common_merchant';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sys_user_id',  'title', 'video', 'image', 'desc', 'logo', 'name', 'tel',
        'province', 'city', 'area', 'address', 'old_address', 'service', 'id_card', 'id_card_a',
        'id_card_b', 'license', 'status', 'error_msg', 'is_show', 'type', 'lat', 'log', 'service_score',
        'goods_score', 'sales_score', 'keep', 'comment', 'goods_score_all', 'service_score_all', 'number',
        'identification', 'money', 'pay_money', 'pay_type', 'pay_status', 'signPath', 'weight', 'charge',
        'pay_time', 'expire_time', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at',
        'user_name','password'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer','sys_user_id' => 'integer', 'status' => 'string','state' => 'string',
        'is_show' => 'string', 'type' => 'integer', 'service_score' => 'decimal:2', 'goods_score' => 'decimal:2',
        'sales_score' => 'decimal:2', 'keep' => 'integer', 'comment' => 'integer', 'goods_score_all' => 'decimal:2',
        'service_score_all' => 'decimal:2', 'number' => 'integer', 'pay_type' => 'integer', 'pay_status' => 'integer',
        'weight' => 'integer', 'created_by' => 'integer', 'updated_by' => 'integer', 'created_at' => 'datetime',
        'updated_at' => 'datetime','address' => 'array'
    ];

    public function setAddressAttribute($value)
    {
        $this->attributes['province'] = $value[0] ?? '';
        $this->attributes['city'] = $value[1] ?? '';
        $this->attributes['area'] = $value[2] ?? '';
        $this->attributes['address'] = json_encode($value,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    // 数据创建与更新时
    public function saving()
    {

        if (empty($this->image)) {
            $this->image = ConstantsService::AVATAR;
        }
    }
}