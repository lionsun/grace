<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Mapper;

use App\System\Model\SystemKikiUser;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * kiki一对一Mapper类
 */
class SystemKikiUserMapper extends AbstractMapper
{
    /**
     * @var SystemKikiUser
     */
    public $model;

    public function assignModel()
    {
        $this->model = SystemKikiUser::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id', 'desc');


        // 
        if (isset($params['username']) && $params['username'] !== '') {
            $query->where('username', '=', $params['username']);
        }

        // 
        if (isset($params['phone']) && $params['phone'] !== '') {
            $query->where('phone', '=', $params['phone']);
        }

        return $query;
    }
}