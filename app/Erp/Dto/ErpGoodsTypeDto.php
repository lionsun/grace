<?php
namespace App\Erp\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 商品类别Dto （导入导出）
 */
#[ExcelData]
class ErpGoodsTypeDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "名称", index: 1)]
    public string $name;

    #[ExcelProperty(value: "编号", index: 2)]
    public string $number;

    #[ExcelProperty(value: "上级分类", index: 3)]
    public string $parent_id;

    #[ExcelProperty(value: "备注", index: 4)]
    public string $remark;

    #[ExcelProperty(value: "状态", index: 5)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 6)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 7)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 8)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 9)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 10)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 11)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 12)]
    public string $deleted_at;


}