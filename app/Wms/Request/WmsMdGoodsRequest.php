<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 商品管理验证数据类
 */
class WmsMdGoodsRequest extends MineFormRequest
{
    /**
     * 验证场景
     */
    public $scenes = [
        'create' => ['suo_shu_ke_hu', 'shp_ming_cheng', 'shp_jian_cheng', 'shp_xing_hao', 'shp_gui_ge', 'shp_yan_se', 'chp_shu_xing', 'cf_wen_ceng', 'chl_kong_zhi', 'mp_dan_ceng', 'mp_ceng_gao', 'jf_shp_lei', 'shp_pin_pai', 'shp_tiao_ma', 'pp_tu_pian', 'bzhi_qi', 'shl_dan_wei', 'jsh_dan_wei', 'ti_ji_cm', 'zhl_kg', 'chl_shl', 'jti_ji_bi', 'jm_zhong_bi', 'jj_zhong_bi', 'chc_dan_wei', 'ch_dan_pin', 'ku_dan_pin', 'gao_dan_pin', 'ch_zh_xiang', 'ku_zh_xiang', 'gao_zh_xiang', 'shp_miao_shu', 'zhl_kgm', 'SHP_BIAN_MAKH', 'JIZHUN_WENDU', 'min_stock', 'status', 'sort',],
        'update' => ['suo_shu_ke_hu', 'shp_ming_cheng', 'shp_jian_cheng', 'shp_xing_hao', 'shp_gui_ge', 'shp_yan_se', 'chp_shu_xing', 'cf_wen_ceng', 'chl_kong_zhi', 'mp_dan_ceng', 'mp_ceng_gao', 'jf_shp_lei', 'shp_pin_pai', 'shp_tiao_ma', 'pp_tu_pian', 'bzhi_qi', 'shl_dan_wei', 'jsh_dan_wei', 'ti_ji_cm', 'zhl_kg', 'chl_shl', 'jti_ji_bi', 'jm_zhong_bi', 'jj_zhong_bi', 'chc_dan_wei', 'ch_dan_pin', 'ku_dan_pin', 'gao_dan_pin', 'ch_zh_xiang', 'ku_zh_xiang', 'gao_zh_xiang', 'shp_miao_shu', 'zhl_kgm', 'SHP_BIAN_MAKH', 'JIZHUN_WENDU', 'min_stock', 'status', 'sort',],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'shp_ming_cheng.required' => ' 商品名称不能为空',
            'chp_shu_xing.required' => ' 产品属性不能为空',
            'cf_wen_ceng.required' => ' 存放温层不能为空',
//            'jf_shp_lei.required' => ' 计费商品类不能为空',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [

            // 商品名称 验证
            'shp_ming_cheng' => 'required',
            // 产品属性 验证
            'chp_shu_xing' => 'required',
            // 存放温层 验证
            'cf_wen_ceng' => 'required',
            // 计费商品类 验证
//            'jf_shp_lei' => 'required',
        ];
    }

}