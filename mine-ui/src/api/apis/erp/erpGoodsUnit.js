import { request } from '@/utils/request.js'

/**
 * 商品单位 API JS
 */

export default {

  /**
   * 获取商品单位分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/goodsUnit/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加商品单位
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/goodsUnit/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新商品单位数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/goodsUnit/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/goodsUnit/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取商品单位
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/goodsUnit/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将商品单位删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/goodsUnit/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取商品单位数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/goodsUnit/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复商品单位数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/goodsUnit/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除商品单位
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/goodsUnit/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改商品单位数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/goodsUnit/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改商品单位数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/goodsUnit/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 商品单位导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/goodsUnit/import',
      method: 'post',
      data
    })
  },

  /**
   * 商品单位下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/goodsUnit/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 商品单位导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/goodsUnit/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}