<?php

declare(strict_types=1);

/**
 *登录日志
 */

namespace Api;

use App\System\Model\Debug;
use Hyperf\Context\Context;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Mine\Exception\NormalStatusException;
use Mine\Helper\MineCode;
use Mine\MineApi;
use Mine\MineController;
use Mine\Traits\ControllerTrait;
use Psr\Container\ContainerInterface;
use Hyperf\HttpServer\Router\Dispatched;

class BaseController extends MineController
{

}
