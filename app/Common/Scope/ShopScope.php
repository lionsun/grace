<?php
/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/6/24
 * Time: 14:57
 */

namespace App\Common\Scope;

use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Scope;
use Hyperf\DbConnection\Model\Model;

class ShopScope implements Scope
{
    /**
     * 把约束加到 Eloquent 查询构造中
     *
     */
    public function apply(Builder $builder, $model)
    {
        $builder->where('age', '>', 200);
    }
}