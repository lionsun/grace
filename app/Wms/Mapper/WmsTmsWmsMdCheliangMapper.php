<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsTmsWmsMdCheliang;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 车辆管理Mapper类
 */
class WmsTmsWmsMdCheliangMapper extends AbstractMapper
{
    /**
     * @var WmsTmsWmsMdCheliang
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsTmsWmsMdCheliang::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 车牌号
        if (isset($params['chepaihao']) && $params['chepaihao'] !== '') {
            $query->where('chepaihao', '=', $params['chepaihao']);
        }

        // 车型
        if (isset($params['chexing']) && $params['chexing'] !== '') {
            $query->where('chexing', '=', $params['chexing']);
        }

        // 颜色
        if (isset($params['color']) && $params['color'] !== '') {
            $query->where('color', '=', $params['color']);
        }

        // 载人数
        if (isset($params['zairen']) && $params['zairen'] !== '') {
            $query->where('zairen', '=', $params['zairen']);
        }

        // 备注
        if (isset($params['beizhu']) && $params['beizhu'] !== '') {
            $query->where('beizhu', '=', $params['beizhu']);
        }

        // 默认司机
        if (isset($params['username']) && $params['username'] !== '') {
            $query->where('username', '=', $params['username']);
        }

        // gps
        if (isset($params['gpsid']) && $params['gpsid'] !== '') {
            $query->where('gpsid', '=', $params['gpsid']);
        }

        // 
        if (isset($params['quyu']) && $params['quyu'] !== '') {
            $query->where('quyu', '=', $params['quyu']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 商户id
        if (isset($params['mer_id']) && $params['mer_id'] !== '') {
            $query->where('mer_id', '=', $params['mer_id']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}