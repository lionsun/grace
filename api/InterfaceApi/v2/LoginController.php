<?php

declare(strict_types=1);

namespace Api\InterfaceApi\v2;

use Api\BaseController;
use Api\Middleware\ApiMiddleware;
use App\Common\Service\Tool\JwtService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Mine\Annotation\ApiAuth;

#[Controller(prefix: "v2"),Middleware(ApiMiddleware::class)]
class LoginController extends BaseController
{
    /**
     * 无需校验登录
     * @var bool
     */
    public bool $auth = false;

    /**
     * 账号密码登录
     */
    public function login()
    {
        return 666;
    }

    /**
     * 微信小程序授权登录
     */
    #[RequestMapping("wxLogin")]
    public function wxLogin()
    {
        $code = $this->request->input('code');
        $iv = $this->request->input('iv');
        $encryptedData = $this->request->input('encryptedData');
        $userId = 20;
        /** @var JwtService $jwt */
        $jwt = make(JwtService::class);
        return $jwt->token($userId);
        $token = $this->services->newAuth($code,$iv, $encryptedData);
        if ($token) {
            if (isset($token['key']) && $token['key']) {
                return app('json')->successful('授权成功，请绑定手机号', $token);
            } else {
                return app('json')->successful('登录成功！', ['token' => $token['token'], 'userInfo' => $token['userInfo'], 'expires_time' => $token['params']['exp']]);
            }
        } else
            return app('json')->fail('获取用户访问token失败!');
    }

    /**
     * 手机登录
     */
    public function phoneLogin()
    {
        return 888;
//        return $this->success($this->userService->phoneLogin($request->validated()));

    }
}
