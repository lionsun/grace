<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 计费管理验证数据类
 */
class WmsWmDayCostRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['cus_code','coswms_t_code','coswms_t_name','coswms_t_data','day_coswms_t_yj','day_coswms_t_bhs','day_coswms_t_se','day_coswms_t_hsj','coswms_t_ori','beizhu','coswms_t_sl','coswms_t_unit','coswms_t_js','status','sort',],
             'update' => ['cus_code','coswms_t_code','coswms_t_name','coswms_t_data','day_coswms_t_yj','day_coswms_t_bhs','day_coswms_t_se','day_coswms_t_hsj','coswms_t_ori','beizhu','coswms_t_sl','coswms_t_unit','coswms_t_js','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'cus_code.required'=>' 客户不能为空',
'coswms_t_code.required'=>' 费用不能为空',
'coswms_t_name.required'=>' 费用名称不能为空',
'day_coswms_t_yj.numeric'=>' 每日费用格式错误',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 客户 验证
            'cus_code' => 'required',
            // 费用 验证
            'coswms_t_code' => 'required',
            // 费用名称 验证
            'coswms_t_name' => 'required',
            // 每日费用 验证
            'day_coswms_t_yj' => 'numeric',
            ];
        }

}