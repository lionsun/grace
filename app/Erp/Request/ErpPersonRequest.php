<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Erp\Request;

use Mine\MineFormRequest;

/**
 * 经手人管理验证数据类
 */
class ErpPersonRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['name','type','status','sort',],
             'update' => ['name','type','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'name.required'=>' 姓名不能为空',
'type.required'=>' 类型不能为空',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 姓名 验证
            'name' => 'required',
            // 类型 验证
            'type' => 'required',
            ];
        }

}