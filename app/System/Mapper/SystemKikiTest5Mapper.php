<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Mapper;

use App\System\Model\SystemKikiTest5;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 测试5Mapper类
 */
class SystemKikiTest5Mapper extends AbstractMapper
{
    /**
     * @var SystemKikiTest5
     */
    public $model;

    public function assignModel()
    {
        $this->model = SystemKikiTest5::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
//        $query->orderBy('id','desc');

        
        // 用户名
        if (isset($params['username']) && $params['username'] !== '') {
            $query->where('username', '=', $params['username']);
        }

        // 性别
        if (isset($params['sex']) && $params['sex'] !== '') {
            $query->where('sex', '=', $params['sex']);
        }

        // 爱好
        if (isset($params['like']) && $params['like'] !== '') {
            $query->where('like', '=', $params['like']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        // 商家类型
        if (isset($params['type']) && $params['type'] !== '') {
            $query->where('type', '=', $params['type']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 状态2
        if (isset($params['status2']) && $params['status2'] !== '') {
            $query->where('status2', '=', $params['status2']);
        }

        // 订单状态
        if (isset($params['order_status']) && $params['order_status'] >= 0) {
            $query->where('order_status', '=', $params['order_status']);
        }

        return $query;
    }
}