import { request } from '@/utils/request.js'

/**
 * 商品类别 API JS
 */

export default {

  /**
   * 获取商品类别列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/goodsType/index',
      method: 'get',
      params
    })
  },

  /**
    * 获取商品类别选择树
    * @returns
    */
  tree () {
    return request({
      url: 'erp/goodsType/tree',
      method: 'get'
    })
  },


  /**
   * 添加商品类别
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/goodsType/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新商品类别数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/goodsType/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/goodsType/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取商品类别
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/goodsType/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将商品类别删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/goodsType/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取商品类别数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/goodsType/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复商品类别数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/goodsType/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除商品类别
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/goodsType/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改商品类别数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/goodsType/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改商品类别数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/goodsType/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 商品类别导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/goodsType/import',
      method: 'post',
      data
    })
  },

  /**
   * 商品类别下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/goodsType/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 商品类别导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/goodsType/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}