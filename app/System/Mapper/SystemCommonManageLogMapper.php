<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Mapper;

use App\System\Model\SystemCommonManageLog;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 业务日志Mapper类
 */
class SystemCommonManageLogMapper extends AbstractMapper
{
    /**
     * @var SystemCommonManageLog
     */
    public $model;

    public function assignModel()
    {
        $this->model = SystemCommonManageLog::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 英文索引
        if (isset($params['en_key']) && $params['en_key'] !== '') {
            $query->where('en_key', '=', $params['en_key']);
        }

        // 中文索引
        if (isset($params['cn_key']) && $params['cn_key'] !== '') {
            $query->where('cn_key', '=', $params['cn_key']);
        }

        return $query;
    }
}