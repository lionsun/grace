<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Common\Request;

use Hyperf\Validation\Rule;
use Mine\MineFormRequest;

/**
 * 平台用户验证数据类
 */
class CommonUserRequest extends MineFormRequest
{
    /**
     * 验证场景
     */
    public $scenes = [
        'sys_create' => ['account', 'pwd','phone'],
        'api_create' => ['account', 'pwd'],
        'sys_update' => ['account', 'pwd','phone'],
        'create' => ['avatar', 'account', 'pwd', 'real_name', 'birthday', 'card_id', 'mark', 'nickname', 'phone', 'add_time', 'now_money', 'brokerage_price', 'integral', 'exp', 'sign_num', 'agent_level', 'spread_open', 'spread_time', 'addres', 'user_type', 'division_end_time', 'division_invite',],
        'update' => ['avatar', 'account', 'pwd', 'real_name', 'birthday', 'card_id', 'mark', 'nickname', 'phone', 'add_time', 'now_money', 'brokerage_price', 'integral', 'exp', 'sign_num', 'agent_level', 'spread_open', 'spread_time', 'addres', 'user_type', 'division_end_time', 'division_invite',],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'account.required' => ' 用户账号不能为空',
            'pwd.required' => ' 用户密码不能为空',
            'card_id.required' => ' 身份证号码不能为空',
            'card_id.id_card' => ' 身份证号码格式错误',
            'phone.required' => ' 手机号码不能为空',
            'phone.phone' => ' 手机号码格式错误',
            'phone.unique' => ' 手机号已存在',
            'now_money.required' => ' 用户余额不能为空',
            'now_money.numeric' => ' 用户余额格式错误',
            'brokerage_price.required' => ' 佣金金额不能为空',
            'brokerage_price.numeric' => ' 佣金金额格式错误',
            'user_type.required' => ' 用户类型，shop,erp,crm不能为空',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [

            // 用户账号 验证
            'account' => 'required',
            // 用户密码 验证
            'pwd' => 'required',
            // 身份证号码 验证
            'card_id' => 'required|id_card',
            // 手机号码 验证
            'phone' => ['required','phone'],
            // 用户余额 验证
            'now_money' => 'required|numeric',
            // 佣金金额 验证
            'brokerage_price' => 'required|numeric',
            // 用户类型，shop,erp,crm 验证
            'user_type' => 'required',
        ];
    }

}