import { request } from '@/utils/request.js'

/**
 * 测试5 API JS
 */

export default {

  /**
   * 获取测试5分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'system/kikiTest5/index',
      method: 'get',
      params
    })
  },

   /**
     * 获取测试5列表
     * @returns
     */
    list (params = {}) {
      return request({
        url: 'system/kikiTest5/list',
        method: 'get',
        params
      })
    },

  /**
   * 添加测试5
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'system/kikiTest5/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新测试5数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'system/kikiTest5/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'system/kikiTest5/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取测试5
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'system/kikiTest5/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将测试5删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'system/kikiTest5/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取测试5数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'system/kikiTest5/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复测试5数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'system/kikiTest5/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除测试5
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'system/kikiTest5/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改测试5数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'system/kikiTest5/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改测试5数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'system/kikiTest5/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 测试5导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'system/kikiTest5/import',
      method: 'post',
      data
    })
  },

  /**
   * 测试5下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'system/kikiTest5/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 测试5导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'system/kikiTest5/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}