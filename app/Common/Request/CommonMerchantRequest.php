<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Common\Request;

use Mine\MineFormRequest;

/**
 * 商家管理验证数据类
 */
class CommonMerchantRequest extends MineFormRequest
{
    /**
     * 验证场景
     */
    public $scenes = [
        'sys_create' => ['title','user_name','password'],
        'sys_update' => ['title','user_name'],
        'create' => ['sys_user_id', 'title', 'video', 'image', 'desc', 'logo', 'name', 'tel', 'province', 'city', 'area', 'address', 'old_address', 'service', 'id_card', 'id_card_a', 'id_card_b', 'license', 'status', 'error_msg', 'is_show', 'type', 'lat', 'log', 'service_score', 'goods_score', 'sales_score', 'keep', 'comment', 'goods_score_all', 'service_score_all', 'number', 'identification', 'money', 'pay_money', 'pay_type', 'pay_status', 'signPath', 'weight', 'charge', 'pay_time', 'expire_time',],
        'update' => ['sys_user_id', 'title', 'video', 'image', 'desc', 'logo', 'name', 'tel', 'province', 'city', 'area', 'address', 'old_address', 'service', 'id_card', 'id_card_a', 'id_card_b', 'license', 'status', 'error_msg', 'is_show', 'type', 'lat', 'log', 'service_score', 'goods_score', 'sales_score', 'keep', 'comment', 'goods_score_all', 'service_score_all', 'number', 'identification', 'money', 'pay_money', 'pay_type', 'pay_status', 'signPath', 'weight', 'charge', 'pay_time', 'expire_time',],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'title.required' => ' 店铺名称不能为空',
            'image.required' => ' 店铺图片不能为空',
            'name.required' => ' 联系人不能为空',
            'tel.required' => ' 联系电话不能为空',
            'tel.numeric' => ' 联系电话格式错误',
            'address.required' => ' 详细地址不能为空',
            'old_address.required' => ' 完整地址不能为空',
            'id_card.required' => ' 身份证号不能为空',
            'id_card.id_card' => ' 身份证号格式错误',
            'id_card_a.required' => ' 身份证正面不能为空',
            'id_card_b.required' => ' 身份证反面不能为空',
            'license.required' => ' 营业执照不能为空',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [

            // 店铺名称 验证
            'title' => 'required',
            // 店铺图片 验证
            'image' => 'required',
            // 联系人 验证
            'name' => 'required',
            // 联系电话 验证
            'tel' => 'required|numeric',
            // 详细地址 验证
            'address' => 'required',
            // 完整地址 验证
            'old_address' => 'required',
            // 身份证号 验证
            'id_card' => 'required|id_card',
            // 身份证正面 验证
            'id_card_a' => 'required',
            // 身份证反面 验证
            'id_card_b' => 'required',
            // 营业执照 验证
            'license' => 'required',
        ];
    }

}