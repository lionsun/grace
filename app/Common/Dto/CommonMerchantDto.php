<?php
namespace App\Common\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 商家管理Dto （导入导出）
 */
#[ExcelData]
class CommonMerchantDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "系统用户ID", index: 1)]
    public string $sys_user_id;

    #[ExcelProperty(value: "前台用户ID", index: 2)]
    public string $user_id;

    #[ExcelProperty(value: "店铺名称", index: 3)]
    public string $title;

    #[ExcelProperty(value: "店铺视频", index: 4)]
    public string $video;

    #[ExcelProperty(value: "店铺图片", index: 5)]
    public string $image;

    #[ExcelProperty(value: "店铺说明", index: 6)]
    public string $desc;

    #[ExcelProperty(value: "店铺logo", index: 7)]
    public string $logo;

    #[ExcelProperty(value: "联系人", index: 8)]
    public string $name;

    #[ExcelProperty(value: "联系电话", index: 9)]
    public string $tel;

    #[ExcelProperty(value: "省", index: 10)]
    public string $province;

    #[ExcelProperty(value: "市", index: 11)]
    public string $city;

    #[ExcelProperty(value: "区", index: 12)]
    public string $area;

    #[ExcelProperty(value: "详细地址", index: 13)]
    public string $address;

    #[ExcelProperty(value: "完整地址", index: 14)]
    public string $old_address;

    #[ExcelProperty(value: "提供的服务", index: 15)]
    public string $service;

    #[ExcelProperty(value: "身份证号", index: 16)]
    public string $id_card;

//    #[ExcelProperty(value: "身份证正面", index: 17)]
//    public string $id_card_a;
//
//    #[ExcelProperty(value: "身份证反面", index: 18)]
//    public string $id_card_b;
//
//    #[ExcelProperty(value: "营业执照", index: 19)]
//    public string $license;

//    #[ExcelProperty(value: "审核状态 ", index: 20)]
//    public string $status;
//
//    #[ExcelProperty(value: "拒绝原因", index: 21)]
//    public string $error_msg;
//
//    #[ExcelProperty(value: "显示 ", index: 22)]
//    public string $is_show;
//
//    #[ExcelProperty(value: "商家类型", index: 23)]
//    public string $type;

//    #[ExcelProperty(value: "纬度", index: 24)]
//    public string $lat;
//
//    #[ExcelProperty(value: "经度", index: 25)]
//    public string $log;

//    #[ExcelProperty(value: "服务评分", index: 26)]
//    public string $service_score;
//
//    #[ExcelProperty(value: "商品评分", index: 27)]
//    public string $goods_score;
//
//    #[ExcelProperty(value: "售后评分", index: 28)]
//    public string $sales_score;
//
//    #[ExcelProperty(value: "收藏数", index: 29)]
//    public string $keep;
//
//    #[ExcelProperty(value: "评论数", index: 30)]
//    public string $comment;
//
//    #[ExcelProperty(value: "商品总分", index: 31)]
//    public string $goods_score_all;
//
//    #[ExcelProperty(value: "服务总分", index: 32)]
//    public string $service_score_all;
//
//    #[ExcelProperty(value: "商家编号", index: 33)]
//    public string $number;
//
//    #[ExcelProperty(value: "认证", index: 34)]
//    public string $identification;
//
//    #[ExcelProperty(value: "支付金额", index: 35)]
//    public string $money;

//    #[ExcelProperty(value: "真实支付金额", index: 36)]
//    public string $pay_money;

//    #[ExcelProperty(value: "支付方式", index: 37)]
//    public string $pay_type;

//    #[ExcelProperty(value: "支付状态", index: 38)]
//    public string $pay_status;

//    #[ExcelProperty(value: "签名", index: 39)]
//    public string $signPath;

//    #[ExcelProperty(value: "权重排序", index: 40)]
//    public string $weight;
//
//    #[ExcelProperty(value: "商家手续费", index: 41)]
//    public string $charge;

//    #[ExcelProperty(value: "支付时间", index: 42)]
//    public string $pay_time;
//
//    #[ExcelProperty(value: "过期时间", index: 43)]
//    public string $expire_time;
//
//    #[ExcelProperty(value: "创建者", index: 44)]
//    public string $created_by;
//
//    #[ExcelProperty(value: "更新者", index: 45)]
//    public string $updated_by;

//    #[ExcelProperty(value: "创建时间", index: 46)]
//    public string $created_at;

//    #[ExcelProperty(value: "更新时间", index: 47)]
//    public string $updated_at;
//
//    #[ExcelProperty(value: "删除时间", index: 48)]
//    public string $deleted_at;


}