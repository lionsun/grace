<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Common\Mapper;

use App\Common\Model\CommonUser;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 平台用户Mapper类
 */
class CommonUserMapper extends AbstractMapper
{
    /**
     * @var CommonUser
     */
    public $model;

    public function assignModel()
    {
        $this->model = CommonUser::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 用户id
        if (isset($params['id']) && $params['id'] !== '') {
            $query->where('id', '=', $params['id']);
        }

        // 用户账号
        if (isset($params['account']) && $params['account'] !== '') {
            $query->where('account', '=', $params['account']);
        }

        // 用户密码
        if (isset($params['pwd']) && $params['pwd'] !== '') {
            $query->where('pwd', '=', $params['pwd']);
        }

        // 身份证号码
        if (isset($params['card_id']) && $params['card_id'] !== '') {
            $query->where('card_id', '=', $params['card_id']);
        }

        // 用户昵称
        if (isset($params['nickname']) && $params['nickname'] !== '') {
            $query->where('nickname', 'like', '%'.$params['nickname'].'%');
        }

        // 手机号码
        if (isset($params['phone']) && $params['phone'] !== '') {
            $query->where('phone', '=', $params['phone']);
        }

        // 添加时间
        if (isset($params['add_time']) && $params['add_time'] !== '') {
            $query->where('add_time', '=', $params['add_time']);
        }

        // 用户余额
        if (isset($params['now_money']) && $params['now_money'] !== '') {
            $query->where('now_money', '=', $params['now_money']);
        }

        // 佣金金额
        if (isset($params['brokerage_price']) && $params['brokerage_price'] !== '') {
            $query->where('brokerage_price', '=', $params['brokerage_price']);
        }

        // 用户剩余积分
        if (isset($params['integral']) && $params['integral'] !== '') {
            $query->where('integral', '=', $params['integral']);
        }

        // 会员经验
        if (isset($params['exp']) && $params['exp'] !== '') {
            $query->where('exp', '=', $params['exp']);
        }

        // 连续签到天数
        if (isset($params['sign_num']) && $params['sign_num'] !== '') {
            $query->where('sign_num', '=', $params['sign_num']);
        }

        // 分销等级
        if (isset($params['agent_level']) && $params['agent_level'] !== '') {
            $query->where('agent_level', '=', $params['agent_level']);
        }

        // 是否有推广资格
        if (isset($params['spread_open']) && $params['spread_open'] !== '') {
            $query->where('spread_open', '=', $params['spread_open']);
        }

        // 推广员关联时间
        if (isset($params['spread_time']) && $params['spread_time'] !== '') {
            $query->where('spread_time', '=', $params['spread_time']);
        }

        // 用户类型，shop,erp,crm
        if (isset($params['user_type']) && $params['user_type'] !== '') {
            $query->where('user_type', '=', $params['user_type']);
        }

        // 事业部/代理/员工结束时间
        if (isset($params['division_end_time']) && $params['division_end_time'] !== '') {
            $query->where('division_end_time', '=', $params['division_end_time']);
        }

        // 代理商邀请码
        if (isset($params['division_invite']) && $params['division_invite'] !== '') {
            $query->where('division_invite', '=', $params['division_invite']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}