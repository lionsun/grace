<?php
namespace App\Erp\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 车辆管理Dto （导入导出）
 */
#[ExcelData]
class ErpCarDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "车牌号码", index: 1)]
    public string $number;

    #[ExcelProperty(value: "品牌型号", index: 2)]
    public string $brand;

    #[ExcelProperty(value: "驾驶员", index: 3)]
    public string $user_name;

    #[ExcelProperty(value: "车身颜色", index: 4)]
    public string $color;

    #[ExcelProperty(value: "行驶里程(公里)", index: 5)]
    public string $mileage;

    #[ExcelProperty(value: "发动机型号", index: 6)]
    public string $engine;

    #[ExcelProperty(value: "车架号码", index: 7)]
    public string $frame_number;

    #[ExcelProperty(value: "生产厂家", index: 8)]
    public string $factory;

    #[ExcelProperty(value: "出厂日期", index: 9)]
    public string $factory_at;

    #[ExcelProperty(value: "年检到期", index: 10)]
    public string $year_at;

    #[ExcelProperty(value: "保险到期", index: 11)]
    public string $insurance_at;

    #[ExcelProperty(value: "排放等级", index: 12)]
    public string $level;

    #[ExcelProperty(value: "车辆归属", index: 13)]
    public string $belong;

    #[ExcelProperty(value: "车辆价格", index: 14)]
    public string $peice;

    #[ExcelProperty(value: "车辆状态", index: 15)]
    public string $car_status;

    #[ExcelProperty(value: "状态", index: 16)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 17)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 18)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 19)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 20)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 21)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 22)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 23)]
    public string $deleted_at;


}