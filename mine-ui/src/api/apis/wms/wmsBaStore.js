import { request } from '@/utils/request.js'

/**
 * 仓库管理 API JS
 */

export default {

  /**
   * 获取仓库管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/baStore/index',
      method: 'get',
      params
    })
  },

	/**
	 * 获取仓库管理列表
	 * @returns
	 */
	list (params = {}) {
		return request({
			url: 'wms/baStore/list',
			method: 'get',
			params
		})
	},


  /**
   * 添加仓库管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/baStore/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新仓库管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/baStore/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/baStore/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取仓库管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/baStore/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将仓库管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/baStore/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取仓库管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/baStore/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复仓库管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/baStore/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除仓库管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/baStore/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改仓库管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/baStore/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改仓库管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/baStore/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 仓库管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/baStore/import',
      method: 'post',
      data
    })
  },

  /**
   * 仓库管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/baStore/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 仓库管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/baStore/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}
