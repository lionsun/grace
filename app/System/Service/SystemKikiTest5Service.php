<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Service;

use App\System\Mapper\SystemKikiTest5Mapper;
use Mine\Abstracts\AbstractService;

/**
 * 测试5服务类
 */
class SystemKikiTest5Service extends AbstractService
{
    /**
     * @var SystemKikiTest5Mapper
     */
    public $mapper;

    public function __construct(SystemKikiTest5Mapper $mapper)
    {
        $this->mapper = $mapper;
    }
}