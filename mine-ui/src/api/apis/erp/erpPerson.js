import { request } from '@/utils/request.js'

/**
 * 经手人管理 API JS
 */

export default {

  /**
   * 获取经手人管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/person/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加经手人管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/person/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新经手人管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/person/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/person/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取经手人管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/person/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将经手人管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/person/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取经手人管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/person/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复经手人管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/person/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除经手人管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/person/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改经手人管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/person/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改经手人管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/person/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 经手人管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/person/import',
      method: 'post',
      data
    })
  },

  /**
   * 经手人管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/person/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 经手人管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/person/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}