<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Crm\Request;

use Mine\MineFormRequest;

/**
 * 客户管理验证数据类
 */
class CrmUserRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['sales_stag','follow_user','name','from','level','com','phone','email','legal_perso','money','type','scale','industry','range','address','address_detail','bank','bank_number','user_name','sex','position','user_phone','user_email','user_wechat','user_ding','user_qq','status','sort',],
             'update' => ['sales_stag','follow_user','name','from','level','com','phone','email','legal_perso','money','type','scale','industry','range','address','address_detail','bank','bank_number','user_name','sex','position','user_phone','user_email','user_wechat','user_ding','user_qq','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'name.required'=>' 客户名称不能为空',
'email.email'=>' 邮箱格式错误',
'user_phone.phone'=>' 手机格式错误',
'user_email.email'=>' 邮箱格式错误',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 客户名称 验证
            'name' => 'required',
            // 邮箱 验证
            'email' => 'email',
            // 手机 验证
            'user_phone' => 'phone',
            // 邮箱 验证
            'user_email' => 'email',
            ];
        }

}