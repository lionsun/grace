# 项目介绍

[//]: # (<p align="center">)

[//]: # (    <img src="https://doc.mineadmin.com/logo.svg" width="120" />)

[//]: # (</p>)
<p align="center">
    <a href="https://ma.thlz.xyz" target="_blank">演示</a> |
    <a href="https://hyperf.wiki/2.2/#/" target="_blank">Hyperf官方文档</a> 
</p>

基于Swoole Hyperf框架开发的<b style="color:red">多商户平台</b>，前端使用Vue3.0 + SCUI（基于Element UI），也支持PC和移动端。企业和个人可以免费使用。

开箱则内置一个WMS服务，如果觉着还不错的话，就请点个 ⭐star 支持一下吧，这将是对我最大的支持和鼓励！


## 主要功能
1. 服务监控，基于prometheus + grafana搭建可视化服务监控，动态查看协程总数量、
内存占用、api接口ops等各种实用数据 (<a href="https://zhuanlan.zhihu.com/p/564253491" target="_blank">prometheus + grafana环境搭建</a>)
2. 队列管理，队列基于rabbitMQ服务搭建，由于死信队列的局限，系统统一由rabbitMQ延迟插件实现延迟队列，所以需要安装rabbitMQ延迟插件，
3. 队列日志，监控队列的生产成功、未消费、消费中、消费成功等各个环节
4. 
- 代码生成，前后端代码的生成（php、vue、js）,可配置每个字段的检验规则、生成类型(图片、文件、富文本、文本、进度条、颜色、标签页、状态...)、关联模型等,
- 生成的的下拉、单选框、多选框、switch、标签页，需要修改模型中的 $casts属性相对应的的字段类型为：string (重要),然后重启服务端
6. 数据表设计器，默认生成status(状态)、sort(排序)、mer_id(商户id)、created_by、updated_by、created_at、updated_at、deleted_at，每个表都必须要存在这些字段
7. 多商户，登录页进行注册的用户均为商户，商户标识为mer_id,所以如果一个功能商户平台与总平台共用的话，表必须存在mer_id字段,mer_id默认为0,0表示平台
8. 
- 多端接口，生成的移动端接口位置(用于App、小程序、H5)：Api\InterfaceApi\v2，http访问路径：例如：127.0.0.1:9501/v2/system/kikiTest5/index，
- 接口公用中间件为：Api\Middleware\ApiMiddleware,鉴权中间件为：Mine\Annotation\ApiAuth;

## 环境需求

- Swoole >= 4.6.x  Swoole < 5.0 && 并关闭 `Short Name`
- PHP === 8.0 并开启以下扩展：
    - xlswriter
    - mbstring
    - json
    - pdo
    - openssl
    - redis
    - pcntl
- Mysql v5.7 || TIDB v7.0.0
- Redis >= 4.0
- rabbitMQ  >=3.8.9   <a href="https://zhuanlan.zhihu.com/p/564233274" target="_blank">rabbitMQ环境搭建</a>
- Node  >=14.16.0



## 下载项目

- 项目下载，请确保已经安装了 `Composer`
```shell
git clone https://gitee.com/kikigoper/grace && cd grace
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
composer install
```

## 后端安装
 - 新建数据库，命名为：grace，将<a href="https://pan.baidu.com/s/1XvPfGialVdgILwSq6bJ6yg?pwd=sne8" target="_blank">grace.sql</a>导入数据库
 - 将根目录下的.env.example文件复制到同级目录下并命名为.env,根据需要修改.env配置中的mysql配置、redis配置、rabbitMQ配置，需要确保这3个服务已启动并可连接

在grace目录下打开启动终端，启动项目
```shell
方法1: php bin/hyperf.php start
```
```shell
方法2(热更新启动): php bin/hyperf.php server:watch
```

## 前端安装

请先确保安装了node.js，npm 工具,进入grace目录
```shell
cd mine-ui && npm i
```
如果报以下错，则需要先运行  npm install core-js
```shell
ules/es.error.cause.js in ./node_modules/_@babel_runtime@7.16.7@@babel/runtime/helpers/esm/nonIterableSpread.js
To install it, you can run: npm install --save core-js/modules/es.error.cause.js
```
在mine-ui目录下,将.env.example文件复制到mine-ui目录下并命名为.env,然后启动前端项目

```shell
npm run dev
```

## 正式环境部署
```shell
# 至少需要一个 Hyperf 节点，多个配置多行
upstream hyperf {
    # Hyperf HTTP Server 的 IP 及 端口
    server 127.0.0.1:9501;
}

server {
  # 端口
  listen 80;
  # 域名
  server_name https://ma.thlz.xyz;
  # 日志
  access_log /data/wwwlogs/https://ma.thlz.xyz_nginx_access.log combined;
  error_log /data/wwwlogs/https://ma.thlz.xyz_nginx_error.log debug;

  # 同域根目录前端代码部署,注意：
  location / {
      # 部署的是打包后的dist文件夹，即npm run build运行所生成的dist文件夹：
      root /data/wwwroot/grace/mine-ui/dist;
      try_files $uri $uri/ /index.html;
      index index.html;
  }

  # 支持自定义下划线参数通过header传输
  # underscores_in_headers on;

  # PHP 代码
  location /api/ {
      # 将客户端的 Host 和 IP 信息一并转发到对应节点
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      # 将协议架构转发到对应节点，如果使用非https请改为http
      proxy_set_header X-scheme https;

      # 执行代理访问真实服务器
      proxy_pass http://hyperf/;
  }
}
```

## 常见问题
- 如何使用队列
  - 先安装rabbitMQ服务并安装延迟插件,将.env文件中的AMQP_ENABLE 值设置为true
  - gracePush()为生产消息公用方法
  - 例如：gracePush(new GraceProducer(['order_id' => 123]),0),其中参数1：GraceProducer为生产者,参数2：0为延迟时间，
  注意：GraceProducer为用户自定义的生产者，这里仅为说明，生产者与消费者是需要同时出现的，具体可以参考App\Common\Queue\Consumer\GraceConsumer 和
  App\Common\Queue\Consumer\GraceProducer
- ui图片点击放大乱码解决
  - 把如图的 <img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/uploadfile/20220914/21702099349152.png"> css类中的z-index去除
- 右键表格行可进行快捷操作
  - <img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/uploadfile/20220914/21703132937376.png">
- 定义新的验证规则,App\System\Listener\ValidatorFactoryResolvedListener
- 缓存
  - 由于是多商户架构，代码大部分平台与商户都是共用的，所以设置缓存的时候，记得后缀或前缀带上mer_id,谨记噢！！！
- 数据库模型应用雪花id,因为 Snowflake 中会复写 creating 方法，而用户有需要自己设置 creating 方法时，就会出现无法生成 ID 的问题。这里需要用户按照以下方式自行处理即可。提示：数据库id类型建议设置为 varchar(36)
- 日志使用 elk('test')->info('XXX'); // 需要在 config/autoload/logger.php配置key
```shell
<?php
use Hyperf\Database\Model\Model;
use Hyperf\Snowflake\Concern\Snowflake;

class User extends Model {
   
    use Snowflake {
        creating as createPrimaryKey;
    }
    
    public $incrementing = false;  // 关闭默认的自增
    protected $keyType = 'string'; // 设置主键类型,覆盖Mine\Aspect\SaveAspect切面的写法,使用hyperf自带的雪花id算法

    public function creating()
    {
        $this->createPrimaryKey();
        // Do something ...
    }
}
```


[//]: # (## QQ群)

[//]: # ()
[//]: # (> <img src="https://img.shields.io/badge/Q群-15169734-red.svg" />)

[//]: # ()
[//]: # (## 鸣谢)

> 以下排名不分先后

[hyperf 一款高性能企业级协程框架](https://hyperf.io/)

[SCUI 中后台前端解决方案](https://gitee.com/lolicode/scui)

[swoole PHP协程框架](https://www.swoole.com)

[Element Plus 桌面端组件库](https://element-plus.gitee.io/zh-CN/)

[Mineadmin 后台权限管理](https://gitee.com/xmo/MineAdmin)

## 演示图片

<table>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663580709_N2zw976e.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663580904_ko6YXq4X.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663580941_ZFn8Nzw1.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663580971_wzRASGxK.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663581017_jtw0B9T4.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663581032_guuUdJdU.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663581278_Q2ZI2iuq.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663581286_f5c6gP5z.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663581039_l7ELmQOz.png"></td>
    </tr>
    <tr>
        <td><img src="https://kiki-admin.oss-cn-beijing.aliyuncs.com/images/2022/09/19/image_1663581045_fJROTTBt.png"></td>
    </tr>
</table>

