<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 车辆管理验证数据类
 */
class WmsTmsWmsMdCheliangRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['chepaihao','chexing','color','zuidatiji','zaizhong','zairen','jiazhao','beizhu','username','gpsid','quyu','status','sort',],
             'update' => ['chepaihao','chexing','color','zuidatiji','zaizhong','zairen','jiazhao','beizhu','username','gpsid','quyu','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'chepaihao.required'=>' 车牌号不能为空',
'color.required'=>' 颜色不能为空',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 车牌号 验证
            'chepaihao' => 'required',
            // 颜色 验证
            'color' => 'required',
            ];
        }

}