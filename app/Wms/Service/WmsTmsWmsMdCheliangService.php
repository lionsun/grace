<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Service;

use App\Wms\Mapper\WmsTmsWmsMdCheliangMapper;
use Mine\Abstracts\AbstractService;

/**
 * 车辆管理服务类
 */
class WmsTmsWmsMdCheliangService extends AbstractService
{
    /**
     * @var WmsTmsWmsMdCheliangMapper
     */
    public $mapper;

    public function __construct(WmsTmsWmsMdCheliangMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}