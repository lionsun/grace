<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Mapper;

use App\Erp\Model\ErpCar;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 车辆管理Mapper类
 */
class ErpCarMapper extends AbstractMapper
{
    /**
     * @var ErpCar
     */
    public $model;

    public function assignModel()
    {
        $this->model = ErpCar::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 车牌号码
        if (isset($params['number']) && $params['number'] !== '') {
            $query->where('number', '=', $params['number']);
        }

        // 品牌型号
        if (isset($params['brand']) && $params['brand'] !== '') {
            $query->where('brand', '=', $params['brand']);
        }

        // 驾驶员
        if (isset($params['user_name']) && $params['user_name'] !== '') {
            $query->where('user_name', '=', $params['user_name']);
        }

        // 车身颜色
        if (isset($params['color']) && $params['color'] !== '') {
            $query->where('color', '=', $params['color']);
        }

        // 行驶里程(公里)
        if (isset($params['mileage']) && $params['mileage'] !== '') {
            $query->where('mileage', '=', $params['mileage']);
        }

        // 发动机型号
        if (isset($params['engine']) && $params['engine'] !== '') {
            $query->where('engine', '=', $params['engine']);
        }

        // 车架号码
        if (isset($params['frame_number']) && $params['frame_number'] !== '') {
            $query->where('frame_number', '=', $params['frame_number']);
        }

        // 生产厂家
        if (isset($params['factory']) && $params['factory'] !== '') {
            $query->where('factory', '=', $params['factory']);
        }

        // 出厂日期
        if (isset($params['factory_at']) && $params['factory_at'] !== '') {
            $query->where('factory_at', '=', $params['factory_at']);
        }

        // 年检到期
        if (isset($params['year_at']) && $params['year_at'] !== '') {
            $query->where('year_at', '=', $params['year_at']);
        }

        // 保险到期
        if (isset($params['insurance_at']) && $params['insurance_at'] !== '') {
            $query->where('insurance_at', '=', $params['insurance_at']);
        }

        // 车辆归属
        if (isset($params['belong']) && $params['belong'] !== '') {
            $query->where('belong', '=', $params['belong']);
        }

        // 车辆状态
        if (isset($params['car_status']) && $params['car_status'] !== '') {
            $query->where('car_status', '=', $params['car_status']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}