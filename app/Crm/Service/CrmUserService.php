<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Crm\Service;

use App\Crm\Mapper\CrmUserMapper;
use Mine\Abstracts\AbstractService;

/**
 * 客户管理服务类
 */
class CrmUserService extends AbstractService
{
    /**
     * @var CrmUserMapper
     */
    public $mapper;

    public function __construct(CrmUserMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}