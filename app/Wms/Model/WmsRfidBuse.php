<?php

declare (strict_types=1);
namespace App\Wms\Model;

use Carbon\Carbon;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;
/**
 * @property string $id 
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $bpm_status 流程状态
 * @property string $wms_rfid_type 类型
 * @property string $wms_rfid_buseno 业务编号
 * @property string $wms_rfid_busecont 业务内容
 * @property string $wms_rfid_id1 RFID1
 * @property string $wms_rfid_id2 RFID2
 * @property string $wms_rfid_id3 RFID3
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property Carbon $created_at 创建时间
 * @property Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsRfidBuse extends MineModel
{
    use SoftDeletes;
    use Snowflake;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_rfid_buse';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sys_org_code', 'sys_company_code', 'bpm_status', 'wms_rfid_type', 'wms_rfid_buseno', 'wms_rfid_busecont', 'wms_rfid_id1', 'wms_rfid_id2', 'wms_rfid_id3', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'
    ];
}