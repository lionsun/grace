import { request } from '@/utils/request.js'

/**
 * 库位管理 API JS
 */

export default {

  /**
   * 获取库位管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/mdBin/index',
      method: 'get',
      params
    })
  },

	/**
	 * 启用的数据
	 * @returns
	 */
	list (params = {}) {
		return request({
			url: 'wms/mdBin/list',
			method: 'get',
			params
		})
	},

  /**
   * 添加库位管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/mdBin/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新库位管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/mdBin/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/mdBin/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取库位管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/mdBin/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将库位管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/mdBin/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取库位管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/mdBin/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复库位管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/mdBin/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除库位管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/mdBin/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改库位管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/mdBin/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改库位管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/mdBin/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 库位管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/mdBin/import',
      method: 'post',
      data
    })
  },

  /**
   * 库位管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/mdBin/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 库位管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/mdBin/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}
