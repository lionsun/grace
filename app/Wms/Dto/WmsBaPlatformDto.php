<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 月台定义Dto （导入导出）
 */
#[ExcelData]
class WmsBaPlatformDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "月台名称", index: 3)]
    public string $platform_name;

    #[ExcelProperty(value: "月台代码", index: 4)]
    public string $platform_code;

    #[ExcelProperty(value: "停用", index: 5)]
    public string $platform_del;

    #[ExcelProperty(value: "状态", index: 6)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 7)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 8)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 9)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 10)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 11)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 12)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 13)]
    public string $deleted_at;


}