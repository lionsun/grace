<?php

declare (strict_types=1);
namespace App\Wms\Model;

use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;
/**
 * @property string $id 
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $suo_shu_ke_hu 所属客户
 * @property string $shp_ming_cheng 商品名称
 * @property string $shp_jian_cheng 商品简称
 * @property string $shp_bian_ma 商品编码
 * @property string $shp_xing_hao 商品型号
 * @property string $shp_gui_ge 商品规格
 * @property string $shp_yan_se 商品颜色
 * @property string $chp_shu_xing 产品属性
 * @property string $cf_wen_ceng 存放温层
 * @property string $chl_kong_zhi 拆零控制
 * @property string $mp_dan_ceng 码盘单层数量
 * @property string $mp_ceng_gao 码盘层高
 * @property string $jf_shp_lei 计费商品类
 * @property string $shp_pin_pai 商品品牌
 * @property string $shp_tiao_ma 商品条码
 * @property string $pp_tu_pian 品牌图片
 * @property string $bzhi_qi 保质期
 * @property string $shl_dan_wei 单位
 * @property string $jsh_dan_wei 拆零单位
 * @property string $ti_ji_cm 体积
 * @property string $zhl_kg 净重
 * @property string $chl_shl 拆零数量
 * @property string $jti_ji_bi 件数与体积比
 * @property string $jm_zhong_bi 件数与毛重比
 * @property string $jj_zhong_bi 件数与净重比
 * @property string $chc_dan_wei 尺寸单位
 * @property string $ch_dan_pin 长单品
 * @property string $ku_dan_pin 宽单品
 * @property string $gao_dan_pin 高单品
 * @property string $ch_zh_xiang 长整箱
 * @property string $ku_zh_xiang 宽整箱
 * @property string $gao_zh_xiang 高整箱
 * @property string $shp_miao_shu 商品描述
 * @property string $zhuang_tai 停用
 * @property string $zhl_kgm 毛重
 * @property string $SHP_BIAN_MAKH 商品客户编码
 * @property string $JIZHUN_WENDU 基准温度
 * @property string $yw_ming_cheng 
 * @property string $rw_ming_cheng 
 * @property string $cus_name 
 * @property string $peisongdian 
 * @property string $min_stock 最小库存
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsMdGoods extends MineModel
{
    use SoftDeletes;
    use Snowflake;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_md_goods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sys_org_code', 'sys_company_code', 'suo_shu_ke_hu', 'shp_ming_cheng', 'shp_jian_cheng',
        'shp_bian_ma', 'shp_xing_hao', 'shp_gui_ge', 'shp_yan_se', 'chp_shu_xing', 'cf_wen_ceng',
        'chl_kong_zhi', 'mp_dan_ceng', 'mp_ceng_gao', 'jf_shp_lei', 'shp_pin_pai', 'shp_tiao_ma',
        'pp_tu_pian', 'bzhi_qi', 'shl_dan_wei', 'jsh_dan_wei', 'ti_ji_cm', 'zhl_kg', 'chl_shl',
        'jti_ji_bi', 'jm_zhong_bi', 'jj_zhong_bi', 'chc_dan_wei', 'ch_dan_pin', 'ku_dan_pin',
        'gao_dan_pin', 'ch_zh_xiang', 'ku_zh_xiang', 'gao_zh_xiang', 'shp_miao_shu', 'zhuang_tai',
        'zhl_kgm', 'SHP_BIAN_MAKH', 'JIZHUN_WENDU', 'yw_ming_cheng', 'rw_ming_cheng', 'cus_name',
        'peisongdian', 'min_stock', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at',
        'updated_at', 'deleted_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime',
        'cf_wen_ceng' => 'string','jf_shp_lei' => 'string','chp_shu_xing' => 'string',
    ];
}