<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 入库管理验证数据类
 */
class WmsWmImNoticeHRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['goods_id','md_bin_id','cus_code','im_data','im_cus_code','im_car_dri','im_car_mobile','im_car_no','order_type_code','platform_code','im_sta','notice_id','appendix','READ_ONLY','WHERE_CON','sup_code','sup_name','pi_class','pi_master','status','sort','im_beizhu',],
             'update' => ['goods_id','md_bin_id','cus_code','im_data','im_cus_code','im_car_dri','im_car_mobile','im_car_no','order_type_code','platform_code','im_sta','notice_id','appendix','READ_ONLY','WHERE_CON','sup_code','sup_name','pi_class','pi_master','status','sort','im_beizhu',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'goods_id.required'=>' 商品不能为空',
'md_bin_id.required'=>' 库位不能为空',
'cus_code.required'=>' 客户不能为空',
'im_data.required'=>' 预计到货时间不能为空',
'im_car_no.required'=>' 车号不能为空',
'order_type_code.required'=>' 订单类型不能为空',
'platform_code.required'=>' 月台不能为空',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 商品 验证
            'goods_id' => 'required',
            // 库位 验证
            'md_bin_id' => 'required',
            // 客户 验证
            'cus_code' => 'required',
            // 预计到货时间 验证
            'im_data' => 'required',
            // 车号 验证
            'im_car_no' => 'required',
            // 订单类型 验证
            'order_type_code' => 'required',
            // 月台 验证
            'platform_code' => 'required',
            ];
        }

}