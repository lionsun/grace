import { request } from '@/utils/request.js'

/**
 * 业务日志 API JS
 */

export default {

  /**
   * 获取业务日志分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'system/commonManageLog/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加业务日志
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'system/commonManageLog/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新业务日志数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'system/commonManageLog/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'system/commonManageLog/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取业务日志
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'system/commonManageLog/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将业务日志删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'system/commonManageLog/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取业务日志数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'system/commonManageLog/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复业务日志数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'system/commonManageLog/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除业务日志
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'system/commonManageLog/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改业务日志数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'system/commonManageLog/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改业务日志数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'system/commonManageLog/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 业务日志导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'system/commonManageLog/import',
      method: 'post',
      data
    })
  },

  /**
   * 业务日志下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'system/commonManageLog/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 业务日志导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'system/commonManageLog/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}