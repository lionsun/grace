<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Service;

use App\Erp\Mapper\ErpStorehouseMapper;
use Mine\Abstracts\AbstractService;

/**
 * 仓库信息服务类
 */
class ErpStorehouseService extends AbstractService
{
    /**
     * @var ErpStorehouseMapper
     */
    public $mapper;

    public function __construct(ErpStorehouseMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}