<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 库位管理验证数据类
 */
class WmsMdBinRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['bin_store','ku_wei_ming_cheng','ku_wei_bian_ma','ku_wei_tiao_ma','ku_wei_lei_xing','ku_wei_shu_xing','shang_jia_ci_xu','qu_huo_ci_xu','suo_shu_ke_hu','ti_ji_dan_wei','zhong_liang_dan_wei','mian_ji_dan_wei','zui_da_ti_ji','zui_da_zhong_liang','zui_da_mian_ji','zui_da_tuo_pan','chang','kuan','gao','ting_yong','ming_xi','chp_shu_xing','ming_xi1','ming_xi2','ming_xi3','xnode','ynode','znode','status','sort',],
             'update' => ['bin_store','ku_wei_ming_cheng','ku_wei_bian_ma','ku_wei_tiao_ma','ku_wei_lei_xing','ku_wei_shu_xing','shang_jia_ci_xu','qu_huo_ci_xu','suo_shu_ke_hu','ti_ji_dan_wei','zhong_liang_dan_wei','mian_ji_dan_wei','zui_da_ti_ji','zui_da_zhong_liang','zui_da_mian_ji','zui_da_tuo_pan','chang','kuan','gao','ting_yong','ming_xi','chp_shu_xing','ming_xi1','ming_xi2','ming_xi3','xnode','ynode','znode','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'bin_store.required'=>' 仓库不能为空',
'ku_wei_ming_cheng.required'=>' 库位名称不能为空',
'ku_wei_bian_ma.required'=>' 库位编码不能为空',
'ku_wei_tiao_ma.required'=>' 库位条码不能为空',
'ku_wei_lei_xing.required'=>' 库区不能为空',
'ku_wei_shu_xing.required'=>' 库位属性不能为空',
'shang_jia_ci_xu.required'=>' 上架次序不能为空',
'qu_huo_ci_xu.required'=>' 取货次序不能为空',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 仓库 验证
            'bin_store' => 'required',
            // 库位名称 验证
            'ku_wei_ming_cheng' => 'required',
            // 库位编码 验证
//            'ku_wei_bian_ma' => 'required',
            // 库位条码 验证
//            'ku_wei_tiao_ma' => 'required',
            // 库区 验证
            'ku_wei_lei_xing' => 'required',
            // 库位属性 验证
            'ku_wei_shu_xing' => 'required',
            // 所属客户 验证
            'suo_shu_ke_hu' => 'required',
            // 上架次序 验证
//            'shang_jia_ci_xu' => 'required',
            // 取货次序 验证
//            'qu_huo_ci_xu' => 'required',
            ];
        }

}