<?php
/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/4/14
 * Time: 13:52
 */
declare(strict_types=1);

namespace App\System\Listener;

use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\Event\ValidatorFactoryResolved;

/**
 * 自定义表单验证器
 */
#[Listener]
class ValidatorFactoryResolvedListener implements ListenerInterface
{

    public function listen(): array
    {
        return [
            ValidatorFactoryResolved::class,
        ];
    }

    public function process(object $event)
    {
        /**  @var ValidatorFactoryInterface $validatorFactory */
        $validatorFactory = $event->validatorFactory;

        //phone验证器
        $validatorFactory->extend('phone', function ($attribute, $value, $parameters, $validator) {
            return (bool)preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $value);
        });
        // 当创建一个自定义验证规则时，你可能有时候需要为错误信息定义自定义占位符这里扩展了 :phone 占位符
        $validatorFactory->replacer('phone', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':phone', $attribute, $message);
        });

        // 身份证验证器
        $validatorFactory->extend('id_card', function ($attribute, $value, $parameters, $validator) {
            $idCard = (string)$value;
            if (strlen($idCard) == 15) {
                $idCard = $this->idcard15to18($idCard);
            }
            return $this->idcardChecksum18($idCard);
        });
        // 当创建一个自定义验证规则时，你可能有时候需要为错误信息定义自定义占位符这里扩展了 :id_card 占位符
        $validatorFactory->replacer('id_card', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':id_card', $attribute, $message);
        });
    }

    /**
     * 计算身份证校验码，根据国家标准GB 11643-1999
     * @param string $idcardBase
     * @return false|string
     */
    public function idcardVerifyNumber(string $idcardBase): bool|string
    {
        if (strlen($idcardBase) != 17) {
            return false;
        }
        //加权因子
        $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        //校验码对应值
        $verify_number_list = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
        $checksum = 0;
        for ($i = 0; $i < strlen($idcardBase); $i++) {
            $checksum += substr($idcardBase, $i, 1) * $factor[$i];
        }
        $mod = $checksum % 11;

        return $verify_number_list[$mod];
    }

    /**
     * 将15位身份证升级到18位
     */
    public function idcard15to18($idcard): bool|string
    {
        if (strlen($idcard) != 15) {
            return false;
        } else {
            // 如果身份证顺序码是996 997 998 999，这些是为百岁以上老人的特殊编码
            if (array_search(substr($idcard, 12, 3), array('996', '997', '998', '999')) !== false) {
                $idcard = substr($idcard, 0, 6) . '18' . substr($idcard, 6, 9);
            } else {
                $idcard = substr($idcard, 0, 6) . '19' . substr($idcard, 6, 9);
            }
        }

        return $idcard . $this->idcardVerifyNumber($idcard);
    }

    /**
     * 18位身份证校验码有效性检查
     * @param string $idcard
     * @return bool
     */
    public function idcardChecksum18(string $idcard): bool
    {
        if (strlen($idcard) != 18) {
            return false;
        }
        $idcardBase = substr($idcard, 0, 17);
        if ($this->idcardVerifyNumber($idcardBase) != strtoupper(substr($idcard, 17, 1))) {
            return false;
        } else {
            return true;
        }
    }
}