<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsBaPlatform;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 月台定义Mapper类
 */
class WmsBaPlatformMapper extends AbstractMapper
{
    /**
     * @var WmsBaPlatform
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsBaPlatform::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 月台代码
        if (isset($params['platform_code']) && $params['platform_code'] !== '') {
            $query->where('platform_code', '=', $params['platform_code']);
        }

        // 月台名称
        if (isset($params['platform_name']) && $params['platform_name'] !== '') {
            $query->where('platform_name', '=', $params['platform_name']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 商户id
        if (isset($params['mer_id']) && $params['mer_id'] !== '') {
            $query->where('mer_id', '=', $params['mer_id']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}