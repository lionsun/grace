<?php
namespace App\Common\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 平台用户Dto （导入导出）
 */
#[ExcelData]
class CommonUserDto implements MineModelExcel
{
    #[ExcelProperty(value: "用户id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "用户头像", index: 1)]
    public string $avatar;

    #[ExcelProperty(value: "用户账号", index: 2)]
    public string $account;

    #[ExcelProperty(value: "用户密码", index: 3)]
    public string $pwd;

    #[ExcelProperty(value: "真实姓名", index: 4)]
    public string $real_name;

    #[ExcelProperty(value: "生日", index: 5)]
    public string $birthday;

    #[ExcelProperty(value: "身份证号码", index: 6)]
    public string $card_id;

    #[ExcelProperty(value: "用户备注", index: 7)]
    public string $mark;

    #[ExcelProperty(value: "合伙人id", index: 8)]
    public string $partner_id;

    #[ExcelProperty(value: "用户分组id", index: 9)]
    public string $group_id;

    #[ExcelProperty(value: "用户昵称", index: 10)]
    public string $nickname;

    #[ExcelProperty(value: "商家id", index: 11)]
    public string $mer_id;

    #[ExcelProperty(value: "手机号码", index: 12)]
    public string $phone;

    #[ExcelProperty(value: "添加时间", index: 13)]
    public string $add_time;

    #[ExcelProperty(value: "添加ip", index: 14)]
    public string $add_ip;

    #[ExcelProperty(value: "最后一次登录时间", index: 15)]
    public string $last_time;

    #[ExcelProperty(value: "最后一次登录ip", index: 16)]
    public string $last_ip;

    #[ExcelProperty(value: "用户余额", index: 17)]
    public string $now_money;

    #[ExcelProperty(value: "佣金金额", index: 18)]
    public string $brokerage_price;

    #[ExcelProperty(value: "用户剩余积分", index: 19)]
    public string $integral;

    #[ExcelProperty(value: "会员经验", index: 20)]
    public string $exp;

    #[ExcelProperty(value: "连续签到天数", index: 21)]
    public string $sign_num;

    #[ExcelProperty(value: "1为正常，0为禁止", index: 22)]
    public string $status;

    #[ExcelProperty(value: "等级", index: 23)]
    public string $level;

    #[ExcelProperty(value: "分销等级", index: 24)]
    public string $agent_level;

    #[ExcelProperty(value: "是否有推广资格", index: 25)]
    public string $spread_open;

//    #[ExcelProperty(value: "推广员id", index: 26)]
//    public string $spread_uid;

//    #[ExcelProperty(value: "推广员关联时间", index: 27)]
//    public string $spread_time;
//
//    #[ExcelProperty(value: "是否为推广员", index: 28)]
//    public string $is_promoter;
//
//    #[ExcelProperty(value: "用户购买次数", index: 29)]
//    public string $pay_count;
//
//    #[ExcelProperty(value: "下级人数", index: 30)]
//    public string $spread_count;
//
//    #[ExcelProperty(value: "清理会员时间", index: 31)]
//    public string $clean_time;
//
//    #[ExcelProperty(value: "详细地址", index: 32)]
//    public string $addres;
//
//    #[ExcelProperty(value: "管理员编号 ", index: 33)]
//    public string $adminid;
//
//    #[ExcelProperty(value: "用户登陆类型，h5,wechat,routine", index: 34)]
//    public string $login_type;
//
//    #[ExcelProperty(value: "用户类型，shop,erp,crm", index: 35)]
//    public string $user_type;
//
//    #[ExcelProperty(value: "记录临时电话", index: 36)]
//    public string $record_phone;
//
//    #[ExcelProperty(value: "会员来源  0: 购买商品升级   1：花钱购买的会员2: 会员卡领取", index: 37)]
//    public string $is_money_level;
//
//    #[ExcelProperty(value: "是否永久性会员  0: 非永久会员  1：永久会员", index: 38)]
//    public string $is_ever_level;
//
//    #[ExcelProperty(value: "会员到期时间", index: 39)]
//    public string $overdue_time;
//
//    #[ExcelProperty(value: "uniqid", index: 40)]
//    public string $uniqid;
//
//    #[ExcelProperty(value: "代理类型：0普通，1事业部，2代理，3员工", index: 41)]
//    public string $division_type;
//
//    #[ExcelProperty(value: "代理状态", index: 42)]
//    public string $division_status;
//
//    #[ExcelProperty(value: "事业部状态", index: 43)]
//    public string $is_division;
//
//    #[ExcelProperty(value: "代理状态", index: 44)]
//    public string $is_agent;
//
//    #[ExcelProperty(value: "员工状态", index: 45)]
//    public string $is_staff;
//
//    #[ExcelProperty(value: "事业部id", index: 46)]
//    public string $division_id;
//
//    #[ExcelProperty(value: "代理商id", index: 47)]
//    public string $agent_id;
//
//    #[ExcelProperty(value: "员工id", index: 48)]
//    public string $staff_id;
//
//    #[ExcelProperty(value: "分佣比例", index: 49)]
//    public string $division_percent;
//
//    #[ExcelProperty(value: "事业部/代理/员工修改时间", index: 50)]
//    public string $division_change_time;
//
//    #[ExcelProperty(value: "事业部/代理/员工结束时间", index: 51)]
//    public string $division_end_time;
//
//    #[ExcelProperty(value: "代理商邀请码", index: 52)]
//    public string $division_invite;
//
//    #[ExcelProperty(value: "分账金额，单位分", index: 53)]
//    public string $sharing_money;
//
//    #[ExcelProperty(value: "地图角色：1门店商家，2业务员，3代理", index: 54)]
//    public string $map_level;
//
//    #[ExcelProperty(value: "地图代理等级：1全国，2省，3市，4区", index: 55)]
//    public string $map_acting_level;
//
//    #[ExcelProperty(value: "地图代理区域编码", index: 56)]
//    public string $map_code;
//
//    #[ExcelProperty(value: "城市代理等级", index: 57)]
//    public string $city_level;
//
//    #[ExcelProperty(value: "城市代理城市area_code", index: 58)]
//    public string $city_level_code;
//
//    #[ExcelProperty(value: "城市代理城市address", index: 59)]
//    public string $city_level_address;
//
//    #[ExcelProperty(value: "创建者", index: 60)]
//    public string $created_by;
//
//    #[ExcelProperty(value: "更新者", index: 61)]
//    public string $updated_by;
//
//    #[ExcelProperty(value: "创建时间", index: 62)]
//    public string $created_at;
//
//    #[ExcelProperty(value: "更新时间", index: 63)]
//    public string $updated_at;
//
//    #[ExcelProperty(value: "删除时间", index: 64)]
//    public string $deleted_at;


}