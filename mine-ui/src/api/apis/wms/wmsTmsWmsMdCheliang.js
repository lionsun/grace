import { request } from '@/utils/request.js'

/**
 * 车辆管理 API JS
 */

export default {

  /**
   * 获取车辆管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/tmsMdCheliang/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加车辆管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/tmsMdCheliang/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新车辆管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/tmsMdCheliang/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/tmsMdCheliang/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取车辆管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/tmsMdCheliang/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将车辆管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/tmsMdCheliang/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取车辆管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/tmsMdCheliang/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复车辆管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/tmsMdCheliang/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除车辆管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/tmsMdCheliang/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改车辆管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/tmsMdCheliang/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改车辆管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/tmsMdCheliang/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 车辆管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/tmsMdCheliang/import',
      method: 'post',
      data
    })
  },

  /**
   * 车辆管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/tmsMdCheliang/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 车辆管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/tmsMdCheliang/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}