<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Crm\Mapper;

use App\Crm\Model\CrmUser;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 客户管理Mapper类
 */
class CrmUserMapper extends AbstractMapper
{
    /**
     * @var CrmUser
     */
    public $model;

    public function assignModel()
    {
        $this->model = CrmUser::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 销售阶段
        if (isset($params['sales_stag']) && $params['sales_stag'] >= 0) {
            $query->where('sales_stag', '=', $params['sales_stag']);
        }

        // 跟进人
        if (isset($params['follow_user_name']) && $params['follow_user_name'] !== '') {
            $query->where('follow_user_name', '=', $params['follow_user_name']);
        }

        // 客户名称
        if (isset($params['name']) && $params['name'] !== '') {
            $query->where('name', '=', $params['name']);
        }

        // 客户来源
        if (isset($params['from']) && $params['from'] !== '') {
            $query->where('from', '=', $params['from']);
        }

        // 需求级别
        if (isset($params['level']) && $params['level'] !== '') {
            $query->where('level', '=', $params['level']);
        }

        // 电话
        if (isset($params['phone']) && $params['phone'] !== '') {
            $query->where('phone', '=', $params['phone']);
        }

        // 法人
        if (isset($params['legal_perso']) && $params['legal_perso'] !== '') {
            $query->where('legal_perso', '=', $params['legal_perso']);
        }

        // 姓名
        if (isset($params['user_name']) && $params['user_name'] !== '') {
            $query->where('user_name', '=', $params['user_name']);
        }

        // 手机
        if (isset($params['user_phone']) && $params['user_phone'] !== '') {
            $query->where('user_phone', '=', $params['user_phone']);
        }

        // 微信
        if (isset($params['user_wechat']) && $params['user_wechat'] !== '') {
            $query->where('user_wechat', '=', $params['user_wechat']);
        }

        // QQ
        if (isset($params['user_qq']) && $params['user_qq'] !== '') {
            $query->where('user_qq', '=', $params['user_qq']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}