<?php
/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/7/8
 * Time: 18:18
 */

namespace App\Common\Task;

use App\Common\Queue\Producer\GraceProducer;
use App\Common\Service\Tool\SmtpService;

/**
 * 监控服务是否可用
 * 每1小时执行1次
 */
class ServiceTask
{
    public function execute()
    {
        $content = '';
        try {
           if (redis()->set('graceServiceKey')) {
               $content .= 'redis服务正常----------';
           } else {
               $content .= 'redis服务异常----------';
           }
            if(gracePush(new GraceProducer(['order_id' => 123]),0)) {
                $content .= '队列服务正常-----------';
            } else {
                $content .= '队列服务异常-----------';
            }
            SmtpService::email('522402295@qq.com',env('SERVICE_TASK_TITKE'),$content);
        } catch (\Throwable $e) {
            SmtpService::email('522402295@qq.com',env('SERVICE_TASK_TITKE'),$e->getMessage());

        }
        logger('service-task-log')->info('服务监控执行完成');
    }

    public function isEnable(): bool
    {
        return true;
    }
}