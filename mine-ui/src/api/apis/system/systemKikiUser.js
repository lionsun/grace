import {request} from '@/utils/request.js'

/**
 * kiki一对一 API JS
 */

export default {

	/**
	 * 获取kiki一对一分页列表
	 * @returns
	 */
	getList(params = {}) {
		return request({
			url: 'system/kikiUser/index',
			method: 'get',
			params
		})
	},

	/**
	 * 添加kiki一对一
	 * @returns
	 */
	save(params = {}) {
		return request({
			url: 'system/kikiUser/save',
			method: 'post',
			data: params
		})
	},

	/**
	 * 更新kiki一对一数据
	 * @returns
	 */
	update(id, params = {}) {
		return request({
			url: 'system/kikiUser/update/' + id,
			method: 'put',
			data: params
		})
	},

	/**
	 * 更改单个数据
	 * @returns
	 */
	updateSingle(id, params = {}) {
		return request({
			url: 'system/kikiUser/updateSingle/' + id,
			method: 'put',
			data: params
		})
	},

	/**
	 * 读取kiki一对一
	 * @returns
	 */
	read(params = {}) {
		return request({
			url: 'system/kikiUser/read',
			method: 'post',
			data: params
		})
	},

	/**
	 * 将kiki一对一删除，有软删除则移动到回收站
	 * @returns
	 */
	deletes(ids) {
		return request({
			url: 'system/kikiUser/delete/' + ids,
			method: 'delete'
		})
	},

	/**
	 * 从回收站获取kiki一对一数据列表
	 * @returns
	 */
	getRecycleList(params = {}) {
		return request({
			url: 'system/kikiUser/recycle',
			method: 'get',
			params
		})
	},

	/**
	 * 恢复kiki一对一数据
	 * @returns
	 */
	recoverys(ids) {
		return request({
			url: 'system/kikiUser/recovery/' + ids,
			method: 'put'
		})
	},

	/**
	 * 真实删除kiki一对一
	 * @returns
	 */
	realDeletes(ids) {
		return request({
			url: 'system/kikiUser/realDelete/' + ids,
			method: 'delete'
		})
	},

	/**
	 * 更改kiki一对一数据
	 * @returns
	 */
	changeStatus(data = {}) {
		return request({
			url: 'system/kikiUser/changeStatus',
			method: 'put',
			data
		})
	},

	/**
	 * 修改kiki一对一数值数据，自增自减
	 * @returns
	 */
	numberOperation(data = {}) {
		return request({
			url: 'system/kikiUser/numberOperation',
			method: 'put',
			data
		})
	},

	/**
	 * kiki一对一导入
	 * @returns
	 */
	importExcel(data = {}) {
		return request({
			url: 'system/kikiUser/import',
			method: 'post',
			data
		})
	},

	/**
	 * kiki一对一下载模板
	 * @returns
	 */
	downloadTemplate() {
		return request({
			url: 'system/kikiUser/downloadTemplate',
			method: 'post',
			responseType: 'blob'
		})
	},

	/**
	 * kiki一对一导出
	 * @returns
	 */
	exportExcel(params = {}) {
		return request({
			url: 'system/kikiUser/export',
			method: 'post',
			responseType: 'blob',
			params
		})
	},

}
