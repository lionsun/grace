import { request } from '@/utils/request.js'

/**
 * RFID管理 API JS
 */

export default {

  /**
   * 获取RFID管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'wms/rfidBuse/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加RFID管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'wms/rfidBuse/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新RFID管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'wms/rfidBuse/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'wms/rfidBuse/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取RFID管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'wms/rfidBuse/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将RFID管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'wms/rfidBuse/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取RFID管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'wms/rfidBuse/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复RFID管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'wms/rfidBuse/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除RFID管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'wms/rfidBuse/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改RFID管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'wms/rfidBuse/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改RFID管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'wms/rfidBuse/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * RFID管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'wms/rfidBuse/import',
      method: 'post',
      data
    })
  },

  /**
   * RFID管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'wms/rfidBuse/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * RFID管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'wms/rfidBuse/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}