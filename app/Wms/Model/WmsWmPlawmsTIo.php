<?php

declare (strict_types=1);
namespace App\Wms\Model;

use Hyperf\Database\Model\Relations\HasOne;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;
/**
 * @property string $id 主键
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $carno 车号
 * @property string $doc_id 单据编号
 * @property string $plawms_t_id 月台编号
 * @property string $in_data 进入时间
 * @property string $ouwms_t_data 驶出时间
 * @property string $plawms_t_sta 月台状态
 * @property string $plawms_t_beizhu 备注
 * @property string $plan_indata 计划进入时间
 * @property string $plan_outdata 计划驶出时间
 * @property string $plawms_t_oper 月台操作
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsWmPlawmsTIo extends MineModel
{
    use SoftDeletes;
    use Snowflake;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_wm_plawms_t_io';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sys_org_code', 'sys_company_code', 'carno', 'doc_id', 'plawms_t_id', 'in_data', 'ouwms_t_data', 'plawms_t_sta', 'plawms_t_beizhu', 'plan_indata', 'plan_outdata', 'plawms_t_oper', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string','status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer',
        'created_by' => 'integer', 'updated_by' => 'integer', 'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * 月台
     * @return HasOne
     */
    public function baPlatform(): HasOne
    {
        return $this->hasOne(WmsBaPlatform::class, 'id', 'plawms_t_id');
    }
}