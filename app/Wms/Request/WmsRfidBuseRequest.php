<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * RFID管理验证数据类
 */
class WmsRfidBuseRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['sys_org_code','sys_company_code','bpm_status','wms_rfid_type','wms_rfid_buseno','wms_rfid_busecont','wms_rfid_id1','wms_rfid_id2','wms_rfid_id3','status','sort',],
             'update' => ['sys_org_code','sys_company_code','bpm_status','wms_rfid_type','wms_rfid_buseno','wms_rfid_busecont','wms_rfid_id1','wms_rfid_id2','wms_rfid_id3','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             
          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            ];
        }

}