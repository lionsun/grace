<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsBaStore;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 仓库管理Mapper类
 */
class WmsBaStoreMapper extends AbstractMapper
{
    /**
     * @var WmsBaStore
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsBaStore::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 仓库代码
        if (isset($params['store_code']) && $params['store_code'] !== '') {
            $query->where('store_code', '=', $params['store_code']);
        }

        // 仓库名称
        if (isset($params['store_name']) && $params['store_name'] !== '') {
            $query->where('store_name', '=', $params['store_name']);
        }

        // 仓库属性
        if (isset($params['store_text']) && $params['store_text'] !== '') {
            $query->where('store_text', '=', $params['store_text']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 活跃状态
        if (isset($params['active_status']) && $params['active_status'] === true) {
            $query->where('status', '=', 1);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}