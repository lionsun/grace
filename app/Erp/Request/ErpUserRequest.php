<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Erp\Request;

use Mine\MineFormRequest;

/**
 * 会员信息验证数据类
 */
class ErpUserRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['user_name','phone','email','remark','status','sort',],
             'update' => ['user_name','phone','email','remark','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'user_name.required'=>' 会员名称不能为空',
'phone.required'=>' 手机号码不能为空',
'phone.phone'=>' 手机号码格式错误',
'email.email'=>' 邮箱格式错误',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 会员名称 验证
            'user_name' => 'required',
            // 手机号码 验证
            'phone' => 'required|phone',
            // 邮箱 验证
            'email' => 'email',
            ];
        }

}