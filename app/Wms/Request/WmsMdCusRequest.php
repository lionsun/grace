<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 客户信息验证数据类
 */
class WmsMdCusRequest extends MineFormRequest
{
    /**
     * 验证场景
     */
    public $scenes = [
        'create' => ['sys_org_code', 'sys_company_code', 'zhu_ji_ma', 'ke_hu_jian_cheng', 'ke_hu_ying_wen', 'zeng_yong_qi', 'zeng_yong_qi_ye', 'ke_hu_deng_ji', 'suo_shu_xing_ye', 'shou_qian_ri_qi', 'zhong_zhi_he_shi_jian', 'gui_shu_zu_zh', 'gui_shu_sheng', 'gui_shu_shi_dai', 'gui_shu', 'you_zheng_bian_ma', 'dian_hua', 'chuan_zhen', 'Emaildi_zhi', 'wang_ye_di_zhi', 'fa_ren_dai_biao', 'fa_ren_shen_fen', 'zhu_ce_zi_jin', 'bi_bie', 'shui_wu_deng', 'zu_zhi_ji_gou', 'dao_lu_yun_shu', 'zhu_ying_ye_wu', 'he_yi_xiang', 'pi_zhun_ji_guan', 'pi_zhun_wen_hao', 'zhu_ce_ri_qi', 'bei_zhu', 'zhu_lian_xi_ren1', 'dian_hua1', 'status', 'sort', 'shou_ji', 'zhu_lian_xi_ren', 'di_zhi', 'ying_ye_zhi_zhao', 'zhong_wen_qch', 'ke_hu_shu_xing', 'ke_hu_bian_ma', 'xing_ye_fen_lei',],
        'update' => ['sys_org_code', 'sys_company_code', 'zhu_ji_ma', 'ke_hu_jian_cheng', 'ke_hu_ying_wen', 'zeng_yong_qi', 'zeng_yong_qi_ye', 'ke_hu_deng_ji', 'suo_shu_xing_ye', 'shou_qian_ri_qi', 'zhong_zhi_he_shi_jian', 'gui_shu_zu_zh', 'gui_shu_sheng', 'gui_shu_shi_dai', 'gui_shu', 'you_zheng_bian_ma', 'dian_hua', 'chuan_zhen', 'Emaildi_zhi', 'wang_ye_di_zhi', 'fa_ren_dai_biao', 'fa_ren_shen_fen', 'zhu_ce_zi_jin', 'bi_bie', 'shui_wu_deng', 'zu_zhi_ji_gou', 'dao_lu_yun_shu', 'zhu_ying_ye_wu', 'he_yi_xiang', 'pi_zhun_ji_guan', 'pi_zhun_wen_hao', 'zhu_ce_ri_qi', 'bei_zhu', 'zhu_lian_xi_ren1', 'dian_hua1', 'status', 'sort', 'shou_ji', 'zhu_lian_xi_ren', 'di_zhi', 'ying_ye_zhi_zhao', 'zhong_wen_qch', 'ke_hu_shu_xing', 'ke_hu_bian_ma', 'xing_ye_fen_lei',],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'shou_ji.required' => ' 手机不能为空',
            'zhu_lian_xi_ren.required' => ' 负责人不能为空',
            'di_zhi.required' => ' 地址不能为空',
            'zhong_wen_qch.required' => ' 中文全称不能为空',
            'ke_hu_shu_xing.required' => ' 客户属性不能为空',
            'ke_hu_bian_ma.required' => ' 客户编码不能为空',
            'xing_ye_fen_lei.required' => ' 企业属性不能为空',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [

            // 手机 验证
            'shou_ji' => 'required',
            // 负责人 验证
            'zhu_lian_xi_ren' => 'required',
            // 地址 验证
            'di_zhi' => 'required',
            // 中文全称 验证
            'zhong_wen_qch' => 'required',
            // 客户属性 验证
            'ke_hu_shu_xing' => 'required',
            // 客户编码 验证
            'ke_hu_bian_ma' => 'required',
            // 企业属性 验证
            'xing_ye_fen_lei' => 'required',
        ];
    }

}