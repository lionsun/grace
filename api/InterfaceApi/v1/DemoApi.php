<?php
/**
 * MineAdmin is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using MineAdmin.
 *
 * @Author kiki
 * @Link   https://gitee.com/xmo/MineAdmin
 */

namespace Api\InterfaceApi\v1;

use Api\BaseController;
use App\Common\Queue\Producer\GraceProducer;
use App\Common\Task\GraceTask;
use App\JsonRpc\CalculatorServiceConsumer;
use App\System\Mapper\SystemDeptMapper;
use App\System\Mapper\SystemUserMapper;
use App\System\Model\Debug;
use App\System\Model\SystemApi;
use App\System\Model\SystemQueueLog;
use App\System\Service\SystemKikiTest5Service;
use App\System\Service\SystemQueueLogService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Snowflake\IdGeneratorInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Parallel;
use Mine\Exception\NormalStatusException;
use Mine\Exception\TokenException;
use Mine\Helper\MineCode;
use Yansongda\HyperfPay\Pay;

/**
 * 演示，测试专用
 */
class DemoApi extends BaseController
{
//    public bool $auth = true;

//    public array $noNeedLogin = ['getUserList']; // 登录校验中无需检验的方法


    /**
     * @var SystemDeptMapper
     */
    protected SystemDeptMapper $dept;

    #[Inject]
    protected SystemKikiTest5Service $kiki5Service;

    #[Inject]
    protected GraceTask $graceTask;

//    /**
//     * DemoApi constructor.
//     * @param SystemUserMapper $user
//     * @param SystemDeptMapper $dept
//     */
//    public function __construct(SystemUserMapper $user, SystemDeptMapper $dept,  SystemQueueLogService $service
//)
//    {
//        parent:__construct();
//        $this->user = $user;
//        $this->dept = $dept;
////        $this->kiki5Service = $kiki5Service;
//        $this->service = $service;
//    }

    /**
     * 获取用户列表接口
     * @return array
     */
    public function getUserList()
    {
//        $client = ApplicationContext::getContainer()->get(CalculatorServiceConsumer::class);
//
//        $result = $client->add(10, 2);
//
//        return  $result;
//        throw new TokenException(t('jwt.validate_fail'));
//            throw new NormalStatusException('请先登录', MineCode::TOKEN_EXPIRED);
//        throw new NormalStatusException('请登录11','401');
        // 第二个参数，不进行数据权限检查，否则会拉起检测是否登录。
//        $params = [
//            'userWhere' => 'kiki2'
//        ];
//        $userId = 20;
//        /** @var Jwt $jwt */
//        $jwt = make(Jwt::class);
//        return $jwt->token($userId);
        return $this->kiki5Service->getList();
//        return [\Hyperf\Utils\Coroutine::inCoroutine()];

//        $this->graceTask->push(new GraceProducer('队列测试'),0);

//        $container = ApplicationContext::getContainer();
//        $generator = $container->get(IdGeneratorInterface::class);
//        $id = $generator->generate();
//        return [$id];

//        return $pay->alipay()->web([
//            'out_trade_no' => ''.time(),
//            'total_amount' => '0.01',
//            'subject' => 'yansongda 测试 - 1',
//        ]);

        // 开启5个协程
//        $parallel = new Parallel(5);
//        for ($i = 0; $i < 6; $i++) {
//            $parallel->add(function () {
//                sleep(1);
//                return Coroutine::id();
//            });
//        }
//
//        try{
//            $results = $parallel->wait();
//            return $results;
//        } catch(ParallelExecutionException $e){
//            return $e->getResults();
//            // $e->getThrowables() 获取协程中出现的异常。
//        }

//        return $results;

    }

    public function wxLogin()
    {
        return 77;
    }

    public function shopOrderIndex()
    {

    }


    /**
     * 获取部门列表接口
     * @return array
     */
    public function getDeptList(): array
    {
        // 第二个参数，不进行数据权限检查，否则会拉起检测是否登录。
        return $this->dept->getTreeList([], false);
    }
}