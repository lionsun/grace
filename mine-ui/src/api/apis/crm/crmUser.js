import { request } from '@/utils/request.js'

/**
 * 客户管理 API JS
 */

export default {

  /**
   * 获取客户管理分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'crm/user/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加客户管理
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'crm/user/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新客户管理数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'crm/user/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'crm/user/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取客户管理
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'crm/user/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将客户管理删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'crm/user/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取客户管理数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'crm/user/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复客户管理数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'crm/user/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除客户管理
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'crm/user/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改客户管理数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'crm/user/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改客户管理数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'crm/user/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 客户管理导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'crm/user/import',
      method: 'post',
      data
    })
  },

  /**
   * 客户管理下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'crm/user/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 客户管理导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'crm/user/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}