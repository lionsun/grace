<?php

declare(strict_types=1);

namespace Api;

use App\Common\Service\Tool\JwtService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Psr\Container\ContainerInterface;
use Hyperf\HttpServer\Annotation\AutoController;

/**
 * api基类（包含用户数据）
 */
#[Controller(prefix: "api")]
class JwtController
{
    protected $request;

    protected $response;

    protected $container;

//    public function __construct(RequestInterface $request, ContainerInterface $container)
//    {
//        $this->request = $request;
//        $this->container = $container;
//        $this->response = $container->get(Response::class);
//
//
//        // 记录请求日志
////        (new Log())->apiLog($this->request);
//    }

    /**
     * 通过用户获取token
     * @return false|string
     */
    public function token()
    {
        $userId = 20;
        return $this->response->success((new JwtService())->token($userId));
    }

    /**
     * 通过token获取用户
     * @return int
     */
    public function user()
    {
        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjQwOTEyNTUsInN1YiI6IuS4u-mimCIsIm5iZiI6MTYyMzQ4NjQ1NSwiYXVkIjoyMCwiaWF0IjoxNjIzNDg2NDU1LCJqdGkiOiJlNzVmMjNmNzM5YmI1Njk0MDgwNmM5MjZkZDQ2Zjg1YiIsImlzcyI6ImVhc3lzd29vbGUiLCJzdGF0dXMiOjEsImRhdGEiOlsib3RoZXJfaW5mbyJdfQ.3CK7trYBkGUAagtZd5zsoFRF3U9W-xz0THAJr30jwwA';
        return (new JwtService())->user($token);
    }
}
