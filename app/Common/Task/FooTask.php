<?php
/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/7/8
 * Time: 18:18
 */

namespace App\Common\Task;

class FooTask
{
    public function execute()
    {
        echo 777;
    }

    public function isEnable(): bool
    {
        return true;
    }
}