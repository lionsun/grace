<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Service;

use App\Erp\Mapper\ErpPersonMapper;
use Mine\Abstracts\AbstractService;

/**
 * 经手人管理服务类
 */
class ErpPersonService extends AbstractService
{
    /**
     * @var ErpPersonMapper
     */
    public $mapper;

    public function __construct(ErpPersonMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}