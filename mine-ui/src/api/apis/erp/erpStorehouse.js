import { request } from '@/utils/request.js'

/**
 * 仓库信息 API JS
 */

export default {

  /**
   * 获取仓库信息分页列表
   * @returns
   */
  getList (params = {}) {
    return request({
      url: 'erp/storehouse/index',
      method: 'get',
      params
    })
  },

  /**
   * 添加仓库信息
   * @returns
   */
  save (params = {}) {
    return request({
      url: 'erp/storehouse/save',
      method: 'post',
      data: params
    })
  },

  /**
   * 更新仓库信息数据
   * @returns
   */
  update (id, params = {}) {
    return request({
      url: 'erp/storehouse/update/' + id,
      method: 'put',
      data: params
    })
  },

  /**
     * 更改单个数据
     * @returns
     */
    updateSingle(id,params = {}) {
      return request({
        url: 'erp/storehouse/updateSingle/' + id,
        method: 'put',
        data: params
      })
    },

  /**
   * 读取仓库信息
   * @returns
   */
  read (params = {}) {
    return request({
      url: 'erp/storehouse/read',
      method: 'post',
      data: params
    })
  },

  /**
   * 将仓库信息删除，有软删除则移动到回收站
   * @returns
   */
  deletes (ids) {
    return request({
      url: 'erp/storehouse/delete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 从回收站获取仓库信息数据列表
   * @returns
   */
  getRecycleList (params = {}) {
    return request({
      url: 'erp/storehouse/recycle',
      method: 'get',
      params
    })
  },

  /**
   * 恢复仓库信息数据
   * @returns
   */
  recoverys (ids) {
    return request({
      url: 'erp/storehouse/recovery/' + ids,
      method: 'put'
    })
  },

  /**
   * 真实删除仓库信息
   * @returns
   */
  realDeletes (ids) {
    return request({
      url: 'erp/storehouse/realDelete/' + ids,
      method: 'delete'
    })
  },

  /**
   * 更改仓库信息数据
   * @returns
   */
  changeStatus (data = {}) {
    return request({
      url: 'erp/storehouse/changeStatus',
      method: 'put',
      data
    })
  },

  /**
   * 修改仓库信息数值数据，自增自减
   * @returns
   */
  numberOperation (data = {}) {
    return request({
      url: 'erp/storehouse/numberOperation',
      method: 'put',
      data
    })
  },

  /**
    * 仓库信息导入
    * @returns
    */
  importExcel (data = {}) {
    return request({
      url: 'erp/storehouse/import',
      method: 'post',
      data
    })
  },

  /**
   * 仓库信息下载模板
   * @returns
   */
  downloadTemplate () {
    return request({
      url: 'erp/storehouse/downloadTemplate',
      method: 'post',
      responseType: 'blob'
    })
  },

  /**
   * 仓库信息导出
   * @returns
   */
  exportExcel (params = {}) {
    return request({
      url: 'erp/storehouse/export',
      method: 'post',
      responseType: 'blob',
      params
    })
  },

}