<?php

declare (strict_types=1);
namespace App\Erp\Model;

use Hyperf\Database\Model\SoftDeletes;
use Mine\MineModel;
/**
 * @property int $id 
 * @property string $number 车牌号码
 * @property string $brand 品牌型号
 * @property string $user_name 驾驶员
 * @property string $color 车身颜色
 * @property int $mileage 行驶里程(公里)
 * @property string $engine 发动机型号
 * @property string $frame_number 车架号码
 * @property string $factory 生产厂家
 * @property string $factory_at 出厂日期
 * @property string $year_at 年检到期
 * @property string $insurance_at 保险到期
 * @property string $level 排放等级
 * @property int $belong 车辆归属
 * @property string $peice 车辆价格
 * @property int $car_status 车辆状态
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class ErpCar extends MineModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'erp_car';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'number', 'brand', 'user_name', 'color', 'mileage', 'engine', 'frame_number', 'factory', 'factory_at', 'year_at', 'insurance_at', 'level', 'belong', 'peice', 'car_status', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer', 'mileage' => 'integer', 'belong' => 'string', 'peice' => 'decimal:2',
        'car_status' => 'string', 'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer',
        'created_by' => 'integer', 'updated_by' => 'integer', 'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}