<?php

declare(strict_types=1);

namespace App\Common\Queue\Producer;

use App\System\Model\Debug;
use Hyperf\Amqp\Annotation\Producer;
use Hyperf\Amqp\Message\ProducerDelayedMessageTrait;
use Hyperf\Amqp\Message\ProducerMessage;
use Hyperf\Amqp\Message\Type;

/**
 * 公共生产者
 */
#[Producer(exchange: "grace.exchange",routingKey: "grace.routing")]
class GraceProducer extends ProducerMessage
{
    use ProducerDelayedMessageTrait;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->payload = $data;
    }
}
