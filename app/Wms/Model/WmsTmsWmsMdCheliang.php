<?php

declare (strict_types=1);
namespace App\Wms\Model;

use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\MineModel;
/**
 * @property string $id 主键
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $bpm_status 流程状态
 * @property string $chepaihao 车牌号
 * @property string $chexing 车型
 * @property string $color 颜色
 * @property string $zuidatiji 最大体积
 * @property string $zaizhong 载重
 * @property string $zairen 载人数
 * @property string $jiazhao 准假驾照
 * @property string $zhuangtai 是否可用
 * @property string $beizhu 备注
 * @property string $username 默认司机
 * @property string $gpsid gps
 * @property string $quyu 
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class WmsTmsWmsMdCheliang extends MineModel
{
    use SoftDeletes;
    use Snowflake;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_tms_wms_md_cheliang';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sys_org_code', 'sys_company_code', 'bpm_status', 'chepaihao', 'chexing', 'color', 'zuidatiji', 'zaizhong', 'zairen', 'jiazhao', 'zhuangtai', 'beizhu', 'username', 'gpsid', 'quyu', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'
    ];
}