<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Service;

use App\Erp\Mapper\ErpGoodsUnitMapper;
use Mine\Abstracts\AbstractService;

/**
 * 商品单位服务类
 */
class ErpGoodsUnitService extends AbstractService
{
    /**
     * @var ErpGoodsUnitMapper
     */
    public $mapper;

    public function __construct(ErpGoodsUnitMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}