<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Mapper;

use App\Erp\Model\ErpCustomerVendor;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 供应商信息Mapper类
 */
class ErpCustomerVendorMapper extends AbstractMapper
{
    /**
     * @var ErpCustomerVendor
     */
    public $model;

    public function assignModel()
    {
        $this->model = ErpCustomerVendor::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 名称
        if (isset($params['name']) && $params['name'] !== '') {
            $query->where('name', '=', $params['name']);
        }

        // 手机号码
        if (isset($params['phone']) && $params['phone'] !== '') {
            $query->where('phone', '=', $params['phone']);
        }

        // 联系人
        if (isset($params['user_name']) && $params['user_name'] !== '') {
            $query->where('user_name', '=', $params['user_name']);
        }

        // 联系电话
        if (isset($params['user_phone']) && $params['user_phone'] !== '') {
            $query->where('user_phone', '=', $params['user_phone']);
        }

        // 纳税人识别号
        if (isset($params['tax_iden_num']) && $params['tax_iden_num'] !== '') {
            $query->where('tax_iden_num', '=', $params['tax_iden_num']);
        }

        // 账号
        if (isset($params['account']) && $params['account'] !== '') {
            $query->where('account', '=', $params['account']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}