<?php

declare (strict_types=1);
namespace App\System\Model;

use App\Common\Scope\ShopScope;
use Hyperf\Database\Model\SoftDeletes;
use Mine\MineModel;
use Hyperf\Snowflake\Concern\Snowflake;

/**
 * @property int $id id
 * @property int $user_id 用户id
 * @property string $users 用户ids
 * @property string $username 用户名
 * @property string $image 图片
 * @property int $sex 性别
 * @property string $like 爱好
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 * @property string $pic 多图
 * @property string $content 内容
 * @property int $type 商家类型
 * @property int $status 状态
 * @property int $status2 状态2
 * @property int $order_status 订单状态
 * @property int $rate 评分
 * @property string $color 颜色
 * @property int $pro 进度条
 * @property string $province 省
 * @property string $city 市
 * @property string $area 区
 * @property string $address 省市区
 * @property string $multiLineList 多行表单
 * @property int $shop_id 商家id
 * @property-write mixed $file 文件
 * @property-read SystemKikiUser $user 
 */
class SystemKikiTest5 extends MineModel
{
    use SoftDeletes;
    use Snowflake;

    //    protected static function booted()
    //    {
    //        static::addGlobalScope(new ShopScope());
    //    }
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_kiki_test5';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'mer_id','users', 'username', 'image', 'file', 'sex', 'like', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'province', 'city', 'area', 'address', 'multiLineList', 'shop_id'];

    protected $casts = [
        'id' => 'string',
        'sex' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'type' => 'string',
        'status' => 'string',
        'status2' => 'string',
        'order_status' => 'string',
        'rate' => 'integer',
        'pro' => 'integer',
        'like' => 'array',
        'multiLineList' => 'array',
        'address' => 'array',
    ];
    /**
     * 用户
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function user() : \Hyperf\Database\Model\Relations\HasOne
    {
        return $this->hasOne(SystemKikiUser::class, 'id', 'user_id');
    }
    public function setFileAttribute($value)
    {
        $this->attributes['file'] = config('file.storage.oss.domain') . $value;
    }
}