<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\System\Request;

use Mine\MineFormRequest;

/**
 * kiki一对一验证数据类
 */
class SystemKikiUserRequest extends MineFormRequest
{
    /**
     * 验证场景
     */
    public $scenes = [
        'create' => ['username', 'file', 'sex', 'like', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'address', 'multiLineList', 'username', 'image', 'file', 'sex', 'like', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'province', 'city', 'area', 'address', 'multiLineList', 'users', 'username', 'image', 'file', 'sex', 'like', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'province', 'city', 'area', 'address', 'multiLineList', 'users', 'user_id', 'username', 'phone',],
        'update' => ['username', 'file', 'sex', 'like', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'province', 'city', 'area', 'address', 'multiLineList', 'username', 'image', 'file', 'sex', 'like', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'province', 'city', 'area', 'address', 'multiLineList', 'users', 'username', 'image', 'file', 'sex', 'like', 'pic', 'content', 'type', 'status', 'status2', 'order_status', 'rate', 'color', 'pro', 'province', 'city', 'area', 'address', 'multiLineList', 'users', 'user_id', 'username', 'phone',],
    ];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * 获取已定义验证规则的错误消息
     */
    public function messages(): array
    {
        return [
            'username.required' => ' 用户名必须',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [

            // 用户名 验证
            'username' => 'required',
        ];
    }

}