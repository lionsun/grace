<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Common\Service;

use App\Common\Mapper\CommonUserMapper;
use App\Common\Service\Tool\ConstantsService;
use App\Common\Service\Tool\JwtService;
use Hyperf\Di\Annotation\Inject;
use Mine\Abstracts\AbstractService;
use Mine\Exception\NormalStatusException;

/**
 * 基础类
 */
class BaseService
{

}