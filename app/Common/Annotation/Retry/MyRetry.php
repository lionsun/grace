<?php
/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/7/3
 * Time: 15:24
 */

namespace App\Common\Annotation\Retry;

use Doctrine\Common\Annotations\Annotation\Target;
use Hyperf\Retry\Policy\ClassifierRetryPolicy;
use Hyperf\Retry\Policy\MaxAttemptsRetryPolicy;
use Hyperf\Retry\Policy\SleepRetryPolicy;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class MyRetry extends \Hyperf\Retry\Annotation\AbstractRetry
{
    public $policies = [
        MaxAttemptsRetryPolicy::class,
        ClassifierRetryPolicy::class,
        SleepRetryPolicy::class,
    ];
    public int $maxAttempts = 3;
    public int $base = 100;
    public $strategy = \Hyperf\Retry\BackoffStrategy::class;
}