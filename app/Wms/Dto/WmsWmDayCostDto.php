<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 计费管理Dto （导入导出）
 */
#[ExcelData]
class WmsWmDayCostDto implements MineModelExcel
{
    #[ExcelProperty(value: "主键", index: 0)]
    public string $id;

    #[ExcelProperty(value: "所属部门", index: 1)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 2)]
    public string $sys_company_code;

    #[ExcelProperty(value: "客户", index: 3)]
    public string $cus_code;

    #[ExcelProperty(value: "客户名称", index: 4)]
    public string $cus_name;

    #[ExcelProperty(value: "费用", index: 5)]
    public string $coswms_t_code;

    #[ExcelProperty(value: "费用名称", index: 6)]
    public string $coswms_t_name;

    #[ExcelProperty(value: "费用日期", index: 7)]
    public string $coswms_t_data;

    #[ExcelProperty(value: "每日费用", index: 8)]
    public string $day_coswms_t_yj;

    #[ExcelProperty(value: "不含税价", index: 9)]
    public string $day_coswms_t_bhs;

    #[ExcelProperty(value: "税额", index: 10)]
    public string $day_coswms_t_se;

    #[ExcelProperty(value: "含税价", index: 11)]
    public string $day_coswms_t_hsj;

    #[ExcelProperty(value: "费用来源", index: 12)]
    public string $coswms_t_ori;

    #[ExcelProperty(value: "备注", index: 13)]
    public string $beizhu;

    #[ExcelProperty(value: "状态", index: 14)]
    public string $coswms_t_sta;

    #[ExcelProperty(value: "计费数量", index: 15)]
    public string $coswms_t_sl;

    #[ExcelProperty(value: "数量单位", index: 16)]
    public string $coswms_t_unit;

    #[ExcelProperty(value: "是否已结算", index: 17)]
    public string $coswms_t_js;

    #[ExcelProperty(value: "状态", index: 18)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 19)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 20)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 21)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 22)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 23)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 24)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 25)]
    public string $deleted_at;


}