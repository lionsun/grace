<?php
declare(strict_types=1);

namespace App\Common\Service\Tool;

use App\Common\Service\BaseService;

class ConstantsService extends BaseService
{
    // Header token 参数名称
    const TOKEN = 'token';

    // api-token
    const API_TOKEN = 'api_token:';

    const API_USER_ID_NAME = 'api_user_id';
    const API_USER_DATA = 'api_user_data';

    const API_SOURCE = 'api_source';

    // 默认头像
    const AVATAR = 'https://kiki-admin.oss-cn-beijing.aliyuncs.com/uploadfile/20220720/19228628684960.jpg';

}
