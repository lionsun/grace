<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsRfidBuse;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * RFID管理Mapper类
 */
class WmsRfidBuseMapper extends AbstractMapper
{
    /**
     * @var WmsRfidBuse
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsRfidBuse::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 业务编号
        if (isset($params['wms_rfid_buseno']) && $params['wms_rfid_buseno'] !== '') {
            $query->where('wms_rfid_buseno', '=', $params['wms_rfid_buseno']);
        }

        // 业务内容
        if (isset($params['wms_rfid_busecont']) && $params['wms_rfid_busecont'] !== '') {
            $query->where('wms_rfid_busecont', '=', $params['wms_rfid_busecont']);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}