<?php

declare (strict_types=1);
namespace App\Wms\Model;

use App\Wms\Mapper\WmsBaStoreMapper;
use Hyperf\Database\Model\Relations\HasOne;
use Hyperf\Database\Model\SoftDeletes;
use Hyperf\Snowflake\Concern\Snowflake;
use Mine\Exception\NormalStatusException;
use Mine\MineModel;
/**
 * @property string $id 主键
 * @property string $sys_org_code 所属部门
 * @property string $sys_company_code 所属公司
 * @property string $ku_wei_ming_cheng 库位名称
 * @property string $ku_wei_bian_ma 库位编码
 * @property string $ku_wei_tiao_ma 库位条码
 * @property string $ku_wei_lei_xing 库区
 * @property string $ku_wei_shu_xing 库位属性
 * @property string $shang_jia_ci_xu 上架次序
 * @property string $qu_huo_ci_xu 取货次序
 * @property string $suo_shu_ke_hu 所属客户
 * @property string $ti_ji_dan_wei 体积单位
 * @property string $zhong_liang_dan_wei 重量单位
 * @property string $mian_ji_dan_wei 面积单位
 * @property string $zui_da_ti_ji 最大体积
 * @property string $zui_da_zhong_liang 最大重量
 * @property string $zui_da_mian_ji 最大面积
 * @property string $zui_da_tuo_pan 最大托盘
 * @property string $chang 长度
 * @property string $kuan 宽度
 * @property string $gao 高度
 * @property string $ting_yong 停用
 * @property string $ming_xi 明细
 * @property string $bin_store 仓库
 * @property string $chp_shu_xing 产品属性
 * @property string $ming_xi1 备注1
 * @property string $ming_xi2 备注2
 * @property string $ming_xi3 动线
 * @property string $LORA_bqid
 * @property string $xnode
 * @property string $ynode
 * @property string $znode
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 * @property string $ku_shelves 货架
 * @property string $ku_layers 层数
 */
class WmsMdBin extends MineModel
{
    use SoftDeletes;
    use Snowflake {
        creating as createPrimaryKey;
    }

    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wms_md_bin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sys_org_code', 'sys_company_code', 'ku_wei_ming_cheng', 'ku_wei_bian_ma',
        'ku_wei_tiao_ma', 'ku_wei_lei_xing', 'ku_wei_shu_xing', 'shang_jia_ci_xu', 'qu_huo_ci_xu',
        'suo_shu_ke_hu', 'ti_ji_dan_wei', 'zhong_liang_dan_wei', 'mian_ji_dan_wei', 'zui_da_ti_ji',
        'zui_da_zhong_liang', 'zui_da_mian_ji', 'zui_da_tuo_pan', 'chang', 'kuan', 'gao', 'ting_yong',
        'ming_xi', 'bin_store', 'chp_shu_xing', 'ming_xi1', 'ming_xi2', 'ming_xi3', 'LORA_bqid', 'xnode',
        'ynode', 'znode', 'status', 'sort', 'mer_id', 'created_by', 'updated_by', 'created_at', 'updated_at',
        'deleted_at','ku_location_use_status','ku_shelves','ku_layers'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'ku_wei_lei_xing' => 'string','ku_wei_shu_xing' => 'string',
        'suo_shu_ke_hu' =>'string','bin_store' => 'string','wms_location_type' => 'string',
        'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer', 'created_by' => 'integer',
        'updated_by' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime',
        'wms_goods_env' => 'string','ku_location_use_status' => 'string',
        'ku_shelves' => 'string','ku_layers' => 'string'
    ];

    /**
     * 仓库
     * @return HasOne
     */
    public function baStore(): HasOne
    {
        return $this->hasOne(WmsBaStore::class, 'id', 'bin_store');
    }

    /**
     * 客户
     * @return HasOne
     */
    public function mdCus(): HasOne
    {
        return $this->hasOne(WmsMdCus::class, 'id', 'suo_shu_ke_hu');
    }

    public function saving()
    {
        $this->createPrimaryKey();
        /** @var WmsBaStoreMapper $storeMapper */
        $storeMapper = make(WmsBaStoreMapper::class);
        $baStore = $storeMapper->read((int)$this->bin_store);
        if (!$baStore) {
            throw new NormalStatusException('仓库信息不存在');
        }

        // 库位编码 = 仓库+库位类型+货架+层数
        $this->ku_wei_bian_ma = $baStore['store_code'].$this->ku_wei_lei_xing.'-'.$this->ku_shelves.'-'.$this->ku_layers;
        $this->ku_wei_tiao_ma = $this->ku_wei_bian_ma;
    }
}