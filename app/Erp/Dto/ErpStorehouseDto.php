<?php
namespace App\Erp\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 仓库信息Dto （导入导出）
 */
#[ExcelData]
class ErpStorehouseDto implements MineModelExcel
{
    #[ExcelProperty(value: "id", index: 0)]
    public string $id;

    #[ExcelProperty(value: "仓库名称", index: 1)]
    public string $name;

    #[ExcelProperty(value: "仓库地址", index: 2)]
    public string $address;

    #[ExcelProperty(value: "仓储费", index: 3)]
    public string $storage_fee;

    #[ExcelProperty(value: "搬运费", index: 4)]
    public string $carry_fee;

    #[ExcelProperty(value: "负责人", index: 5)]
    public string $user_id;

    #[ExcelProperty(value: "状态 (1正常 0停用)", index: 6)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 7)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 8)]
    public string $mer_id;

    #[ExcelProperty(value: "创建者", index: 9)]
    public string $created_by;

    #[ExcelProperty(value: "更新者", index: 10)]
    public string $updated_by;

    #[ExcelProperty(value: "创建时间", index: 11)]
    public string $created_at;

    #[ExcelProperty(value: "更新时间", index: 12)]
    public string $updated_at;

    #[ExcelProperty(value: "删除时间", index: 13)]
    public string $deleted_at;


}