<?php
namespace App\Wms\Dto;

use Mine\Interfaces\MineModelExcel;
use Mine\Annotation\ExcelData;
use Mine\Annotation\ExcelProperty;

/**
 * 入库管理Dto （导入导出）
 */
#[ExcelData]
class WmsWmImNoticeHDto implements MineModelExcel
{
    #[ExcelProperty(value: "主键", index: 0)]
    public string $id;

    #[ExcelProperty(value: "商品", index: 1)]
    public string $goods_id;

    #[ExcelProperty(value: "库位", index: 2)]
    public string $md_bin_id;

    #[ExcelProperty(value: "所属部门", index: 3)]
    public string $sys_org_code;

    #[ExcelProperty(value: "所属公司", index: 4)]
    public string $sys_company_code;

    #[ExcelProperty(value: "客户", index: 5)]
    public string $cus_code;

    #[ExcelProperty(value: "预计到货时间", index: 6)]
    public string $im_data;

    #[ExcelProperty(value: "客户订单号", index: 7)]
    public string $im_cus_code;

    #[ExcelProperty(value: "司机", index: 8)]
    public string $im_car_dri;

    #[ExcelProperty(value: "司机电话", index: 9)]
    public string $im_car_mobile;

    #[ExcelProperty(value: "车号", index: 10)]
    public string $im_car_no;

    #[ExcelProperty(value: "订单类型", index: 11)]
    public string $order_type_code;

    #[ExcelProperty(value: "月台", index: 12)]
    public string $platform_code;

    #[ExcelProperty(value: "单据状态", index: 13)]
    public string $im_sta;

    #[ExcelProperty(value: "进货通知单号", index: 14)]
    public string $notice_id;

    #[ExcelProperty(value: "附件", index: 15)]
    public string $appendix;

    #[ExcelProperty(value: "READ_ONLY", index: 16)]
    public string $READ_ONLY;

    #[ExcelProperty(value: "WHERE_CON", index: 17)]
    public string $WHERE_CON;

    #[ExcelProperty(value: "供应商编码", index: 18)]
    public string $sup_code;

    #[ExcelProperty(value: "供应商名称", index: 19)]
    public string $sup_name;

    #[ExcelProperty(value: "对接单据类型", index: 20)]
    public string $pi_class;

    #[ExcelProperty(value: "账套", index: 21)]
    public string $pi_master;

    #[ExcelProperty(value: "存放库区", index: 22)]
    public string $area_code;

    #[ExcelProperty(value: "状态", index: 23)]
    public string $status;

    #[ExcelProperty(value: "排序", index: 24)]
    public string $sort;

    #[ExcelProperty(value: "商户id", index: 25)]
    public string $mer_id;

//    #[ExcelProperty(value: "创建者", index: 26)]
//    public string $created_by;
//
//    #[ExcelProperty(value: "更新者", index: 27)]
//    public string $updated_by;
//
//    #[ExcelProperty(value: "创建时间", index: 28)]
//    public string $created_at;
//
//    #[ExcelProperty(value: "更新时间", index: 29)]
//    public string $updated_at;
//
//    #[ExcelProperty(value: "删除时间", index: 30)]
//    public string $deleted_at;
//
//    #[ExcelProperty(value: "备注", index: 31)]
//    public string $im_beizhu;


}