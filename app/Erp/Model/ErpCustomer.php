<?php

declare (strict_types=1);
namespace App\Erp\Model;

use Hyperf\Database\Model\SoftDeletes;
use Mine\MineModel;
/**
 * @property int $id
 * @property string $name 名称
 * @property string $phone 手机号码
 * @property string $user_name 联系人
 * @property string $user_phone 联系电话
 * @property string $email 电子邮箱
 * @property string $fax 传真
 * @property string $opening_money 期初应收
 * @property string $end_money 期末应收
 * @property string $tax_iden_num 纳税人识别号
 * @property int $tax_rate 税率(%)
 * @property string $bank 开户行
 * @property string $account 账号
 * @property int $status 状态
 * @property int $sort 排序
 * @property int $mer_id 商户id
 * @property int $created_by 创建者
 * @property int $updated_by 更新者
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class ErpCustomer extends MineModel
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'erp_customer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'phone', 'user_name', 'user_phone',
        'email', 'fax', 'opening_money', 'end_money', 'tax_iden_num',
        'tax_rate', 'bank', 'account', 'status', 'sort', 'mer_id', 'created_by',
        'updated_by', 'created_at', 'updated_at', 'deleted_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer', 'opening_money' => 'decimal:2', 'end_money' => 'decimal:2',
        'tax_rate' => 'integer', 'status' => 'string', 'sort' => 'integer', 'mer_id' => 'integer',
        'created_by' => 'integer', 'updated_by' => 'integer', 'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}