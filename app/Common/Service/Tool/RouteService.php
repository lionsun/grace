<?php
declare(strict_types=1);

namespace App\Common\Service\Tool;

use App\Common\Service\BaseService;
use Hyperf\HttpServer\Router\Dispatched;
use JetBrains\PhpStorm\ArrayShape;

class RouteService extends BaseService
{
    /**
     * 获取请求控制器与方法
     * @param $request
     * @return array
     */
    #[ArrayShape(['controller' => "mixed", 'action' => "mixed"])] public function getControllerAndAction($request): array
    {
        $action = $request->getAttribute(Dispatched::class)->handler->callback;

        return ['controller' => $action[0], 'action' => $action[1]];
    }
}
