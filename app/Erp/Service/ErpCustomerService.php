<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Erp\Service;

use App\Erp\Mapper\ErpCustomerMapper;
use Mine\Abstracts\AbstractService;

/**
 * 客户信息服务类
 */
class ErpCustomerService extends AbstractService
{
    /**
     * @var ErpCustomerMapper
     */
    public $mapper;

    public function __construct(ErpCustomerMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}