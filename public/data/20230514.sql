# system_common_manage_log增加type字段
ALTER TABLE `grace`.`system_common_manage_log`
    ADD COLUMN `type` tinyint(1) NULL DEFAULT 0 COMMENT '0默认，1数组，2对象' AFTER `cn_key`;
# wms_md_bin修改ku_wei_bian_ma索引为普通索引
ALTER TABLE `grace`.`wms_md_bin`
    DROP INDEX `kwbm`,
    ADD INDEX `kwbm`(`ku_wei_bian_ma`) USING BTREE;