<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Common\Mapper;

use App\Common\Model\CommonMerchant;
use App\System\Model\Debug;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 商家管理Mapper类
 */
class CommonMerchantMapper extends AbstractMapper
{
    /**
     * @var CommonMerchant
     */
    public $model;

    public function assignModel()
    {
        $this->model = CommonMerchant::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        
        // 店铺名称
        if (isset($params['title']) && $params['title'] !== '') {
            $query->where('title', '=', $params['title']);
        }

        // 联系人
        if (isset($params['name']) && $params['name'] !== '') {
            $query->where('name', '=', $params['name']);
        }

        // 联系电话
        if (isset($params['tel']) && $params['tel'] !== '') {
            $query->where('tel', '=', $params['tel']);
        }

        // 审核状态 
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 显示 
        if (isset($params['is_show']) && $params['is_show'] !== '') {
            $query->where('is_show', '=', $params['is_show']);
        }

        // 商家类型
        if (isset($params['type']) && $params['type'] !== '') {
            $query->where('type', '=', $params['type']);
        }

        // 支付状态
        if (isset($params['pay_status']) && $params['pay_status'] !== '') {
            $query->where('pay_status', '=', $params['pay_status']);
        }

        // 支付时间
        if (isset($params['pay_time'][0]) && isset($params['pay_time'][1])) {
            $query->whereBetween('pay_time', [$params['pay_time'][0], $params['pay_time'][1]]);
        }

        // 过期时间
        if (isset($params['expire_time'][0]) && isset($params['expire_time'][1])) {
            $query->whereBetween('expire_time', [$params['expire_time'][0], $params['expire_time'][1]]);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}