<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Wms\Request;

use Mine\MineFormRequest;

/**
 * 仓库管理验证数据类
 */
class WmsBaStoreRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['store_name','store_code','store_text','status','sort',],
             'update' => ['store_name','store_code','store_text','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'store_name.required'=>' 仓库名称不能为空',
             'store_code.required'=>' 仓库名称不能为空',
          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 仓库名称 验证
            'store_name' => 'required',
            // 仓库代码 验证
            'store_code' => 'required',
            ];
        }

}