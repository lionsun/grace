<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */

namespace App\Wms\Mapper;

use App\Wms\Model\WmsWmPlawmsTIo;
use Hyperf\Database\Model\Builder;
use Mine\Abstracts\AbstractMapper;

/**
 * 月台管理Mapper类
 */
class WmsWmPlawmsTIoMapper extends AbstractMapper
{
    /**
     * @var WmsWmPlawmsTIo
     */
    public $model;

    public function assignModel()
    {
        $this->model = WmsWmPlawmsTIo::class;
    }

    /**
     * 搜索处理器
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function handleSearch(Builder $query, array $params): Builder
    {
        // id倒序
        $query->orderBy('id','desc');

        $query->with(['baPlatform']);
        // 车牌号
        if (isset($params['carno']) && $params['carno'] !== '') {
            $query->where('carno', '=', $params['carno']);
        }

        // 单据编号
        if (isset($params['doc_id']) && $params['doc_id'] !== '') {
            $query->where('doc_id', '=', $params['doc_id']);
        }

        // 月台
        if (isset($params['plawms_t_id']) && $params['plawms_t_id'] !== '') {
            $query->where('plawms_t_id', '=', $params['plawms_t_id']);
        }

        // 进入时间
        if (isset($params['in_data'][0]) && isset($params['in_data'][1])) {
            $query->whereBetween('in_data', [$params['in_data'][0], $params['in_data'][1]]);
        }

        // 驶出时间
        if (isset($params['ouwms_t_data'][0]) && isset($params['ouwms_t_data'][1])) {
            $query->whereBetween('ouwms_t_data', [$params['ouwms_t_data'][0], $params['ouwms_t_data'][1]]);
        }

        // 备注
        if (isset($params['plawms_t_beizhu']) && $params['plawms_t_beizhu'] !== '') {
            $query->where('plawms_t_beizhu', '=', $params['plawms_t_beizhu']);
        }

        // 计划进入时间
        if (isset($params['plan_indata'][0]) && isset($params['plan_indata'][1])) {
            $query->whereBetween('plan_indata', [$params['plan_indata'][0], $params['plan_indata'][1]]);
        }

        // 计划驶出时间
        if (isset($params['plan_outdata'][0]) && isset($params['plan_outdata'][1])) {
            $query->whereBetween('plan_outdata', [$params['plan_outdata'][0], $params['plan_outdata'][1]]);
        }

        // 状态
        if (isset($params['status']) && $params['status'] !== '') {
            $query->where('status', '=', $params['status']);
        }

        // 排序
        if (isset($params['sort']) && $params['sort'] !== '') {
            $query->where('sort', '=', $params['sort']);
        }

        // 商户id
        if (isset($params['mer_id']) && $params['mer_id'] !== '') {
            $query->where('mer_id', '=', $params['mer_id']);
        }

        // 创建时间
        if (isset($params['created_at'][0]) && isset($params['created_at'][1])) {
            $query->whereBetween('created_at', [$params['created_at'][0], $params['created_at'][1]]);
        }

        return $query;
    }
}