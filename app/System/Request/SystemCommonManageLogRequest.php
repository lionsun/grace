<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\System\Request;

use Mine\MineFormRequest;

/**
 * 业务日志验证数据类
 */
class SystemCommonManageLogRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => [],
             'update' => [],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             
          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            ];
        }

}