<?php
declare(strict_types=1);
/**
 * Grace is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using Grace.
 *
 * @Author kiki
 */
namespace App\Erp\Request;

use Mine\MineFormRequest;

/**
 * 结算账户验证数据类
 */
class ErpAccountRequest extends MineFormRequest
{
    /**
          * 验证场景
          */
         public $scenes = [
             'create' => ['name','number','opening_balance','current_balance','status','sort',],
             'update' => ['name','number','opening_balance','current_balance','status','sort',],
         ];

        /**
         * Determine if the user is authorized to make this request.
         */
        public function authorize(): bool
        {
            return true;
        }

        /**
         * 获取已定义验证规则的错误消息
         */
        public function messages(): array
        {
           return [
             'name.required'=>' 名称不能为空',
'number.required'=>' 编号不能为空',
'opening_balance.numeric'=>' 期初金额格式错误',
'current_balance.numeric'=>' 当前余额格式错误',

          ];
        }

        /**
         * Get the validation rules that apply to the request.
         */
        public function rules(): array
        {
            return [
                
            // 名称 验证
            'name' => 'required',
            // 编号 验证
            'number' => 'required',
            // 期初金额 验证
            'opening_balance' => 'numeric',
            // 当前余额 验证
            'current_balance' => 'numeric',
            ];
        }

}