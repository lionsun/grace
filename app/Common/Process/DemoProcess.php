<?php
/**
 * Created by PhpStorm
 * User: kiki
 * Date: 2022/7/8
 * Time: 17:59
 */
namespace App\Common\Process;

use Hyperf\Process\AbstractProcess;
use Hyperf\Process\Annotation\Process;

#[Process(name: "demo_process")]
class DemoProcess extends AbstractProcess
{
    public function handle(): void
    {
        while (true) {
            echo 666;

            sleep(1);
        }
    }

    public function isEnable($server): bool
    {
        // 不跟随服务启动一同启动
        return false;
    }
}