<p>引用类： use App\Common\Task\GraceTask;</p>
<p>注解：#[Inject]protected GraceTask $graceTask;</p>
<p>使用：$this->graceTask->push(new GraceProducer($params),$delayTime);</p>
<p>GraceProducer为生产者,$params为生产参数，$delayTime为延迟时间</p>